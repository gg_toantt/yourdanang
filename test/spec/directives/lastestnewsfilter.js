'use strict';

describe('Directive: lastestNewsFilter', function () {

  // load the directive's module
  beforeEach(module('nddkApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<lastest-news-filter></lastest-news-filter>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the lastestNewsFilter directive');
  }));
});
