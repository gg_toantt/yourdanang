﻿﻿'use strict';

/**
 * @ngdoc overview
 * @name nddkApp
 * @description # nddkApp
 *
 * Main module of the application.
 */

var app = angular.module('nddkApp', [
    'ngAnimate',
    'ngCookies',
    'ngSanitize',
    'ui.router',
    'ngResource',
    'ngTouch',
    'angularUtils.directives.dirPagination',
    'facebook',
    'googleplus',
    'ngDialog',
    'ipCookie',
    'noCAPTCHA',
    'seo',
    'angucomplete-alt',
    'slickCarousel'
 	])
	.run(function($rootScope) {
		$rootScope.$on('$locationChangeSuccess', function(event, newState) {
            $("body,html").animate({
                scrollTop: 0
            }, "normal");
            $("#page").animate({
                scrollTop: 0
            }, "normal");
             $(".wd-content").animate({
                scrollTop: 0
            }, "normal");
            if ($('#container-dropdown').hasClass('open')) {
                $('#container-dropdown').removeClass('open');
            }
        });
    })
 .config(['$urlRouterProvider','$stateProvider', '$locationProvider', function($urlRouterProvider, $stateProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    $locationProvider.html5Mode(true);
    //$urlRouterProvider.otherwise('/');

    $stateProvider
    .state('home', {
        url: '/',
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
    })
    .state('requiredLoginPublic', {
        url: '/',
        templateUrl: 'views/requiredLoginPublic.html',
        controller: ''
    })
    .state('about', {
        url: '/ve-da-nang',
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        meta : {
            titlePage: "Giới thiệu về đà nẵng",
            metaKeywords: "Giới thiệu về đà nẵng",
            metaDescription: "Giới thiệu về đà nẵng",
            thumbNailUrl: ""}
    })
    .state('aboutTopic', {
        url: '/ve-da-nang/:catId',
        templateUrl: 'views/aboutTopic.html',
        controller: 'AboutTopicCtrl as aboutTopicCtrl'
    })
    .state('aboutDetail', {
        url: '/ve-da-nang/:catId/:aboutId',
        templateUrl: 'views/aboutDetail.html',
        controller: 'AboutDetailCtrl'
    })
    .state('news', {
        url: '/tin-tuc',
        templateUrl: 'views/news.html',
        controller: 'NewsCtrl',
        meta : {
           titlePage: "Tin tức",
           metaKeywords: "Tin tức - Sự kiện",
           metaDescription: "Tin tức - Sự kiện",
           thumbNailUrl: ""
        }
    })
    .state('newsTopic', {
        url: '/tin-tuc/:catId',
        templateUrl: 'views/newsTopic.html',
        controller: 'NewsTopicCtrl'
    })
    .state('newsDetail', {
        url: '/tin-tuc/:catId/:newsId',
        templateUrl: 'views/newsDetail.html',
        controller: 'NewsDetailsCtrl'
    })
    .state('video', {
        url: '/video',
        templateUrl: 'views/video.html',
        controller: 'VideoCtrl'
    })
    .state('register', {
        url: '/dang-ky',
        templateUrl: 'views/registration.html',
        controller: ''
    })
    .state('register-enterprise', {
        url: '/dang-ky-doanh-nghiep',
        templateUrl: 'views/registration.html',
        controller: ''
    })
    .state('register-succ', {
        url: '/r-success',
        templateUrl: 'views/registration-succ.html',
        controller: 'registrationSuccessCtrl',
        params: {'registedEmail': null, 'business':null}
    })
    .state('shareFeel', {
        url: '/chia-se-cam-nhan',
        templateUrl: 'views/share-feel.html',
        params:{'page':null},
        meta : {
               titlePage: "Chia sẻ cảm nhận",
               metaKeywords: "Chia sẻ cảm nhận",
               metaDescription: "Chia sẻ cảm nhận của bạn về Đà Nẵng",
               thumbNailUrl: ""},
        controller: 'ShareFellCtrl as shrCtrl'
    })
    .state('shareFeelDetail', {
        url: '/chia-se-cam-nhan/:shareId',
        templateUrl: 'views/share-detail.html',
        controller: 'ShareFeelDetailCtrl as shrDetailCtrl'
    })
    .state('confirmAccount', {
        url: '/dang-ky/thong-bao/:code',
        templateUrl: 'views/notice.html',
        controller: 'registrationSuccessCtrl'
    })
    .state('service', {
        url : '/dich-vu/:fieldId',
        templateUrl : 'views/residence.html',
        controller : 'residenceService'
    })
    .state('serviceDetail', {
        url : '/dich-vu/:fieldId/:serviceUrl',
        templateUrl : 'views/residence-detail.html',
        controller : 'residenceServiceDetail as serviceDetailCtrl'
    })
    .state('forgotPass', {
        url: '/quen-mat-khau',
        templateUrl: 'views/forgotPass.html',
        controller: 'LoginCtrl'
    })
    .state('changePassword', {
        url: '/thay-doi-mat-khau/:code',
        templateUrl: 'views/getPasswordAgain.html',
        controller: 'LoginCtrl'
    })
    .state('profile', {
        url: '/thong-tin-ca-nhan',
        templateUrl: 'views/profile.html',
        controller: 'LoginCtrl'
    })
    .state('profileEdit', {
        url: '/thong-tin-ca-nhan/chinh-sua',
        templateUrl: 'views/profile-edit.html',
        controller: 'LoginCtrl'
    })
    .state('header', {
        url: '/header',
        templateUrl: 'views/header.html',
        controller: ''
    })
    .state('shoppingOrder', {
        url: '/mua-sam',
        templateUrl: 'views/shopping-order.html',
        controller: 'shoppingOrder',
        meta : {
            titlePage: "Mua sắm",
            metaKeywords: "Mua sắm",
            metaDescription: "Xem trước các dịch vụ Lưu trú, Ẩm thực, Mua sắm, Vui chơi giải trí",
            thumbNailUrl: ""}
    })
    .state('shoppingCart', {
        url: '/gio-hang',
        templateUrl: 'views/order.html',
        controller: 'shoppingCart'
    })
    .state('search', {
        url: '/tim-kiem/:isExactly/:keywords',
        templateUrl: 'views/search.html',
        controller: 'SearchCtrl'
    })
    .state('orderSuccess', {
        url: '/dat-hang-thanh-cong/:ma',
        templateUrl: 'views/order-success.html',
        controller: 'shoppingOrder'
    })
    .state('map', {
        url: '/ban-do?sid&dichvu',
        templateUrl: 'views/map.html',
        controller: 'MapCtrl',
        meta : {
            titlePage: "Bản đồ",
            metaKeywords: "Bản đồ",
            metaDescription: "Bản đồ",
            thumbNailUrl: ""}
    })
    .state('player', {
        url: '/player',
        templateUrl: 'views/jwplayer.html',
        controller: ''
    })
    .state('careerDetail', {
        url : '/viec-lam/:fieldId/',
        templateUrl : 'views/job-detail.html',
        controller : 'careerDetailCtrl'
    })
    .state('jobs', {
        url: '/viec-lam',
        templateUrl: 'views/jobs.html',
        controller: 'careerCtrl as careCtr',
        meta : {
           titlePage: "Việc làm",
           metaKeywords: "Việc làm",
           metaDescription: "Việc làm",
           thumbNailUrl: ""
        }
    });
    
 }]);

 app.directive('colorBoxBind', function() {
        return {
            restrict : 'A',
            link : function(scope, element, attrs, timeout) {
                if (scope.$last) {
                    $(".wd-gallery-img li a").colorbox({rel:'wd-group-1', maxHeight:'90%',  maxWidth: '100%'});

                    $("#cboxContent").swipe({
                        swipeLeft: function() {
                            $.colorbox.next();
                        },
                        swipeRight: function() {
                            $.colorbox.prev();
                        }
                    });                
                }
            }
        };
});


 