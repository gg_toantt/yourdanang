;(function(){
	'use strict';
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:checkRegisterSuccCtrl
	 * @description # checkRegisterSuccCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp')
	.controller('checkRegisterSuccCtrl', checkRegisterSuccCtrl);
	checkRegisterSuccCtrl.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT', '$state', 'Scopes', 'ipCookie' ];
	
	function checkRegisterSuccCtrl($scope, $timeout, homeService, CONSTANT, $state, Scopes, ipCookie) {
		$scope.user = ipCookie('user');

		if (angular.isDefined($scope.user) && $scope.user != null) {
			$state.go('home', {});
		}
	}
})();