;
(function() {	
	'use strict';
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:AboutCtrl
	 * @description
	 * # AboutCtrl
	 * Controller of the nddkApp
	 */
	 angular.module('nddkApp')
	 .controller('AboutCtrl', aboutController)
	 .controller('AboutDetailCtrl', aboutDetailController);

	 aboutController.$inject = [ '$scope', 'aboutService', 'CONSTANT', 'homeService', '$rootScope' ];
	 aboutDetailController.$inject = [ '$scope', '$stateParams', 'aboutService', 'CONSTANT', 'homeService', 'newsService', 'Scopes', "$location", '$rootScope' ];
	 
	 function aboutController($scope, aboutService, CONSTANT, homeService, $rootScope) {
		CONSTANT.lang = homeService.getLanguage();
	 	/* Get all category About  */
	 	aboutService.getData('articles/'+ CONSTANT.lang +'/topic/introduction').then(function(res) {
	 		$scope.abouts = res.data.items;
	 	// check meta data
            homeService.checkMetaState();
            setTimeout(function() {
            	$rootScope.htmlReady();
  	 		}, 100);
	 	});
	 }
	 
	 function aboutDetailController($scope, $stateParams, aboutService, CONSTANT, homeService, newsService, Scopes, $location, $rootScope) {
		CONSTANT.lang = homeService.getLanguage();
	 	/* Detail of topic */
		
		var self = this;
		self.popupEmail = popupEmail;
		$scope.aboutDetailCtrl = self;
		
	  	var aboutDetailId = $stateParams.aboutId.substring($stateParams.aboutId.lastIndexOf('-')+1,$stateParams.aboutId.length);
	 	aboutService.getData('articles/'+ CONSTANT.lang + '/' + aboutDetailId).then(function(res) {
	 		$scope.aboutDetail = res.data.info;
    	 	// create meta header
            homeService.checkMetaState(
                    {"titlePage" : $scope.aboutDetail.tieuDe, 
                     "metaKeywords" : $scope.aboutDetail.tieuDe, 
                     "metaDescription" : $scope.aboutDetail.tomTat, 
                     "thumbNailUrl" : $scope.aboutDetail.anhDaiDien.urlImage,
                     "siteUrl" : $location.$$absUrl
                     });
	 		setTimeout(function() {
	 			$rootScope.htmlReady();
	 		}, 1000);
	 		setTimeout(function() {
	 			newsService.getData('articles/' + CONSTANT.lang + '/' + $scope.aboutDetail.id + '/veDaNang' + '/related_articles').then(function(res) {
	 	    		$scope.newsRelatedArticles = res.data.items;
	 	    	});
	 		}, 100);
	 	});
	 	
	 	$scope.print = function(divName) {
	 		$('.wd-connect-machine').css('display', 'none');
	 		var printContents = document.getElementById(divName).innerHTML;
//	 	    var originalContents = document.body.innerHTML;

	 	    if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
	 	        var popupWin = window.open('', '_blank', 'width=800,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
	 	        popupWin.window.focus();
	 	        popupWin.document.write('<!DOCTYPE html><html><head>' +
	 	            '<link rel="stylesheet" type="text/css" href="style.css" />' +
	 	            '</head><body onload="">' +
	 	            '<div style="float:right;"><a href="javascript:;" onClick="window.print()">In trang</a>' +
	 	            '<span style="color:#B8B8B8;"> (Ctrl + p)</span></div>' +
	 	            '<div class="reward-body">' + printContents + '</div></html>');
	 	        popupWin.onbeforeunload = function (event) {
	 	            popupWin.close();
//	 	            return '.\n';
	 	        };
	 	        popupWin.onabort = function (event) {
	 	            popupWin.document.close();
	 	            popupWin.close();
	 	        };
	 	        
	 	       popupWin.document.close();
	 	    } else {
	 	        var popupWin = window.open('', '_blank', 'width=800,height=600,scrollbars=yes,menubar=no,toolbar=no,location=yes,status=yes,titlebar=yes');
	 	        popupWin.document.open();
	 	        popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" />' +
	 	        		'</head><body onload="">' +
	 	        		'<div style="float:right;"><a href="javascript:;" onClick="window.print()">In trang</a>' +
	 	        		'<span style="color:#B8B8B8;"> (Ctrl + p)</span></div>' +
	 	        		printContents + '</html>');
	 	        popupWin.document.close();
	 	    }
	 	    $('.wd-connect-machine').css('display', 'block');
	 	    return true;
	 	};
	 	
	 	// mail to
        function popupEmail() {
            var link = $location.absUrl();
            if ($scope.aboutDetail != null) {
                homeService.openShareEmailPopup($scope.aboutDetail.tieuDe, link);
            }
        }
	 }
})();