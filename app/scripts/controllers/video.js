;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('VideoCtrl', videoCtrl);

    videoCtrl.$inject = [ '$scope', '$stateParams', 'newsService', 'CONSTANT', 'homeService', '$sce', 'ngDialog', '$timeout' ];

    function videoCtrl($scope, $stateParams, newsService, CONSTANT, homeService, $sce, ngDialog, $timeout) {
    	
		CONSTANT.lang = homeService.getLanguage();
    	
        var self = this;
        
        /* public methods */
        self.getListVideo = getListVideo;
        self.showPopupVideo = showPopupVideo;
        self.onDetach = onDetach;
        
        
        /* Scope */
        $scope.vimeoCtrl = self;
        $scope.videoCtrl = self;
        $scope.videos = [];
        $scope.totalVideo = 0;
        $scope.everyPage = 24;

        /* init */
        getListVideo(1);        
        
        /* implements method */
        /* Get list of video in news */
        function getListVideo(numberPage) {
        	var listVideoApi = 'video/'+numberPage; 
            newsService.getData(listVideoApi).then(function(res) {
            	$scope.videos = res.data.info.data;
            	$scope.totalVideo = res.data.info.totalPage * $scope.everyPage;
            });
        }
        
        /* Responsive Equal Height Rows Videos*/
        $(window).load(function() {
        	homeService.equalheight('.item');
        });
        
        /* Video config */
        $scope.currentTime = 0;
		$scope.totalTime = 0;
		$scope.state = null;
		$scope.volume = 1;
		$scope.isCompleted = false;

		$scope.onPlayerReady = function (API) {
			$scope.API = API;
		};

		$scope.onCompleteVideo = function () {
			$scope.isCompleted = true;
		};

		$scope.onUpdateState = function (state) {
			$scope.state = state;
		};

		$scope.onUpdateTime = function (currentTime, totalTime) {
			$scope.currentTime = currentTime;
			$scope.totalTime = totalTime;
		};

		$scope.onUpdateVolume = function (newVol) {
			$scope.volume = newVol;
		};
        
        self.config = {
            preload: "none",
            autoPlay: false,
            theme: {
                url: "http://www.videogular.com/styles/themes/default/latest/videogular.css"
            }
        };
        
        function showPopupVideo(video){
        	ngDialog.open(
        		{
                    template : 'popupVideo',
                    className : 'ngdialog-theme-default custom-width-share-popup',
                    closeByDocument:false,
                    data : {'video':video},
                    controller: ['homeService',
        			            function(homeService) {
                    	// margin center dialog after created
		            	homeService.locateDialog();
                    }]
                }
        		
        	);
        	
        }
        
        function onDetach(){
        	if(API.mediaElement){
        		API.mediaElement[0].destroy();
        	}else{
//        		console.log("not found");
        	}
        }
    }
    ;
})();