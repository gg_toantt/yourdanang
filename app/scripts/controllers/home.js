;(function() {
	'use strict';

	/**
	* @ngdoc function
	* @name nddkApp.controller:HomeCtrl
	* @description # HomeCtrl Controller of the nddkApp
	*/

	angular.module('nddkApp')

	.controller('HomeCtrl', homeController)
	.controller('tabCtrl', tabController);
	homeController.$inject = ['$scope', '$timeout', 'homeService', 'CONSTANT', '$rootScope', '$state'];
	tabController.$inject = ['$scope', '$timeout', 'homeService', 'CONSTANT', '$rootScope' ];
	function homeController($scope, $timeout, homeService, CONSTANT, $rootScope, $state) {
		$scope.clickBannerBusiness = function() {
			if (homeService.isLoggedIn() && homeService.getCurrentLoginUser().business) {
				window.location="http://backend.yourdanang.vn/";
				//window.location="http://192.168.1.247/nddk";
			} else {
				$state.go('register-enterprise');
			}
		}

		$scope.showClose = false;
		$scope.widthDevice = $('body').outerWidth();
		// check meta data
		homeService.checkMetaState();
		setTimeout(function() {
			$rootScope.htmlReady();
		}, 100);

		$('.popup-slider').removeClass('flip-bottom');
		$('.popup-slider').hide();
		/* run slideshow on banner */
		setTimeout(function() {
			angular.element('#wd-banner .flexslider').flexslider({
				animation : 'fade',
				controlNav : false,
				directionNav : false,
				slideshowSpeed : 9000,
				animationSpeed : 200
			});
			// document.getElementById('notification-span').style.display = 'block';
		}, 200);

		setTimeout(function() {
			if ($('.container').outerWidth() >= 768 && $('.container').outerWidth() < 1024) {
				// $(window).load(function () {

		    		var posOne = $('#posOne').outerHeight();
			    	var posTwo = $('#posTwo').outerHeight();

			    	if (posOne > posTwo) {
			    		$('#posTwo').outerHeight(posOne);
			    	} else {
			    		$('#posOne').outerHeight(posTwo);
			    	}

			    	setTimeout(function() {
			    		var posThree = $('#posThree').outerHeight();
				    	var posFour = $('#posFour').outerHeight();
				    	if (posThree > posFour) {
				    		$('#posFour').outerHeight(posThree);
				    	} else {
				    		$('#posThree').outerHeight(posFour);
				    	}
			    	}, 200);
				// });
			}
    	}, 500);

		/* Get 3 lastest news at left position */
		homeService.getData('articles/' + CONSTANT.lang + '/highlight_articles').then(function(res) {			
			$scope.hotNews = res.data.items;
			$scope.showClose = true;
			setTimeout(function() {
				makeHotNews();
			}, 10);
		});

		function makeHotNews() {
			angular.element('#wd-slide-banpro').carouFredSel({
				auto : true,
				width : '100%',
				prev : '#wd-prev-1',
				next : '#wd-next-1',
				pagination : false,
				mousewheel : false,
				items : {
					height : 80,
					visible : 2
				},
				// scroll : 1,
				scroll : {
					items : 1,
					fx : 'scroll',
					easing : "swing",
					// duration : 1000,
					pauseOnHover : true
				},
				direction : 'up',
				swipe : {
					onMouse : true,
					onTouch : true
				},
				onCreate : function() {
					$(this).css('width','314px');
					$(this).css('height','160px');
				}
			});
			$(".hot-news-on-mobile").flexslider({
				animation: "slide",
				slideshow: false,
				animationLoop: false,
				directionNav: false
			});
		}

		/* Get one lastest news at right position */
		homeService.getData('articles/' + CONSTANT.lang + '/highlight').then(function(res) {
			$scope.highlightNews = res.data.info;
			setTimeout(function () {
				$('.popup-slider').show();
				$('.popup-slider').addClass('flip-bottom');
			}, 200);
		});

		/* Get 2 lastest event */
		homeService.getData('articles/' + CONSTANT.lang + '/event').then(function(res) {
			$scope.events = res.data.items.slice(0, 3);
		});

		/* Get lastest promotions */
		homeService.getData('articles/' + CONSTANT.lang + '/promotion').then(function(res) {
			$scope.promotions = res.data.items;
			var swipeAble = $('.container').outerWidth() >= 768;
			$timeout(function() {
				angular.element('#wd-news-promotion').carouFredSel({
					auto : true,
					width : '100%',
					prev : '',
					next : '',
					pagination : false,
					mousewheel : false,
					infinite : false,
					items : 5,
					duration : 5000,
					scroll : {
						items : 1,
						pauseOnHover : true
					},
					direction : 'up',
					swipe : {
						onMouse : swipeAble,
						onTouch : swipeAble
					}
				});


			}, 200);
		});

		/* Get 12 Attractive Destinations */
		/*homeService.getData('services/' + CONSTANT.lang + '/12_attractive_destinations').then(function(res) {
			$scope.destinations = res.data.items.slice(0, 3);
		});*/
		
		/* Get Testimonials */
		homeService.getData('share/highlight').then(function(res) {
			if (angular.isDefined(res.data.info) && res.data.info != null) {
				$scope.testimonials = res.data.info.slice(0, 2);	
			}
			
		});
		
		//get tin tuyen dung
		homeService.getData('tuyenDungs/' +'tuyenDungs').then(function(res) {
			if (res.meta.code == 200 && res.data.items != null) {
				if (res.data.items.length > 3) {
					$scope.tuyenDungs = res.data.items.slice(0, 3);
				} else {
					$scope.tuyenDungs = res.data.items;
				}
			}
		});
		
		$scope.closePopup = function(name) {
			if (name == 'hotNew') {
				$('#slider-hotNew').slideUp(500);
			} else if (name == 'highlightNews') {
				$('#popupSlider').slideUp(500);
			}
		};

	}

	/* Tab controller */
	function tabController($scope, $timeout, homeService, CONSTANT) {
		$scope.listDichVu = [];
		$scope.loading = false;
		$scope.tenLinhVuc = "";
		homeService.getData('services/' + CONSTANT.lang + '/field').then(function(res) {
			$scope.tabFields = res.data.items;
			/* set curent for first tab */
			angular.element('#tabService0').addClass('resp-tab-active');
		});

		var dichVu = {
			idLinhVuc: 0,
			left: {},
			right: {}
		};
		$scope.listDichVu.push(dichVu);

		$scope.hotServiceLoaded = false;
		$scope.hotServiceRightLoaded = false;

		$scope.slickConfig = {
		    infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: 
			[{
				breakpoint: 1024,
				settings: 
				{
					slidesToShow: 3,
					slidesToScroll: 3,
					dots: false
				}
			},
			{
				breakpoint: 601,
				settings: 
				{
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 321,
				settings: 
				{
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}],
			method: {},
			lazyLoad: 'ondemand',
			arrows: true
		};

		$scope.slickConfigSmaller = {
		    infinite: false,
			speed: 300,
			slidesToShow: 3,
			slidesToScroll: 3,
			method: {},
			lazyLoad: 'ondemand',
			arrows: true,
			dots: false,
			responsive: 
			[{
				breakpoint: 768,
				settings: 
				{
					slidesToShow: 6,
					slidesToScroll: 6
				}
			},
			{
				breakpoint: 601,
				settings: 
				{
					slidesToShow: 4,
					slidesToScroll: 4
				}
			},
			{
				breakpoint: 321,
				settings: 
				{
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}]
		};

		/* Get 15 popuplar services */		
		var left = "left";
		homeService.getData('services/' + CONSTANT.lang + '/all_highlight_services?viTri='+left).then(function(res) {
			$scope.dataServiceLeft = res.data.items;
			$scope.listDichVu[0].left = $scope.dataServiceLeft;
			$scope.hotServiceLoaded = true;
		});

		var right = "right";
		homeService.getData('services/' + CONSTANT.lang + '/all_highlight_services?viTri='+right).then(function(res) {
			$scope.dataServiceRight = res.data.items;
			$scope.listDichVu[0].right = $scope.dataServiceRight;
			$scope.hotServiceRightLoaded = true;
			slideRight(false);
		});

		function slideRight(delay) {
			var delayTime = 0;
			if(delay) delayTime = 200;
			$timeout(function() {
				angular.element('#list-dich-vu-right').carouFredSel({
					responsive: true,
					auto: false,
					infinite: false,
					direction: 'up',
					pagination: false,
					scroll: {
						items: 1,
					},
					items: 3
				});
				domHotItemRight(delay);

			}, delayTime);

		}

		function slideLeft(delay) {
			var delayTime = 0;
			if(delay) delayTime = 200;
			$timeout(function() {
				angular.element('#list-dich-vu-left').carouFredSel({
					responsive: true,
					auto: false,
					width: '100%',
					pagination: false,
					circular: false,
    				infinite: false,
					scroll : {
						items: visibleLeft,
						pauseOnHover: true,
						duration: 5000
					},
					items: {
						visible: {
							min: 1,
							max: 5
						}
					},
					height: 306
				});
				domHotItemLeft(delay);
			}, delayTime);
			
		}

		function slideMobileRight(delay) {
			var delayTime = 0;
			if(delay) delayTime = 200;
			$timeout(function() {
				angular.element('#mobile-list-dich-vu-right').carouFredSel({
					responsive: true,
					auto: false,
					width: '100%',
					pagination: false,
					scroll : {
						items        : 1,
						pauseOnHover : true,
						duration: 600
					},
					items: {
    					visible: {
    						min: 1,
    						max: 10
    					}
    				},

				});
				domHotItemMobileRight(delay);
			}, delayTime);
		}


		$scope.flag = false;
		var activeService = 0;
		$scope.showTab = function(serviceId) {
			if(activeService != serviceId) {
				$scope.hotServiceLoaded = false;
				$scope.hotServiceRightLoaded = false;
				activeService = serviceId;
				if (serviceId == 0) {
					$scope.flag = false;
				} else {
					$scope.flag = true;
				}
				$scope.tenLinhVuc= "";
				for (var int = 0; int < $scope.tabFields.length; int++) {
					if(serviceId==$scope.tabFields[int].id){
						$scope.tenLinhVuc = $scope.tabFields[int].ten;
						break;
					}
				}
				
				var currentTab = angular.element(document.querySelector('#tabService' + serviceId));
				/* remove active tabs */
				angular.element('.resp-tabs-list > li').removeClass('resp-tab-active');
				currentTab.addClass('resp-tab-active');

				// Lấy dữ liệu nổi bật - Tab Service
				var apiLink = 'services/' + CONSTANT.lang;
				var apiLeft = '';
				var apiRight = '';
				var check = checkContain(serviceId);
				if(check == -1) {
					if (serviceId === 0) {
						apiLeft = apiLink + '/all_highlight_services?viTri=left';
						apiRight = apiLink + '/all_highlight_services?viTri=right';
					} else {
						apiLeft = apiLink + '/all_tieu_diem_services?idLinhVuc=' + serviceId + '&viTri=left';
						apiRight = apiLink + '/all_tieu_diem_services?idLinhVuc=' + serviceId + '&viTri=right';
					}

					/* Contain all hotServiceLoaded dich vu */
					var dvs = {
						idLinhVuc: serviceId,
						left: {},
						right: {}
					};
					$scope.listDichVu.push(dvs);

					homeService.getData(apiLeft).then(function(res) {
						$scope.dataServiceLeft = res.data.items;
						$scope.listDichVu[$scope.listDichVu.length - 1].left = 	$scope.dataServiceLeft;
						$scope.hotServiceLoaded = true;
						//slideLeft(false);
					});
					homeService.getData(apiRight).then(function(res) {
						$scope.dataServiceRight = res.data.items;
						$scope.listDichVu[$scope.listDichVu.length - 1].right = $scope.dataServiceRight;
						slideRight(false);
						
						$scope.hotServiceRightLoaded = true;
					});
				} else {
					$scope.dataServiceLeft = $scope.listDichVu[check].left;
					$scope.dataServiceRight =  $scope.listDichVu[check].right;
					//slideLeft(false);
					$timeout(function() {$scope.hotServiceLoaded = true;$scope.hotServiceRightLoaded = true;});
					
					slideRight(false);
					
				}
			}
		};

		function checkContain(id) {
			for(var i = 0; i < $scope.listDichVu.length; i++) {
				if($scope.listDichVu[i].idLinhVuc == id)
				return i;
			}
			return -1;
		}

		var $itemLeft = [], //Cache your DOM selector
		visibleLeft = 1, //Set the number of items that will be visible
		indexLeft = 0, //Starting index
		endIndexLeft = 0,
		itemWidthLeft = 0,
		scrolledLeft = true;
		function domHotItemLeft(init) {
			$itemLeft = $('#list-dich-vu-left .item-hot'),
			indexLeft = 0,
			endIndexLeft = ( $itemLeft.length / visibleLeft ) - 1,
			itemWidthLeft = 0;
			var i = false;
			if($itemLeft.length > 0) {
				itemWidthLeft = $('#list-dich-vu-left .item-hot:first').outerWidth();
			}
			setUpControllLeft(indexLeft, endIndexLeft);
			if(true) {
				var swipeElement = {};
				if ($('.container').outerWidth() >= 768) {
					swipeElement = $("#list-dich-vu-left");
				} else {
					swipeElement = $(".img-left");
				}

				swipeElement.swipe({
					excludedElements: "button, input, select, textarea, a,  .noSwipe",
					swipeLeft: function() {
		               next();
		            },
		            swipeRight: function() {
		                prev();
		            }
		        });

				$('#wd-next-4').click(function() {
					next();
				});

				$('#wd-prev-4').click(function() {
					prev();
				});
			}

			function next() {
				if(indexLeft < endIndexLeft && scrolledLeft ) {
					scrolledLeft = false;
					indexLeft++;
					setUpControllLeft(indexLeft, endIndexLeft);
						$itemLeft.animate({'left':'-='+(itemWidthLeft+10)*visibleLeft}, 500, function() {
						    // Animation complete.
							scrolledLeft = true;
						});
				}
			}
			function prev() {
				if(indexLeft > 0) {
					scrolledLeft = false;
					indexLeft--;
					setUpControllLeft(indexLeft, endIndexLeft);
					$itemLeft.animate({'left':'+='+(itemWidthLeft+10)*visibleLeft}, 500, function() {
						// Annimate complete
						scrolledLeft = true;
					});
				}
			}
		}

		function setUpControllLeft(index, endIndex) {
			if(index < endIndex) {
				$('#wd-next-4').show();
			} else {
				$('#wd-next-4').hide();
			}
			if(index > 0) {
				$('#wd-prev-4').show();
			}else{
				$('#wd-prev-4').hide();
			}
		}

		var $itemRight = [], //Cache your DOM selector
		visibleRight = 1, //Set the number of items that will be visible
		indexRight = 0, //Starting index
		endIndexRight = 0,
		itemHeightRight = 0,
		scrolledRight = true;
		function domHotItemRight(init) {
			$itemRight = $('#list-dich-vu-right .hot-item'); //Cache your DOM selector
			indexRight = 0; //Starting index
			endIndexRight = ( $itemRight.length / visibleRight ) - 1;
			itemHeightRight = 0;
			if($itemRight.length > 0) {
				itemHeightRight = $('#list-dich-vu-right .hot-item:first').outerHeight();
			}
			setUpControllRight(indexRight, endIndexRight);
			if(true) {
				$('#wd-next-6').click(function() {
					if(indexRight < endIndexRight && scrolledRight ) {
						scrolledRight = false;
						indexRight++;
						setUpControllRight(indexRight, endIndexRight);
						$("#list-dich-vu-right").animate({
							'top':'-='+itemHeightRight*visibleRight
						}, 600, function(){
							// Annimate complete
							scrolledRight = true;
						});
					}
				});

				$('#wd-prev-6').click(function() {
					if(indexRight > 0) {
						scrolledRight = false;
						indexRight--;
						setUpControllRight(indexRight, endIndexRight);
						$("#list-dich-vu-right").animate({
							'top':'+='+itemHeightRight*visibleRight
						}, 600, function(){
							// Annimate complete
							scrolledRight = true;
						});
					}
				});
			}
		}

		function setUpControllRight(index, endIndex) {
			if(index < endIndex) {
				$('#wd-next-6').show();
			} else {
				$('#wd-next-6').hide();
			}
			if(index > 0) {
				$('#wd-prev-6').show();
			}else{
				$('#wd-prev-6').hide();
			}
		}


		$scope.reConfig = function() {
		}

		var $itemMobileRight = [], //Cache your DOM selector
		visibleMobileRight = 1, //Set the number of items that will be visible
		indexMobileRight = 0, //Starting index
		endIndexMobileRight = 0,
		itemWidthMobileRight = 0,
		scrolledMobileRight = true;
		function domHotItemMobileRight(init) {
			$itemMobileRight = $('#mobile-list-dich-vu-right .item-hot'),
			indexMobileRight = 0,
			endIndexMobileRight = ( $itemMobileRight.length / visibleMobileRight ) - 1,
			itemWidthMobileRight = 0;
			var i = false;
			if($itemMobileRight.length > 0) {
				itemWidthMobileRight = $('#mobile-list-dich-vu-right .item-hot:first').outerWidth();
			}
			setUpControllMobileRight(indexMobileRight, endIndexMobileRight);
			if(true) {
				$(".img-right").swipe({
					excludedElements: "button, input, select, textarea, a,  .noSwipe",
					swipeLeft: function() {
		               next();
		            },
		            swipeRight: function() {
		                prev();
		            }
		        });

				$('#wd-next-8').click(function() {
					next();
				});

				$('#wd-prev-8').click(function() {
					prev();
				});
			}

			function next() {
				if(indexMobileRight < endIndexMobileRight && scrolledMobileRight ) {
					scrolledMobileRight = false;
					indexMobileRight++;
					setUpControllMobileRight(indexMobileRight, endIndexMobileRight);
						$itemMobileRight.animate({'left':'-='+(itemWidthMobileRight+10)*visibleMobileRight}, 500, function() {
						    // Animation complete.
							scrolledMobileRight = true;
						});
				}
			}
			function prev() {
				if(indexMobileRight > 0) {
					scrolledMobileRight = false;
					indexMobileRight--;
					setUpControllMobileRight(indexMobileRight, endIndexMobileRight);
					$itemMobileRight.animate({'left':'+='+(itemWidthMobileRight+10)*visibleMobileRight}, 500, function() {
						// Annimate complete
						scrolledMobileRight = true;
					});
				}
			}
		}

		function setUpControllMobileRight(index, endIndex) {
			if(index < endIndex) {
				$('#wd-next-8').show();
			} else {
				$('#wd-next-8').hide();
			}
			if(index > 0) {
				$('#wd-prev-8').show();
			}else{
				$('#wd-prev-8').hide();
			}
		}
	}
})();
