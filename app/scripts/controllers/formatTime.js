;
(function() {
    'use strict';
    angular.module('nddkApp').controller('FormatTimeCtrl', FormatTimeCtrl);

    FormatTimeCtrl.$inject = [ '$scope', 'homeService'];

    function FormatTimeCtrl($scope, homeService) {    
    	
        var self = this;
        // public properties
        self.convertTime = convertTime;
        
        $scope.formatTimeCtrl = self;
        
        function convertTime(date) {
        	return homeService.convertTime(date);
        };
        
    };
})();