;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('BannerCtrl', BannerCtrl);

    BannerCtrl.$inject = [ '$scope', 'homeService', 'Scopes' ];

    function BannerCtrl($scope, homeService, Scopes) {
			var self = this;
			// public properties

			// public methods
        
			/* Scope */
			$scope.bnCtrl = self;
			$scope.banner1 = [];
			$scope.banner2 = [];
			$scope.banner2 = [];
			$scope.banner4 = [];

			/* init */
			getBanner1(); 
			getBanner2(); 
			getBanner3(); 
			getBanner4();
        
			/* implements method */
			function getBanner1() {
				var banner1Api = 'advertising/banner_1/'; 
				homeService.getData(banner1Api).then(function(res) {
					if (res.meta.code == 200) {
						$scope.banner1 = res.data.info;
						angular.forEach(res.data.info, function(value, index) {
							if (angular.isDefined(Scopes.get("EventBannerCtrl"))) {
								Scopes.get("EventBannerCtrl").impressionBanner(res.data.info[index].id);		
							}
						})
					}
				}); 
			}
			
			function getBanner2() {
				var banner2Api = 'advertising/banner_2/'; 
				homeService.getData(banner2Api).then(function(res) {
					if (res.meta.code == 200) {
						$scope.banner2 = res.data.info;
						angular.forEach(res.data.info, function(value, index) {
							if (angular.isDefined(Scopes.get("EventBannerCtrl"))) {
								Scopes.get("EventBannerCtrl").impressionBanner(res.data.info[index].id);	
							}
						})
					}
				});
			}
			
			function getBanner3() {
				var banner3Api = 'advertising/banner_3/'; 
				homeService.getData(banner3Api).then(function(res) {
					if (res.meta.code == 200) {
						$scope.banner3 = res.data.info;
						angular.forEach(res.data.info, function(value, index) {
							if (angular.isDefined(Scopes.get("EventBannerCtrl"))) {
								Scopes.get("EventBannerCtrl").impressionBanner(res.data.info[index].id);	
							}
						})
					}
				});
			}
			
			function getBanner4() {
				var banner4Api = 'advertising/banner_4/'; 
				homeService.getData(banner4Api).then(function(res) {
					if (res.meta.code == 200) {
						$scope.banner4 = res.data.info;
						angular.forEach(res.data.info, function(value, index) {
							if (angular.isDefined(Scopes.get("EventBannerCtrl"))) {
								Scopes.get("EventBannerCtrl").impressionBanner(res.data.info[index].id);	
							}
						})
					}
				});
			}
    }
})();