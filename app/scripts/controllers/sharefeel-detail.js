﻿;(function() {
	'use strict';	
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:ShareCtrl
	 * @description
	 * # ShareCtrl
	 * Controller of the nddkApp
	 */
	angular.module('nddkApp')
	 .controller('ShareFeelDetailCtrl', shareFeelDetailCtrl);
	 
	 shareFeelDetailCtrl.$inject = ['$scope', '$stateParams', 'homeService', 'activeService','CONSTANT', '$rootScope', '$location'];
	 
	 function shareFeelDetailCtrl($scope, $stateParams, homeService, activeService, CONSTANT, $rootScope, $location){
		var self = this;
		CONSTANT.lang = homeService.getLanguage();
		// public properties
		self.shareDetailId = $stateParams.shareId;
		
		//public method
		self.getShareFeelDetail = getShareFeelDetail;
		self.getListCommentByShareFeel = getListCommentByShareFeel;
		self.countInterestByShareFeel = countInterestByShareFeel;
		self.addCommentOfShare = addCommentOfShare;
		self.showHideCommentBox = showHideCommentBox;
		self.isBlank = isBlank;
		self.addReplyOfComment = addReplyOfComment;
		self.interest = interest;
		self.checkLoginRequired = checkLoginRequired;
		
		$scope.shareFeelDetail = {};
		$scope.commentByShareList = [];
		$scope.interest = {};
		$scope.count = 1;
		$scope.commentContent = '';
		$scope.replyCommentContent = '';
        $scope.everyPage = 10;
        $scope.totalComment = 0;
		
		// create $scope.shareFeelDetail when load page
		getShareFeelDetail();
		getListCommentByShareFeel(1);
		countInterestByShareFeel();
		
		// get share feel detail by id
		function getShareFeelDetail(){
			homeService.getData('share/' + self.shareDetailId).then(function(res) {
				$scope.shareFeelDetail = res.data.info;
				// create meta header
                homeService.checkMetaState(
                        {"titlePage" : $scope.shareFeelDetail.tieuDe, 
                         "metaKeywords" : $scope.shareFeelDetail.tieuDe, 
                         "metaDescription" : $scope.shareFeelDetail.noiDung, 
                         "thumbNailUrl" : $scope.shareFeelDetail.urlImage,
                         "siteUrl" : $location.$$absUrl}
                );
                setTimeout(function() {
    	 			$rootScope.htmlReady();
    	 		}, 100);
			});
		}
		// get list comment of share
		function getListCommentByShareFeel(page){
			homeService.getDataByGetMethod('comment/share/' + self.shareDetailId + '/list', {'page':page}).then(function(res) {
				$scope.commentByShareList = res.data.data.items[0];
				$scope.countComment = res.data.data.items.count;
				$scope.totalComment = res.data.data.items[2];
			});
		}
		// get count interest of share feel
		function countInterestByShareFeel(){
			var username = '';
			if(homeService.isLoggedIn()){
			    username = homeService.getCurrentLoginUser().id;
			}
			var param = 'username='+username;
			var path = 'comment/share/' + self.shareDetailId + '/interest/count';
			activeService.addInterest(path, param)
			.success(function(data, status, headers, config) {
				if(data.meta.code == 200){
					$scope.interest = data.data.info;
				}else{
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
				}
			})
			.error(function(data, status){
				homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
			});
		}
		
		// add comment of share
		function addCommentOfShare(){
			if(homeService.isLoggedIn()){
				var username = homeService.getCurrentLoginUser().id;
				if(self.isBlank($scope.commentContent)) {
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan, 2500);
				}else{
					var param = 'username='+username+'&content='+$scope.commentContent;
					var path = 'comment/share/'+ self.shareDetailId + '/add';
					activeService.addComment(path, param)
					.success(function(data, status, headers, config) {
						if(data.meta.code == 200){
							$scope.commentContent = '';
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong, 2500);
						} else {
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
						}
					}).error(function(data, status) {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
					});
				}
			}else{
				homeService.requriedLogin();
			}
		}
		
		// add reply of comment
		function addReplyOfComment(comment){
			if(homeService.isLoggedIn()){
				var username = homeService.getCurrentLoginUser().id;
				if(self.isBlank(comment.reply)) {
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan, 2500);
				}else{
				//comment/share/{id}/reply/{idComment}/add
					var param = 'username='+username+'&content='+comment.reply;
					var path = 'comment/share/'+ self.shareDetailId + '/reply/' +comment.id+ '/add';
					activeService.addComment(path, param)
					.success(function(data, status, headers, config) {
						if(data.meta.code == 200){
							comment.reply = '';
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong, 2500);
						} else {
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
						}
					}).error(function(data, status) {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
					});
				}
			}else{
				homeService.requriedLogin();
			}
		}
		
		//show comment box
		function showHideCommentBox(comment_box_id){
			//$scope.replyCommentContent = '';
			if ($('#'+comment_box_id).is(":visible"))
			{
				//visible do some than
				$('#'+comment_box_id).hide('slow');
			}
			else
			{
				//not visible do some else
				$('#'+comment_box_id).show('slow');
			}
		}
		
		//add every type of comment
		function addComment(path, param){
			activeService.addComment(path, param)
			.success(function(data, status, headers, config) {
				if(data.meta.code == 200){
					$scope.isAddCommentSuccess = true;
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong, 2500);
				}else{
					$scope.isAddCommentSuccess = false;
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
				}
			})
			.error(function(data, status){
					$scope.isAddCommentSuccess = false;
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
			});
		}
		
		//interest login required
		function interest(){
			if(homeService.isLoggedIn()){
				var username = homeService.getCurrentLoginUser().id;
				var param = 'username='+username;
				var path = 'comment/share/'+ self.shareDetailId + '/interest/add';
				activeService.addInterest(path, param)
				.success(function(data, status, headers, config) {
					if(data.meta.code == 200){
						$scope.interest = data.data.info;
					}else{
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
					}
				})
				.error(function(data, status){
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
				});
			}else{
				homeService.requriedLogin();
			}
		}
		
		// check if login is required
		function checkLoginRequired($event){
			if(!homeService.isLoggedIn()){
				$event.target.blur();
				homeService.requriedLogin();
			}
		}
	}

	// check if a string is blank
	function isBlank(str){
		return !str || !/[^\s]+/.test(str);
	}
})();

