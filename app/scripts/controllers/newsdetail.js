;
(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:HomeCtrl
	 * @description # HomeCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp').controller('NewsDetailsCtrl', NewsDetailsCtrl);

	NewsDetailsCtrl.$inject = [ '$scope', '$stateParams', 'newsService',
			'activeService', 'CONSTANT', 'homeService', '$location', "$rootScope" ];

	function NewsDetailsCtrl($scope, $stateParams, newsService, activeService,
			CONSTANT, homeService, $location, $rootScope) {

		CONSTANT.lang = homeService.getLanguage();

		var self = this;

		/* Scope */
		$scope.newDetailCtrl = self;
		$scope.user = homeService.getCurrentLoginUser();
		$scope.interest = {};
		$scope.everyPage = 10;
		$scope.totalComment = 0;

		self.commentContent = '';
		self.replyCommentContent = '';

		// public properties
		self.newsId = $stateParams.newsId.substring($stateParams.newsId
				.lastIndexOf('-') + 1, $stateParams.newsId.length);
		self.listComment = [];

		// public method
		self.showHideCommentBox = showHideCommentBox;
		self.addReplyOfComment = addReplyOfComment;
		self.addComment = addComment;
		self.isBlank = isBlank;
		self.interest = interest;
		self.checkLoginRequired = checkLoginRequired;
		self.getListComment = getListComment;
		self.popupEmail = popupEmail;
		self.loading = false;

		/* init */
		getListComment(1);
		getNewsDetail();
		getRelatedArticles();
		countInterestByShareFeel();

		$scope.print = function(divName) {
			$('.wd-connect-machine').css('display', 'none');
			var printContents = document.getElementById(divName).innerHTML;
			// var originalContents = document.body.innerHTML;

			if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
				var popupWin = window
						.open('','_blank','width=800,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
				popupWin.window.focus();
				popupWin.document
						.write('<!DOCTYPE html><html><head>'
								+ '<link rel="stylesheet" type="text/css" href="style.css" />'
								+ '</head><body onload="">'
								+ '<div style="float:right;"><a href="javascript:;" onClick="window.print()">In trang</a>'
								+ '<span style="color:#B8B8B8;"> (Ctrl + p)</span></div>'
								+ '<div class="reward-body">' + printContents
								+ '</div></html>');
				popupWin.onbeforeunload = function(event) {
					popupWin.close();
					// return '.\n';
				};
				popupWin.onabort = function(event) {
					popupWin.document.close();
					popupWin.close();
				};

				popupWin.document.close();
			} else {
				var popupWin = window
						.open(
								'',
								'_blank',
								'width=800,height=600,scrollbars=yes,menubar=no,toolbar=no,location=yes,status=yes,titlebar=yes');
				popupWin.document.open();
				popupWin.document
						.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" />'
								+ '</head><body onload="">'
								+ '<div style="float:right;"><a href="javascript:;" onClick="window.print()">In trang</a>'
								+ '<span style="color:#B8B8B8;"> (Ctrl + p)</span></div>'
								+ printContents + '</html>');
				popupWin.document.close();
			}
			$('.wd-connect-machine').css('display', 'block');
			return true;
		};

		/* implements method */
		function getListComment(page) {
			var newCmtsApi = "comment/news/" + self.newsId + "/list";
			homeService.getDataByGetMethod(newCmtsApi, {
				'page' : page
			}).then(function(res) {
				self.listComment = res.data.data.items[0];
				$scope.totalComment = res.data.data.items[2];
			});
		}

		function getNewsDetail() {
			/* http://api.yourdanang.vn/a/articles/vn/44 */
			self.loading = true;
			newsService
				.getData('articles/' + CONSTANT.lang + '/' + self.newsId)
				.then(function(res) {
					$scope.newsDetail = res.data.info;
					// create meta header
	                homeService.checkMetaState(
	                        {"titlePage" : $scope.newsDetail.tieuDe, 
	                         "metaKeywords" : $scope.newsDetail.tieuDe, 
	                         "metaDescription" : $scope.newsDetail.tomTat, 
	                         "thumbNailUrl" : $scope.newsDetail.anhDaiDien.urlImage,
	                         "siteUrl" : $location.$$absUrl});
	                setTimeout(function() {
	    	 			$rootScope.htmlReady();
	    	 		}, 100);

	    	 		self.loading = false;
				});
		}

		function getRelatedArticles() {
			/* http://api.yourdanang.vn/a/articles/vn/44/related_articles */
			newsService.getData(
				'articles/' + CONSTANT.lang + '/' + self.newsId + '/tinTuc'
				+ '/related_articles')
				.then(function(res) {
				$scope.newsRelatedArticles = res.data.items;
			});
		}

		function showHideCommentBox(comment_box_id) {
			if ($('#' + comment_box_id).is(":visible")) {
				// visible do some than
				$('#' + comment_box_id).hide('slow');
			} else {
				// not visible do some else
				$('#' + comment_box_id).show('slow');
			}
		}

		function addReplyOfComment(comment) {
			if ($scope.user != null) {
				var username = $scope.user.id;
				if (self.isBlank(comment.reply)) {
					homeService
							.showNotification(
									homeService
											.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan,
									2500);
				} else {
					// comment/share/{id}/reply/{idComment}/add
					var param = 'username=' + username + '&content='
							+ comment.reply;
					var path = 'comment/news/' + self.newsId + '/reply/'
							+ comment.id + '/add';
					activeService.addComment(path, param).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
							comment.reply = '';
							homeService.showNotification(
											homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong,2500);
						} else {
							homeService.showNotification(
											homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,2500);
						}
					}).error(function(data, status) {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi,2500);
					});
				}
			} else {
				homeService.requriedLogin();
			}
		}

		function addComment() {
			if ($scope.user != null) {
				var username = $scope.user.id;
				if (self.isBlank(self.commentContent)) {
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan,2500);
				} else {
					var param = 'username=' + username + '&content='
							+ self.commentContent;
					var path = 'comment/news/' + self.newsId + '/add';
					activeService.addComment(path, param).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
							self.commentContent = '';
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong,2500);
						} else {
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,2500);
						}
					}).error(function(data, status) {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi,2500);
					});
				}
			} else {
				homeService.requriedLogin();
			}
		}

		// get count interest of share feel
		function countInterestByShareFeel() {
			var username = '';
			if (homeService.isLoggedIn()) {
				username = homeService.getCurrentLoginUser().id;
			}
			var param = 'username=' + username;
			// /comment/news/{id}/interest/count
			var path = 'comment/news/' + self.newsId + '/interest/count';

			activeService.addInterest(path, param).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
							$scope.interest = data.data.info;
						} else {
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,2500);
						}
					}).error(function(data, status) {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi,2500);
					});
		}

		// interest login required
		function interest() {
			if (homeService.isLoggedIn()) {
				var username = homeService.getCurrentLoginUser().id;
				var param = 'username=' + username;
				var path = 'comment/news/' + self.newsId + '/interest/add';
				activeService.addInterest(path, param).success(function(data, status, headers, config) {
							if (data.meta.code == 200) {
								$scope.interest = data.data.info;
							} else {
								homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,2500);
							}
						}).error(function(data, status) {
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi,2500);
						});
			} else {
				homeService.requriedLogin();
			}
		}

		// check if a string is null or empty or blank?
		function isBlank(str) {
			return !str || !/[^\s]+/.test(str);
		}

		// check if login is required
		function checkLoginRequired($event) {
			if (!homeService.isLoggedIn()) {
				$event.target.blur();
				homeService.requriedLogin();
			}
		}

		// mail to
		function popupEmail() {
			var link = $location.absUrl();
			if ($scope.newsDetail != null) {
				homeService.openShareEmailPopup($scope.newsDetail.tieuDe, link);
			}
		}
	};
})();