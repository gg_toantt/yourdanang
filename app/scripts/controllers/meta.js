;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:ConfigMetaCtrl
     * @description # ConfigMetaCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('MetaCtrl', metaCtrl);
    metaCtrl.$inject = [ '$scope', '$timeout', 'MetaService', 'CONSTANT',
            '$state', '$rootScope' ];

    function metaCtrl($scope, $timeout, metaService, CONSTANT, $state, $rootScope) {
        $scope.getMeta = function() {  
            $rootScope.metaData;
        };
    }
})();