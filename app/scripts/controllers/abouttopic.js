;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('AboutTopicCtrl', aboutTopicCtrl);

    aboutTopicCtrl.$inject = [ '$scope', '$stateParams', 'aboutService', 'CONSTANT', 'homeService', '$rootScope', '$location'];

    function aboutTopicCtrl($scope, $stateParams, aboutService, CONSTANT, homeService, $rootScope, $location) {    	
		CONSTANT.lang = homeService.getLanguage();
    	
        var self = this;
        // public properties
        self.topic = $stateParams.catId;
        self.page = 1;
        self.lang = CONSTANT.lang;
        self.keywords = '';
        // public methods
        self.getListAboutTopic = getListAboutTopic;
        
        /* Scope */

        $scope.aboutTpCtrl = self;
        $scope.aboutTopics = [];
        $scope.everyPage = 10;
        $scope.total = 0;
        $scope.aboutTopicsName = '';
        $rootScope.keywordSuggesAbout = '';
        $scope.placeholder = 'Nhập từ khóa cần tìm';
        if(self.topic == 'le-hoi-truyen-thong') {
            $scope.placeholder = 'Nhập lễ hội tìm kiếm';
        }
        if(self.topic == 'dia-danh') {
            $scope.placeholder = 'Nhập địa danh tìm kiếm';
        } else if(self.topic == 'danh-nhan-duong-pho') {
            $scope.placeholder = 'Nhập đường phố tìm kiếm';
        }
        if (self.topic == 'thanh-pho-dang-song') {
            $scope.placeholder = 'Nhập thành phố tìm kiếm';   
        }

        /* init */
        getListAboutTopic(self.page);    
        
        /* implements method */
        function getListAboutTopic(numberPage) {
            $scope.isLoaded = false;
            // var listAboutTopicApi = 'articles/'+ CONSTANT.lang +'/topic/alias/' + self.topic + "?page="+ numberPage + 
            //     "&keyword=" + $rootScope.keywordSuggesAbout;
            if (angular.isUndefined(self.keywords) || self.keywords == null) {
                self.keywords = '';
            }
            var listAboutTopicApi = 'services/'+ CONSTANT.lang +'/news/searchsSugges?cate='+self.topic+
                "&page="+numberPage+'&keyword=' + homeService.removeSpecialLetter(self.keywords);
            aboutService.getData(listAboutTopicApi).then(function(res) {
            	self.page = numberPage;
                $scope.total = res.data.items.count * $scope.everyPage;
                if (res.data.items.data.length > 0) {
         			$scope.aboutTopicsName =  res.data.items.data[0].chuDe.ten;
         		}
         		$scope.aboutTopics = res.data.items.data;
                if ($scope.aboutTopics.length > 0) {
                    angular.forEach($scope.aboutTopics, function(value, index) {
                        $scope.aboutTopics[index].tieuDe = highlightText($scope.aboutTopics[index].tieuDe);
                    });
                }
                $scope.isLoaded = true;
         		// create meta header
                var metaJson = {};
                if(!angular.isUndefined($scope.aboutTopics[0])){
                    metaJson = {"titlePage" : $scope.aboutTopics[0].chuDe.ten, 
                        "metaKeywords" : $scope.aboutTopics[0].chuDe.ten, 
                        "metaDescription" : $scope.aboutTopics[0].chuDe.moTa, 
                        "thumbNailUrl" : "",
                        "siteUrl" : $location.$$absUrl};
                
                }else{
                    metaJson = {"titlePage" : "Về Đà Nẵng", 
                            "metaKeywords" : "Về Đà Nẵng", 
                            "metaDescription" : "Về Đà Nẵng", 
                            "thumbNailUrl" : "",
                            "siteUrl" : $location.$$absUrl};
                }
                homeService.checkMetaState(metaJson);
                setTimeout(function() {
    	 			$rootScope.htmlReady();
    	 		}, 100);
            });
        }


        //Spiderman search technology
        function subStringBySize(text, size) {
            if(text != null && text != '') {
                var l = text.length;
                var index = size > l ? l : size;
                while (index < l && ' ' != text.charAt(index)) {
                    index++;
                }
                var tail = index < l ? " ..." : "";
                return text.substring(0, index) + tail;
            }
            return "";
        }

        function highlightText(text) {
            var text1 = text;
            var beginInsertTag = '<b class="search-keyword">';
            var endInsertTag = '</b>';
            if (!angular.isUndefined(self.keywords) && self.keywords != null && self.keywords != '') {
                var key = homeService.removeSpecialLetterNoTrim(self.keywords).split(' ');

                for (var k = 0; k < key.length; k++) {
                    if (key[k] != '') {
                        var keyWord = key[k];
                        var kl = keyWord.length;

                        var tmp = homeService.removeSpecialLetterNoTrim(text1).split(' ');
                        for (var m = 0; m < tmp.length; m++) {
                            if (tmp[m] == homeService.removeSpecialLetterNoTrim(keyWord)) {
                                var i = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord));
                                var j = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord + ':'));
                                var n = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord + '.'));

                                if (i > -1) {
                                    text1 = insertString(text1, beginInsertTag, i);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+i+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+i+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+i+kl+endInsertTag.length)));
                                }

                                if (j > -1) {
                                    text1 = insertString(text1, beginInsertTag, j);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+j+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+j+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+j+kl+endInsertTag.length)));
                                }

                                if (n > -1) {
                                    text1 = insertString(text1, beginInsertTag, n);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+n+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+n+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+n+kl+endInsertTag.length)));
                                }
                            }
                        }
                        
                    }
                }
            }
            
            return text1;
        }

        function insertString(text, insertString, index) {
            var s1 = text.substring(0,index);
            var s2 = text.substring(index);
            return s1.concat(insertString).concat(s2);
        }
    }
})();