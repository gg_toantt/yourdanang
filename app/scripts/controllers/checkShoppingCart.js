;(function(){
	'use strict';
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:CheckShoppingCartCtrl
	 * @description # CheckShoppingCartCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp')
	.controller('CheckShoppingCartCtrl', checkShoppingCartCtrl);
	checkShoppingCartCtrl.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT', '$state', 'Scopes' ];
	
	function checkShoppingCartCtrl($scope, $timeout, homeService, CONSTANT, $state, Scopes) {
		Scopes.store('CheckShoppingCartCtrl', $scope);
		
		if ($("#notifyQuantity").text() == 0) {
			$state.go('home', {});
		}
	}
})();