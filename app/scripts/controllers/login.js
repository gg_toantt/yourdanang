﻿;
(function() {
	'use strict';

	angular.module('nddkApp').factory('Scopes', function($rootScope) {
		var mem = {};

		return {
		    store : function(key, value) {
			    $rootScope.$emit('scope.stored', key);
			    mem[key] = value;
		    },
		    get : function(key) {
			    return mem[key];
		    }
		};
	}).config(function(FacebookProvider) {
		//FacebookProvider.init('740849662690314');
		FacebookProvider.init('174327009577654');
	}).config([ 'GooglePlusProvider', function(GooglePlusProvider) {
		GooglePlusProvider.init({
		    clientId : '549480166959-2ak92gkk30ggnhr073lrf3cq6n9qps5t.apps.googleusercontent.com',
		    apiKey : 'AIzaSyDrm4GhIVJ1o-b3i8DDibi4f8DzF-UozVQ'
		});
	} ]).controller('LoginCtrl', LoginCtrl);

	LoginCtrl.$inject = [ '$scope', 'ngDialog', 'Scopes', 'registrationService', '$cookieStore', 'Facebook', 'GooglePlus', '$stateParams', 'homeService', '$state', '$location',
	        'ipCookie', 'CONSTANT', '$timeout' ];

	function LoginCtrl($scope, ngDialog, Scopes, registrationService, $cookieStore, Facebook, GooglePlus, $stateParams, homeService, $state, $location, ipCookie, CONSTANT, $timeout) {
		CONSTANT.lang = homeService.getLanguage();

		hello.init({facebook: '740849662690314'});
		
		$scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
		Scopes.store('LoginCtrl', $scope);
		$scope.user = ipCookie('user');
		$scope.email = '';
		$scope.passwordNew = '';
		$scope.passwordNew_confirm = '';
		$scope.msgConfirm = false;
		$scope.userProfile = {};
		$scope.imageProfileUpload = null;
		$scope.btnUpdateImage = false;
		
		if (angular.isUndefined($scope.user)) {
			$scope.user = null;
		}

		if ($scope.user != null) {
			homeService.getData('users/' + $scope.user.id).then(function(res) {
				$scope.userProfile = res.data.info;
			});
		}

		$scope.uploadChange = function(file) {
			if (window.File && window.FileReader && window.FileList && window.Blob) {			
				handleFileSelect(file.files);
			} else {
				alert('The File APIs are not fully supported in this browser.');
			}
		}
		
		var handleFileSelect = function(files) {
			//var files = evt.target.files;
			var file = files[0];
			
			if (files && file) {
				var reader = new FileReader();					
				reader.onload = function(readerEvt) {
					var binaryString = readerEvt.target.result;
					var base64String  = btoa(binaryString) ;
					var typeImage = getExtensionImage();
					if (checkSize(2097152)) {
						sendAjaxToJava(base64String, typeImage);
					} else {
						alert("Dung lượng quá lớn vui lòng bạn lựa chọn lại hình ảnh");
					}
					
				};
				reader.readAsBinaryString(file);
			}			
		};
		
		function sendAjaxToJava(encodeImage, typeImage) {
			$("#myImageFile").hide();
			$(".wd-avatar-prfct").addClass("loading-image");
			var dataImageUpload = 'image_encode=' + encodeImage + '&type=' + typeImage;

			registrationService.loginUser('users/save_image', dataImageUpload).success(function(data, status, headers, config) {
				if (data.meta.code == 200) {
					$scope.imageProfileUpload = data.data.info.pathImage;
					$(".wd-avatar-prfct").removeClass("loading-image");
					$("#myImageFile").attr("src", data.data.info.pathImage);
					$("#myImageFile").show();
					
					$scope.submittedImage = true;
				} else {
//						console.log("code != 200");
//						console.log(data);
				}
			}).error(function(data, status) {
//					console.log("Error: " + data);
			});
		}
		function getExtensionImage() {
			var fullNameImage = document.getElementById("filePicker").value;
			var imageArray = fullNameImage.split(".");
			var typeImage = imageArray[imageArray.length - 1];
//				console.log(typeImage);
			return typeImage;
		}
		function checkSize(max_img_size) {
			var input = document.getElementById("filePicker");
			// check for browser support (may need to be modified)
			if (input.files && input.files.length == 1) {
				if (input.files[0].size > max_img_size) {
					return false;
				}
			}
			return true;
		}

		// if ($scope.user != null && $scope.user.businessTaxID == null) {
		// 	$scope.businessName = 'null';
		// 	$scope.businessTaxID = 'null';
		// 	$scope.businessAddress = 'null';
		// 	$scope.businessEmail = 'null';
		// 	$scope.businessPhone = 'null';
		// }
		
		$scope.submittedProfile = false;
		$scope.msgConfirmProfile = false;
		$scope.updateMe = function() {
			$scope.submittedProfile = true;
			
			if ($scope.myForm.$valid) {

				if ($scope.password != $scope.password_confirm) {
					$scope.msgConfirmProfile = true;
				} else {
					
					if (!($scope.user.business ^ $scope.businessForm.$valid)) {
						$scope.msgConfirmProfile = false;
					var updateProfile;
					
					if ($scope.user != null && $scope.user.business) {
						updateProfile = 'fullName=' + $scope.fullName + '&username=' + $scope.user.email + '&email=' + $scope.user.email
					    	//+ '&profileImageUR=' + $scope.imageProfileUpload
					    	+ '&businessName=' + $scope.businessName
					    	+ '&businessAddress=' + $scope.businessAddress
					    	+ '&businessPhone=' + $scope.businessPhone
					    	+ '&isBusiness=' + true;
					} else {
						updateProfile = 'fullName=' + $scope.fullName + '&username=' + $scope.user.email + '&email=' + $scope.user.email
				    	//+ '&profileImageUR=' + $scope.imageProfileUpload
				    	+ '&businessName=' + null
				    	+ '&businessAddress=' + null
				    	+ '&businessPhone=' + null
				    	+ '&isBusiness=' + false;
					}
					
					var updatePassword = 'password=' + $scope.password
						+ '&password_confirm=' + $scope.password_confirm;
					registrationService.loginUser('users/update_profile/' + $scope.user.id, updateProfile).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
//							console.log("code == 200");
							registrationService.loginUser('users/' + $scope.user.id + '/password', updatePassword);
							// $cookieStore.put('user', data.data.info);
							ipCookie('user', data.data.info);
							Scopes.get('LoginCtrl').user = data.data.info;

							$state.go('profile');
							setTimeout(function() {
								location.reload();
							}, 100);
						} else {
//							console.log("code != 200");
							// $scope.msg = data.meta.message;
						}
					}).error(function(data, status) {
//						console.log("Error: " + data);
					});
					}
					
				}
			}

		};
		
	
		$scope.updateImage = function() {
				var updateProfile;

				if ($scope.imageProfileUpload == null) {
				} else {
					updateProfile = 'profileImageUR=' + $scope.imageProfileUpload;
					var img = 'I : ' +$scope.imageProfileUpload;
					
					var updatePassword = 'password=' + $scope.password;
					registrationService.loginUser('users/update_img/' + $scope.user.id, updateProfile).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
							registrationService.loginUser('users/' + $scope.user.id + '/password', updatePassword);
							// $cookieStore.put('user', data.data.info);
							ipCookie('user', data.data.info);
							Scopes.get('LoginCtrl').user = data.data.info;

							$state.go('profile');
							setTimeout(function() {
								location.reload();
							}, 100);
						} else {
//							console.log("code != 200");
							// $scope.msg = data.meta.message;
						}
					}).error(function(data, status) {
//						console.log("Error: " + data);
					});
				}	
		};

		$scope.logout = function() {
			// Facebook.logout();
			hello('facebook').logout().then(function() {
			}, function(e) {
			});
			GooglePlus.logout();
			registrationService.logoutUser('auth/logout', 'access_token=' + $scope.user.access_token).success(function(data, status, header, configs) {
				if (data.meta.code == 200) {
					$scope.user = null;
					ipCookie.remove('user');
				} else {
//					console.log('log out khong ok');
				}
			});
			ipCookie.remove('user');
			location.reload();
		};

		$scope.sendEmail = function() {
			$scope.submitedSendEmail = true;
			$scope.msgSendEmailSuccess = '';
			if ($scope.email != '') {
//				console.log($scope.email);
				registrationService.loginUser('users/forget_password', 'email=' + $scope.email).success(function(data, status, headers, config) {
					if (data.meta.code == 200) {
//						console.log(data);
						$scope.msgSendEmailSuccess = 'Chúng tôi đã gởi thông tin lấy lại mật khẩu đến địa chỉ email ' + $scope.email;

					} else {
						$scope.msgSendEmailSuccess = '';
//						console.log(data);
						// $scope.msg = data.meta.message;
					}
				}).error(function(data, status) {
					$scope.msgSendEmailSuccess = '';
//					console.log("Error: " + data.meta.message);
				});
			}

		};

		$scope.changePassword = function() {
			$scope.submitedChange = true;
			$scope.msgChangePass = '';
			$scope.msgChangePass2 = '';
			$scope.msgChangePass3 = '';
			if ($scope.passwordNew != '' && $scope.passwordNew_confirm != '') {
				if ($scope.passwordNew != $scope.passwordNew_confirm) {
					$scope.msgConfirm = true;
				} else {
//					console.log($scope.code);
					var changePass = 'codeChangePass=' + $stateParams.code + '&password=' + $scope.passwordNew + '&passwordConfirm=' + $scope.passwordNew_confirm;
					registrationService.loginUser('users/change_password_forget', changePass).success(function(data, status, headers, config) {
						if (data.meta.code == 200) {
							$scope.msgChangePass = 'Thay đổi mật khẩu thành công. Click';
							$scope.msgChangePass2 = ' đây';
							$scope.msgChangePass3 = ' để đăng nhập';
//							console.log(data);
						} else {
							$scope.msgChangePass = '';
							$scope.msgChangePass2 = '';
							$scope.msgChangePass3 = '';
//							console.log(data);
							// $scope.msg = data.meta.message;
						}
					}).error(function(data, status) {
						$scope.msgChangePass = '';
						$scope.msgChangePass2 = '';
						$scope.msgChangePass3 = '';
//						console.log("Error: " + data.meta.message);
					});
				}
			}
		};

		$scope.openPopup = function() {
			ngDialog.open({
			    template : 'templateLogin',
			    className : 'ngdialog-theme-default custom-width-login-popup',
			    controller : [
			            '$scope',
			            'registrationService',
			            'Scopes',
			            '$cookieStore',
			            'ipCookie', 'homeService', 'CONSTANT',
			            function($scope, registrationService, Scopes, $cookieStore, ipCookie, homeService, CONSTANT) {
			            	
			            	// margin center dialog after created
			            	homeService.locateDialog();
			            	
			        		CONSTANT.lang = homeService.getLanguage();
//			        		console.log(CONSTANT.lang);
			        		$scope.language = homeService.updateLanguageByCode(CONSTANT.lang);
			            	
				            $scope.userName = "";
				            $scope.password = "";
				            $scope.checkLogin = false;

				            $scope.count = 0;
				            $scope.check = function() {
					            if ($scope.count % 2 == 0) {
						            $scope.checkLogin = true;
						            $scope.count += 1;
					            } else {
						            $scope.count += 1;
						            $scope.checkLogin = false;
					            }
				            };

				            $scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
				            var self = this;
				            $scope.loginSys = function() {
					            $scope.submited = true;

					            if ($scope.userName != '' && $scope.password != '') {
						            var userLogin = 'email=' + $scope.userName + '&password=' + $scope.password;

						            registrationService.loginUser('auth/login', userLogin).success(function(data, status, headers, config) {
							            if (data.meta.code == 200) {
//								            console.log("Code == 200 " + data.meta.message);
								            Scopes.get('LoginCtrl').user = data.data.info;
								            // $cookieStore.put('user',
											// data.data.info);
								            if ($scope.checkLogin == true) {
									            ipCookie('user', data.data.info, {
										            expires : 365
									            });
								            } else {
									            ipCookie('user', data.data.info);
								            }
								            console.log(data.data.info);
								            ngDialog.close();
								            location.reload();
							            } else {
								            $scope.msg = data.meta.message;
//								            console.log("Error2: " + data.meta.message);
							            }
						            }).error(function(data, status) {
//							            console.log("Error1: " + data.meta.message);
						            });
					            }

				            };

				            $scope.loginFacebook = function() {

				            	hello.login('facebook',function(res){
									if(res.authResponse){
										hello('facebook').api('/me?fields=id,email,gender,first_name,last_name',function(response){
											var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
								                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
								                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

								            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
									            if (data.meta.code == 200) {
										            Scopes.get('LoginCtrl').user = data.data.info;
										            ipCookie('user', data.data.info, {
											            expires : 365
										            });
										            ngDialog.close();
										            location.reload();
									            } else {
										            $scope.msg = data.meta.message;
									            }
								            }).error(function(data, status) {
								            });
										});
									}
								});
				            };
				            
				            $scope.loginFacebook1 = function() {
					            Facebook.getLoginStatus(function(response) {
					            	console.log("loginFacebook");
						            if (response.status == 'connected') {
							            Facebook.api('/me', function(response) {
								            var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
								                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
								                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

								            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
									            if (data.meta.code == 200) {
										            Scopes.get('LoginCtrl').user = data.data.info;
										            // $cookieStore.put('user',
													// data.data.info);
										            ipCookie('user', data.data.info, {
											            expires : 365
										            });
										            ngDialog.close();
										            location.reload();
									            } else {
										            $scope.msg = data.meta.message;
									            }
								            }).error(function(data, status) {
								            });
							            });
						            } else {
							            Facebook.login(function(response) {
							            	alert(response.status);
								            if (response.status == 'connected') {
									            Facebook.api('/me', function(response) {
										            var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
										                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
										                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

										            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
											            if (data.meta.code == 200) {
												            Scopes.get('LoginCtrl').user = data.data.info;
												            // $cookieStore.put('user',
															// data.data.info);
												            ipCookie('user', data.data.info, {
													            expires : 365
												            });
												            ngDialog.close();
												            location.reload();
											            } else {
												            $scope.msg = data.meta.message;
											            }
										            }).error(function(data, status) {
										            });
									            });
								            }
							            });
						            }
					            });
				            };

				            $scope.loginGoogle = function() {
					            GooglePlus.login().then(
					                function(authResult) {
						                GooglePlus.getUser().then(
						                    function(user) {
							                    var userGoogle = 'username=' + null + '&firstname=' + user.family_name + '&lastname=' + user.given_name + '&email=' + null
							                        + '&gender=' + user.gender + '&profileImageUR=' + user.picture + '&googleID=' + user.id;
							                    registrationService.loginUser('auth/google_login', userGoogle).success(function(data, status, headers, config) {
								                    if (data.meta.code == 200) {
									                    Scopes.get('LoginCtrl').user = data.data.info;
									                    // $cookieStore.put('user',
														// data.data.info);
									                    ipCookie('user', data.data.info, {
										                    expires : 365
									                    });
									                    ngDialog.close();
									                    location.reload();
								                    } else {
									                    $scope.msg = data;
								                    }
							                    }).error(function(data, status) {
//								                    console.log("Error: " + data);
							                    });
						                    });
					                }, function(err) {
//						                console.log(err);
					                });
				            };

			            } ]
			});
		};

		$scope.loginFacebookAtRegister = function() {
        	hello.login('facebook',function(res){
				if(res.authResponse){
					hello('facebook').api('/me?fields=id,email,gender,first_name,last_name',function(response){
						var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
			                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
			                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

			            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
				            if (data.meta.code == 200) {
					            Scopes.get('LoginCtrl').user = data.data.info;
					            ipCookie('user', data.data.info, {
						            expires : 365
					            });
					            ngDialog.close();
					            location.reload();
				            } else {
					            $scope.msg = data.meta.message;
				            }
			            }).error(function(data, status) {
			            });
					});
				}
			});
		}

		$scope.loginFacebookAtRegister1 = function() {
            Facebook.getLoginStatus(function(response) {
	            if (response.status == 'connected') {
	            	Facebook.api('/me', function(response) {
			            var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
			                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
			                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

			            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
				            if (data.meta.code == 200) {
//												            console.log("Code == 200 " + data.meta.message);
					            Scopes.get('LoginCtrl').user = data.data.info;
					            // $cookieStore.put('user',
								// data.data.info);
					            ipCookie('user', data.data.info, {
						            expires : 365
					            });
					            ngDialog.close();
					            location.reload();
				            } else {
					            $scope.msg = data.meta.message;
				            }
			            }).error(function(data, status) {
//											            console.log("Error: " + data.meta.message);
			            });
		            });
	            } else {
		            Facebook.login(function(response) {
			            if (response.status == 'connected') {
				            Facebook.api('/me', function(response) {
					            var userFacebook = '{\"id\":\"' + response.id + '\",\"first_name\":\"' + response.first_name + '\",\"last_name\":\"'
					                + response.last_name + '\",\"name\":\"' + null + '\",\"gender\":\"' + response.gender + '\",\"picture\":\"'
					                + 'http://graph.facebook.com/' + response.id + '/picture\"}';

					            registrationService.loginUserFacebook('auth/facebook_login?p=' + userFacebook).success(function(data, status, headers, config) {
						            if (data.meta.code == 200) {
//												            console.log("Code == 200 " + data.meta.message);
							            Scopes.get('LoginCtrl').user = data.data.info;
							            // $cookieStore.put('user',
										// data.data.info);
							            ipCookie('user', data.data.info, {
								            expires : 365
							            });
							            ngDialog.close();
							            location.reload();
						            } else {
							            $scope.msg = data.meta.message;
						            }
					            }).error(function(data, status) {
//											            console.log("Error: " + data.meta.message);
					            });
				            });
			            }
		            });
	            }
            });
        };

        $scope.loginGoogleAtRegister = function() {
            GooglePlus.login().then(
                function(authResult) {
	                GooglePlus.getUser().then(
	                    function(user) {
		                    var userGoogle = 'username=' + null + '&firstname=' + user.family_name + '&lastname=' + user.given_name + '&email=' + null
		                        + '&gender=' + user.gender + '&profileImageUR=' + user.picture + '&googleID=' + user.id;
		                    registrationService.loginUser('auth/google_login', userGoogle).success(function(data, status, headers, config) {
			                    if (data.meta.code == 200) {
//									                    console.log("Code == 200 " + data);
				                    Scopes.get('LoginCtrl').user = data.data.info;
				                    // $cookieStore.put('user',
									// data.data.info);
				                    ipCookie('user', data.data.info, {
					                    expires : 365
				                    });
				                    ngDialog.close();
				                    location.reload();
			                    } else {
				                    $scope.msg = data;
			                    }
		                    }).error(function(data, status) {
//								                    console.log("Error: " + data);
		                    });
	                    });
                }, function(err) {
//						                console.log(err);
                });
        };
		
		$scope.openPopupLoginPublic = function() {
			setTimeout(function (){
				ngDialog.open({
				    template : 'templateLoginPublic',
				    className : 'ngdialog-theme-default custom-width-login-popup-public',
				    closeByDocument: false,
				    closeByEscape : false,
				    showClose : false,
				    controller : [
				            '$scope',
				            'registrationService',
				            'Scopes',
				            '$cookieStore',
				            'ipCookie', 'homeService', 'CONSTANT',
				            function($scope, registrationService, Scopes, $cookieStore, ipCookie, homeService, CONSTANT) {
				            	
				            	setTimeout(function () {
				            		// margin center dialog after created
				        			homeService.locateDialog();
				        		}, 200);
				            	
				        		CONSTANT.lang = homeService.getLanguage();
//				        		console.log(CONSTANT.lang);
				        		$scope.language = homeService.updateLanguageByCode(CONSTANT.lang);
				            	
					            $scope.userName = "";
					            $scope.password = "";
					            $scope.checkLogin = false;

					            $scope.count = 0;
					            $scope.check = function() {
						            if ($scope.count % 2 == 0) {
							            $scope.checkLogin = true;
							            $scope.count += 1;
						            } else {
							            $scope.count += 1;
							            $scope.checkLogin = false;
						            }
					            };

					            $scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
					            var self = this;
					            $scope.loginSys = function() {
						            $scope.submited = true;

						            if ($scope.userName != '' && $scope.password != '') {
							            var userLogin = 'email=' + $scope.userName + '&password=' + $scope.password;

							            registrationService.loginUser('auth/login', userLogin).success(function(data, status, headers, config) {
								            if (data.meta.code == 200) {
//									            console.log("Code == 200 " + data.data.info);
//								            	console.log(data.data.info);
									            Scopes.get('LoginCtrl').user = data.data.info;
									            // $cookieStore.put('user',
												// data.data.info);
									            if ($scope.checkLogin == true) {
										            ipCookie('user', data.data.info, {
											            expires : 365
										            });
									            } else {
										            ipCookie('user', data.data.info);
									            }
									            
									            if (homeService.checkLoginPublic()) {
										            $state.go('home', {});
										            ngDialog.close();
									            }
								            } else {
									            $scope.msg = data.meta.message;
//									            console.log("Error2: " + data.meta.message);
								            }
							            }).error(function(data, status) {
//								            console.log("Error1: " + data.meta.message);
							            });
						            }

					            };

				            } ]
				});
			}, 200);
		};
	}

})();