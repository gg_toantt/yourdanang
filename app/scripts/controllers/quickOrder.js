;
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name nddkApp.controller:QuickOrderCtrl
   * @description # QuickOrderCtrl Controller of the nddkApp
   */
  angular.module('nddkApp').controller('QuickOrderCtrl', QuickOrderCtrl)
  .config(['noCAPTCHAProvider', function (noCaptchaProvider) {
      noCaptchaProvider.setSiteKey('6LeQkwgTAAAAAIIk8kKnChhlGBwVvDtCsW_cDVQG');
      noCaptchaProvider.setTheme('dark');
  }]);

  QuickOrderCtrl.$inject = [ '$scope', 'homeService', 'CONSTANT', '$timeout', 'ipCookie',];

  function QuickOrderCtrl($scope, homeService, CONSTANT, $timeout, ipCookie) {

    var self = this;
    // properties
    self.contentOrder = '';
    self.contactOrder = '';
    self.showMessageError = '';
    self.showMessageSuccess = '';
    self.showCapcha = false;
    self.numSendOrder = 0;
    self.captchaEmpty = false;
    self.loading = false;

    if (angular.isUndefined(ipCookie('quickOrder')) || ipCookie('quickOrder') == null) {
        ipCookie('quickOrder', self.numSendOrder, { expires : 1 });
        self.numSendOrder = ipCookie('quickOrder');
        self.showCapcha = false;
    } else  {
        self.numSendOrder = ipCookie('quickOrder');
        if (self.numSendOrder >= 3) {
            self.showCapcha = true;
        }
    }

    // public properties
    self.sendQuickOrder = sendQuickOrder;

    function sendQuickOrder() {
        self.loading = true;
        self.showMessageError = '';
        if (angular.isUndefined(self.contentOrder) ||  self.contentOrder == '') {
            self.showMessageError = 'Vui lòng nhập nội dung đặt hàng!';
        } else if (angular.isUndefined(self.contactOrder) ||  self.contactOrder == '') {
            self.showMessageError = 'Vui lòng nhập thông tin liên hệ!';
        } else {
            self.showMessageError = '';
            if (!self.showCapcha || !angular.isUndefined($scope.gRecaptchaResponse)) {
                self.captchaEmpty = false;
                var params = 'notes=' + self.contentOrder + '&infor=' + self.contactOrder;
                homeService.postData('quickOrders', params).success(function(res) {
                    if (res.meta.code == 200) {
                        self.contentOrder = '';
                        self.contactOrder = '';
                        self.showMessageSuccess = 'Gửi thông tin thành công!';
                        $timeout(function() {
                            self.showMessageSuccess = '';
                        }, 3000);
                    }

                    if (ipCookie('quickOrder') <= 3) {
                        self.numSendOrder = ipCookie('quickOrder');
                        ipCookie('quickOrder', (self.numSendOrder + 1), { expires : 1 });
                    }
                    if (ipCookie('quickOrder') >= 3) {
                        
                        self.showCapcha = true;
                        if (ipCookie('quickOrder') > 3) {
                          $scope.noCaptchaControl.reset();
                          $scope.gRecaptchaResponse = undefined;
                        }
                    }
                    self.loading = false;
                });

            } else {
                self.showMessageError = '';
                self.captchaEmpty = true;
            }
           
            self.loading = false;
        }
        self.loading = false;
    }

  }

})();