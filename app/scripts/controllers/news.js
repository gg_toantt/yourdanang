;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('NewsCtrl', NewsCtrl);

    NewsCtrl.$inject = [ '$scope', 'newsService', 'CONSTANT', '$timeout', 'homeService', '$sce','ngDialog', '$state', '$rootScope'];
    function NewsCtrl($scope, newsService, CONSTANT, $timeout, homeService, $sce, ngDialog, $state, $rootScope) {

		CONSTANT.lang = homeService.getLanguage();
    	var self = this;

    	/* Scope */
    	$scope.newsCtrl = self;
    	$scope.lastestNews = [];
    	$scope.promotions = [];
    	$scope.eventHighlight = [];
    	$scope.events = [];
    	$scope.newsGeneralLarge = [];
    	$scope.newsGeneralMedium = [];
    	$scope.newsGenerals = [];
    	$scope.videos = [];
    	$scope.newCtrl = self;
    	$scope.API = null;

    	// public properties

    	//public method
        self.showPopupVideo = showPopupVideo;

    	 /* init */
    	getLastestNews();
    	getPromotions();
    	getEventHighlight();
    	getEvents();
    	getNewsGenerals();
    	getVideos();

        /* implements method */
        function getLastestNews() {
        	/* http://api.yourdanang.vn/a/articles/vn/5_newest_articles */
        	newsService.getData('articles/' + CONSTANT.lang + '/5_newest_articles').then(function(res) {
        		$scope.lastestNews = res.data.items[0];
        	});

        	// check meta data
        	homeService.checkMetaState();
        	setTimeout(function() {
  	 			$rootScope.htmlReady();
//  	 			console.log('new');
  	 		}, 100);
        }

        function getPromotions() {
        	/* http://api.yourdanang.vn/a/articles/vn/promotion */
        	newsService.getData('articles/' + CONSTANT.lang + '/promotion').then(function(res) {
        		$scope.promotions = res.data.items;
        	});
        }

        function getEventHighlight() {
        	/* http://api.yourdanang.vn/a/articles/vn/event_highlight */
        	newsService.getData('articles/' + CONSTANT.lang + '/event_highlight').then(function(res) {
        		$scope.eventHighlight = res.data.info;
        	});
        }

        function getEvents() {
        	/* http://api.yourdanang.vn/a/articles/vn/event */
        	newsService.getData('articles/' + CONSTANT.lang + '/event').then(function(res) {
        		$scope.events = res.data.items.slice(0, 3);
        	});
        }

        function getNewsGenerals() {
        	/* http://api.yourdanang.vn/a/articles/vn/general */
        	newsService.getData('articles/' + CONSTANT.lang + '/general').then(function(res) {
        		$scope.newsGeneralLarge = res.data.items[0];
        		$scope.newsGeneralMedium = res.data.items.slice(1, 3);
        		$scope.newsGenerals = res.data.items.slice(3, res.data.items.length);
        	});
        }

        function getVideos() {
        	/* http://api.yourdanang.vn/a/video/highligh */
        	newsService.getData('video/highligh').then(function(res) {
        		$scope.videos = res.data.info;
        		angular.element(".video-item").colorbox({inline:true, opacity :"0.8"});
        		$timeout(function(){
        			angular.element('#contListVideo').carouFredSel({
        				responsive: true,
        				auto: false,
        				width: '100%',
        				prev: '#prev-video',
        				next: '#next-video',
        				pagination: false,
        				scroll : {
        					items        : 1,
        					pauseOnHover : true
        				},
        				items: {
        					visible: {
        						min: 1,
        						max: 5
        					}
        				},
        				mousewheel: true,
        				swipe: {
        					onMouse: true,
        					onTouch: true
        				}
        			});

    	 		},200);
        	});
        }

        function showPopupVideo(video){
        	ngDialog.open(
        		{
                    template : 'popupVideo',
                    className : 'ngdialog-theme-default custom-width-share-popup',
                    closeByDocument:false,
                    data : {'video':video},
                    controller: ['homeService',
         			            function(homeService) {
                     	// margin center dialog after created
 		            	homeService.locateDialog();
                     }]
                }
        	);
        }

    }
    ;
})();
