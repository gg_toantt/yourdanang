;
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name nddkApp.controller:WarningCtrl
   * @description # WarningCtrl Controller of the nddkApp
   */
  angular.module('nddkApp').controller('WarningCtrl', WarningCtrl);

  WarningCtrl.$inject = [ '$scope', 'homeService', 'CONSTANT'];

  function WarningCtrl($scope, homeService, CONSTANT) {

    var self = this;
    // public properties
    self.canhBaoList = [];
  
    /* Get canhbao */
    homeService.getData('warning/' + CONSTANT.lang).then(function(res) {
      if (res.meta.code == 200) {
        self.canhBaoList =  res.data.items;
      }
    });

  }

})();