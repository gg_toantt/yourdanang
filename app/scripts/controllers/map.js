
;
(function() {
    'use strict';
	angular.module('nddkApp').controller('MapCtrl', MapCtrl);
	 
	MapCtrl.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT', 'ipCookie', '$stateParams', 'Scopes', '$rootScope'];
	
	function MapCtrl($scope, $timeout, homeService, CONSTANT, ipCookie, $stateParams, Scopes, $rootScope) {
		
		CONSTANT.lang = homeService.getLanguage();
		var self = this;
		
		Scopes.store('MapCtrl', self);
		var map;
		var _markerServices = [];
		var infowindowService;
		var infoWinSearch;
		var markerSearch;
		var catalogs;
		var catalogId = 0;
		var circle;
		var markerA;
		var markerB;
		var HEAD = "Head";
		var HEAD_VN = "Đi thẳng";
		var TURN_RIGHT_ON = "Turn right on";
		var TURN_RIGHT_ON_VN = "Rẽ phải tại";
		var TURN_LEFT_ON = "Turn left on";
		var TURN_LEFT_ON_VN = "Rẽ trái tại";
		var CONTINUE_TO_FOLLOW = "Continue to follow";
		var CONTINUE_TO_FOLLOW_VN = "Đi tiếp ";
		var TURN_TO = "Turn to";
		var TURN_TO_VN = "Rẽ sang";
		var AT = "At";
		var AT_VN = "Tại";
		var polyLineShortPath;
		var infWinCircle;
				
        // public properties
		self.field = [];
		self.highlight = [];
		self.resultSearchAb;
		self.keyword = '';
		self.kwSearchA = '';
		self.kwSearchB = '';
		self.ketquaVBD = [];
		self.page = 1;
		self.selected = [];
		self.selectedXungQuanh = [];
		self.selectedXungQuanhFull = [];
		self.currentServiceId = '';
		self.currentServiceCode = '';
		self.currentServiceValue = '';
		self.searchCount = 0;
		self.listCount = 0;
		self.listAttributeOfField = [];
		self.checkThuocTinh = [];
		self.everyPage = 10;
		self.xungQuanhBk = 0;
		self.xungQuanhKd = 0;
		self.xungQuanhVd = 0;
		self.xungQuanhPos = [];
		self.isListOrigin = false;
		self.isXungQuanh = false;
		self.searchCountView = false;
		self.isOnCheckThuocTinh = false;
		self.isHighlight = false;
		self.isSearchStreetAB = false;
		self.searchTextFind = '';
		self.searchPoint = 0;
		self.isBegin = true;
		
		//public method
		self.hideMenuLeft = hideMenuLeft;
		self.hideMenuLeftMobile = hideMenuLeftMobile;
		self.getServicesById = getServicesById;
		self.getServicesByIdPage = getServicesByIdPage;
		self.selectTab = selectTab;
		self.showOneService = showOneService;
		self.searchB = searchB;
		self.searchA = searchA;
		self.clickCloseBtnB = clickCloseBtnB;
		self.clickCloseBtnA = clickCloseBtnA;
		self.getServicesHighlight = getServicesHighlight;
		self.getServicesHighlightPage = getServicesHighlightPage;
		self.searchStreet = searchStreet;
		self.showOneResult = showOneResult;
		self.nextPageSearchStreet = nextPageSearchStreet;
		self.backPageSearchStreet = backPageSearchStreet;
		self.nextPageSearchStreetAB = nextPageSearchStreetAB;
		self.backPageSearchStreetAB = backPageSearchStreetAB;
		self.attributeClick = attributeClick;
		self.selectThuocTinh = selectThuocTinh;
		self.getNumber = getNumber;
		self.getListService = getListService;
		self.mapPageChange = mapPageChange;
		self.showPosition = showPosition;
		
		/* Scope */
		$scope.mapCtrl = self;
		$scope.user = ipCookie('user');
		
		/* init */
		getServiceField();
		vietbando.event.addDomListener(window, 'load', initialize);
		// check meta data
        homeService.checkMetaState();
        setTimeout(function() {
	 			$rootScope.htmlReady();
//	 			console.log('map');
	 		}, 100);
		/* functions */
		
		/* functions get data from API */
		function getServiceField() {
			var serviceFieldAPI = "services/" + CONSTANT.lang + "/field";
			homeService.getData(serviceFieldAPI).then(function(res) {
				//console.log("field");
				//console.log(res.data.items);
                self.field = res.data.items;
                catalogs = res.data.items;
                getServicesHighlight();
                Resize();
            });
		}
		
		function getServicesHighlight() {
			self.page = 1;
			if (($stateParams.dichvu === '' || $stateParams.dichvu == null) && ($stateParams.sid === '' || $stateParams.sid == null)) {
				var servicesHighlightAPI = "map/services/highlight/views/" + self.page;
				homeService.getData(servicesHighlightAPI).then(function(res) {
					self.listAttributeOfField = [];
					if (res.data.items != null) {
						self.highlight = res.data.items.data;
						self.listCount = res.data.items.count;
						self.searchCount = 0;
						self.searchCountView = false;
						self.isListOrigin = true;
						self.isHighlight = true;
						self.currentServiceCode = '';
						$('#contPlaceSpecial .wd-item-special').bind('click',function(){								
							$('#contPlaceSpecial .wd-item-special').removeClass('bg-current');
							$(this).addClass('bg-current');
						});
		                $('.wd-scroll-left-m').resize();
		                $('ul.wd-place-special li.wd-item-special').click(function(e){
							showOneService(res.data.items.data[$(this).index()]);
						});
		                $('.filter-option').empty();
		                $('.filter-option').append('Chọn lĩnh vực');
		                showServices(res.data.items.data, true);
		                if (self.isBegin) {
			                setTimeout(function() {
			                	showServices(res.data.items.data, false);
			                }, 2500);
			                self.isBegin = false;
		                } else {
		                	showServices(res.data.items.data, false);
		                }
		                
					} else {
						loadMap();
						alert('Không tìm thấy dịch vụ cho lĩnh vực này.');
					}
	                
	            });
			} else if (($stateParams.dichvu === '' || $stateParams.dichvu == null) && ($stateParams.sid !== '' || $stateParams.sid != null)) {
				getServiceFromDetail();
			} else {
				getListServiceFromListId();
			}
		}
		
		function getServicesHighlightPage(page) {
			if (($stateParams.dichvu === '' || $stateParams.dichvu == null) && ($stateParams.sid === '' || $stateParams.sid == null)) {
				var servicesHighlightAPI = "map/services/highlight/views/" + page;
				homeService.getData(servicesHighlightAPI).then(function(res) {
					if (res.data.items != null) {
						self.highlight = res.data.items.data;						
		                $('.wd-scroll-left-m').resize();
		                $('ul.wd-place-special li.wd-item-special').click(function(e){
							showOneService(res.data.items.data[$(this).index()]);
						});
		                showServices(res.data.items.data, true);
		                setTimeout(function() {
		                	showServices(res.data.items.data, false);
		                }, 2500);
		                
					} else {
						loadMap();
						alert('Không tìm thấy dịch vụ cho lĩnh vực này.');
					}
	                
	            });
			} else if (($stateParams.dichvu === '' || $stateParams.dichvu == null) && ($stateParams.sid !== '' || $stateParams.sid != null)) {
				getServiceFromDetail();
			} else {
				getListServiceFromListId();
			}
		}
		
		function mapPageChange(page) {
			// console.log("self.isOnCheckThuocTinh: " + self.isOnCheckThuocTinh);
			// console.log("self.highlight.length: " + self.highlight.length);
			if (self.isOnCheckThuocTinh > 0 || self.highlight.length > 0) {
				// console.log("mapPageChange");
				if (self.isListOrigin) {
					if (self.isHighlight) {
						// console.log("getServicesHighlightPage");
						getServicesHighlightPage(page);					
					} else {
						// console.log("getServicesByIdPage");
						getServicesByIdPage(page);
					}
				} else {
					if (self.isXungQuanh) {
						// console.log("getListXungQuanhPage");
						getListXungQuanhPage(page);
					} else if (self.checkThuocTinh.length > 0) {
						// console.log("getListService");
						getListService(page);
					} else {
						searchStreetPage(page);
					}
				}
			}
		}
		
		function getServicesByIdPage(page) {
			var servicesByIdAPI = "map/services/catalog/" + self.currentServiceId + "/" + page;
        	self.listAttributeOfField = [];
        	//console.log("getServicesByIdPage: " + servicesByIdAPI);
			homeService.getData(servicesByIdAPI).then(function(res) {
                self.highlight = res.data.items.data;
    			getListAttribute();
                $('.wd-scroll-left-m').resize();
                showServices(res.data.items.data, true);
            });
		}
		
		function getServicesById(id, ma, value) {
			self.page = 1;
			var servicesByIdAPI = "map/services/catalog/" + id + "/" +self.page;
        	self.listAttributeOfField = [];
        	self.selected = [];
			self.currentServiceId = id;
			self.currentServiceCode = ma;
			self.currentServiceValue = value;
			self.isListOrigin = true;
			self.isHighlight = false;
			//console.log("getServicesById: " + servicesByIdAPI);
			homeService.getData(servicesByIdAPI).then(function(res) {
                self.highlight = res.data.items.data;
                self.listCount = res.data.items.count;
                self.searchCount = 0;
    			self.searchCountView = false;
    			getListAttribute();
                $('.wd-scroll-left-m').resize();
                $('.filter-option').empty();
                $('.filter-option').append(value);
                showServices(res.data.items.data, true);
                setTimeout(function() {
                	showServices(res.data.items.data, true);
                }, 300);
            });
		}
		
		function getServiceFromDetail() {
			var serviceFromDetailAPI = 'map/services/' +  $stateParams.sid;
			homeService.getData(serviceFromDetailAPI).then(function(res) {
				if(res.data.info != null) {
					var item = res.data.info;
					loadMap();
					showOneService(item);
				}else {
					alert('Không tìm thấy dịch vụ, địa chỉ trên.');
				}
			});
		}
		
		function getListServiceFromListId() {
			var listServiceFromListId = 'services/vn/map/list?services=' + $stateParams.dichvu;
			homeService.getData(listServiceFromListId).then(function(res) {
				if(res.data.items != null) {
					self.highlight = res.data.items;
					showServices(res.data.items, true);
	                setTimeout(function() {
	                	showServices(res.data.items, false);
	                }, 2000);
	                heightContent();
				}
			});
		}
		
		function getListAttribute(){
            var attributeApi = "services/" + CONSTANT.lang + "/"
            + self.currentServiceCode + "/attribute";
            homeService.getData(attributeApi).then(function (res){
                self.listAttributeOfField = res.data.items;
            });

        }

        function attributeClick(e) {
        	var ele = e.target.parentElement.parentElement.parentElement.getElementsByClassName("wd-portlet-body");
        	var eleQ = $(ele[0]);
        	if (eleQ.is(':visible')) {
        		eleQ.slideUp();
        		e.target.parentElement.classList.add("active2");
        	} else {
        		eleQ.slideDown();
        		e.target.parentElement.classList.remove("active2");
        	}
        }
		
		function getInterested(id){
			var result = '';
			var userId = '';
			if($scope.user !=null) {
				userId = $scope.user.id;
			}
            $.ajax({
                url : CONSTANT.apiURL + 'comment/service/' + id + '/interest/count',
                type : 'POST',
                data : {'username': userId},
                async : false,
                success : function(res){
                	result = res.data.info.content;
                },
            });
            return result;
		}

		var textSearch = '';
		function searchStreetPage(page) {
			//console.log("self.currentServiceId: " + self.currentServiceId);
			var searchStreetAPI = 'map/searchStreet?textFind=' + textSearch + "&page=" + page + "&id=" + self.currentServiceCode;
			var param = 'isDichVu=' + true;
			homeService
				.getDataByPostMethod(searchStreetAPI, param)
				.success(
					function(res, status, headers, config) {
						if (res.meta.code == 200 && res.data.items != null) {
							loadMap();
							var items = res.data.items;
							var isSearchDvu = false;
							if (items.count !== undefined && items.count > 0) {
								$.each (items.data, function (index) {
									self.highlight = items.data;
									self.searchCount = items.count;
									self.listCount = 0;
									isSearchDvu = true;
								});
							} 
							if(isSearchDvu) {
								$('.wd-scroll-left-m').resize();
								$('ul.wd-place-special li.wd-item-special').click(function(e){
									showOneService(res.data.items.data[$(this).index()]);
								});
								showServices(res.data.items.data, true);
							}
						}else {
							alert('Không tìm thấy dịch vụ, địa chỉ trên.');
						}
					})
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
				
		/* functions public */
		function searchStreet(textFind) {
			// console.log("searchStreet");
			self.isListOrigin = false;
			self.isSearchStreetAB = false;
			textSearch = textFind;
			//console.log("self.currentServiceId: " + self.currentServiceId);
			var searchStreetAPI = 'map/searchStreet?textFind=' + textFind + "&page=" + self.page + "&id=" + self.currentServiceCode;
			var param = 'isDichVu=' + true;
			self.isOnCheckThuocTinh = false;
			//console.log("searchStreet2");
			// console.log("searchStreet API: " + searchStreetAPI);
			homeService
				.getDataByPostMethod(searchStreetAPI, param)
				.success(
					function(res, status, headers, config) {
						//console.log(res);
						
						//console.log("searchStreet3");
						if (res.meta.code == 200 && res.data.items != null) {
							loadMap();
							var items = res.data.items;
							var isSearchDvu = false;
							if (items.count !== undefined && items.count > 0) {
								$.each (items.data, function (index) {
									self.highlight = items.data;
									self.searchCount = items.count;
									self.listCount = 0;
									self.searchCountView = true;
									isSearchDvu = true;
								});
							} else {
								$.each (items, function (index) {
									self.highlight = [];
									self.searchCountView = false;
									self.isOnCheckThuocTinh = false;
									
									var lengthVBD = items[index].length;
									if (lengthVBD == undefined) {
										self.ketquaVBD = [];
										self.ketquaVBD.push(items[index]);
										$('.map-next-button').addClass('disabled');
										$('.map-back-button').addClass('disabled');
										$('.map-paging').css('display', 'none');
										
									} else {
										self.ketquaVBD = items[index];
										if (lengthVBD < 10) {
											$('.map-next-button').addClass('disabled');
										} else {
											$('.map-next-button').removeClass('disabled');
										}
										if (self.page === 1 && items[index].length < 10) {
											$('.map-paging').css('display', 'none');
										} else {
											$('.map-paging').css('display', 'block');
										}
										if (self.page === 1) {
											$('.map-back-button').addClass('disabled');
										} else {
											$('.map-back-button').removeClass('disabled');
										}
									}
									if (items[index].Name !== undefined) {
										showOneResult(items[index], (self.page-1)*10 + 1);
									} else {
										showResult(items[index]);
									}
								});
							}
							if(isSearchDvu) {
								$('.wd-scroll-left-m').resize();
								$('ul.wd-place-special li.wd-item-special').click(function(e){
									showOneService(res.data.items.data[$(this).index()]);
								});
								showServices(res.data.items.data, true);
							}
						}else {
							self.highlight = [];
							loadMap();
							self.searchCount = 0;
							self.searchCountView = true;
							alert('Không tìm thấy dịch vụ, địa chỉ trên.');
						}
					})
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
		
		function nextPageSearchStreet() {
			// console.log("nextPageSearchStreet");
			if (!$('.map-next-button').hasClass('disabled')) {
				self.page++;
				searchStreet(textSearch);
			}
		}
		
		function backPageSearchStreet() {
			if (!$('.map-back-button').hasClass('disabled')) {
				self.page--;
				searchStreet(textSearch);
			}
		}
		
		function nextPageSearchStreetAB() {
			// console.log("nextPageSearchStreet");
			if (!$('.map-next-button').hasClass('disabled')) {
				self.page++;
				searchStreetAB(self.searchTextFind,self.searchPoint);
			}
		}
		
		function backPageSearchStreetAB() {
			if (!$('.map-back-button').hasClass('disabled')) {
				self.page--;
				searchStreetAB(self.searchTextFind,self.searchPoint);
			}
		}
		
		function hideMenuLeft() {
			$('.wd-left-map').toggle("slow");
			$('.wd-bt-tog-lct').toggleClass("wd-bt-tog-lct-tg");
		}
		
		function hideMenuLeftMobile() {
			$('.wd-left-map').toggle("slow");
			$('.wd-bt-tog-lct-mb').toggleClass("wd-bt-tog-lct-mb-tg");
		}
		
		function selectTab(a) {
			// console.log("selectTab: " + a);
			if (a === 'location') {
				
				$('#location-tab').addClass('active');
				$('#search-tab').removeClass('active');
				$('#location-tab-mb').addClass('active');
				$('#search-tab-mb').removeClass('active');
				if ($('#wd-sortby-top-pc').outerHeight() > 0) {
					heightContent();
				}
			} else {
				
				$('#location-tab').removeClass('active');
				$('#search-tab').addClass('active');
				$('#location-tab-mb').removeClass('active');
				$('#search-tab-mb').addClass('active');
				heightContent();
			}
		}
		
		function searchA() {
			if($('#fromA').val() != "" ) {
				markerA = searchStreetAB(self.kwSearchA, 1);
				if(markerA != null ){
					dragMarkerA_Event(markerA);
					map.setCenter(markerA.getPosition());
					checkShowAB();
					if(markerB != null ){ findShortestPath();}
				}
				return false;
			}else {
				alert('Vui lòng nhập địa chỉ điểm bắt đầu.');
			}
		}
		
		function searchB() {
			if($('#toB').val() != "" ) {
				markerB = searchStreetAB(self.kwSearchB ,2);
				if(markerB != null){
					dragMarkerB_Event(markerB);
					map.setCenter(markerB.getPosition());
					checkShowAB();
					if(markerA != null ){ findShortestPath();}
				}
				return false;
			}else {
				alert('Vui lòng nhập địa chỉ điểm kết thúc.');
			}
		}
		
		function clickCloseBtnB() {
			$('#toB').val('');
			closeMarker(markerB);
			markerB = null;
			closePolyLine(polyLineShortPath);
			polyLineShortPath = null;
			checkShowAB();
			clearResultDiv();
		}
		
		function clickCloseBtnA() {
			$('#fromA').val('');
			closeMarker(markerA);
			markerA = null;
			closePolyLine(polyLineShortPath);
			polyLineShortPath = null;
			checkShowAB();
			clearResultDiv();
		}
		
		function showOneService(item){
			var kinhDo = item.kinhDo;
			var viDo = item.viDo;
			var ten = item.ten;
			var id = item.id;
			var idLinhVuc = item.idLinhVuc;
			var urlImage = item.anhDaiDien.urlImage;
			var comment=item.comment;
			var interest=item.interest;
			var iconMap=item.imgLinhVuc;
			var moTa = item.moTa;
			var urlService=item.url;
			var priceFormat = item.priceFormat;
			var reMoTa = "";
			if (moTa != null) {
				var reMoTa = moTa.replace(/<\/?[^>]+(>|$)/g, "");
			}
			if(viDo == 0 || kinhDo == 0) {
				alert("Không xác định được tọa độ của dịch vụ này.");
			} else {
				var interested = '';
				if (getInterested(id) === 'daquantam') { 
					interested = 'active'; 
				}
				var pos = new vietbando.LatLng(viDo,kinhDo);
				var strHtml = 
					"<html>"
						+"<div class='caption-map'> "
							+"<div class='img-map-show'>"
								+"<img src='"+urlImage+"'/>"
								+"<span class='pricing'>Giá từ: <strong>" + item.priceFormat + "</strong> <i class='conmuch-bdl'>&nbsp;</i>" 
								+"<span class='score'>"+item.markValueFormat+"<i class='iconmuch-arleft'>&nbsp;</i></span>"
								
							+"</div>"
							+"<div class='map-description'>"
								+"<a href='/#/dich-vu/"+ idLinhVuc + "/" + urlService+"' class='fix-text href-ser'>"+ten+"</a>"
								+"<p class='fix-text-address'>"+item.diaChi+"</p>"
								+"<div class=''>" 
									+"<span class='map-bl-qt'><i class='iconmuch-comment'>&nbsp;</i> "+comment+" bình luận</span>"
									+"<span class='map-bl-qt ml12'>"
										+"<i class='iconmuch-heart'>&nbsp;</i>"
										+"<span id='"+"quantam"+id+"'>" +interest+" </span> quan tâm </span>"
									+"<p class='cut-text-map' style='text-align: justify;'>"+reMoTa+"</p>"
								+"</div>"
							+"</div>"
							+"<div class='linkmap-action'>"
								+"<input type='button' class='search-street iconInterest " + interested +"' value='Quan tâm' onclick='clickInterest("+id+")'/>"		
								+"<input type='button' value='Tìm đường' onclick='timDuongEvent("+viDo+","+ kinhDo+")' class='search-street' />"
								+"<input type='button' value='Xung quanh' onclick='btnXungQuanhClick("+viDo+","+ kinhDo+", false)' class='search-around'/>"
							+"</div>"
						+"</div>"
					+"</html>";
				var strHtmlMobile = 
					"<html>"
						+"<div class='caption-map-mb'> "
							+"<div class='img-map-show'>"
								+"<img src='"+urlImage+"' />"
								+"<span class='pricing'>Giá từ: <strong>" + item.priceFormat + "</strong> <i class='conmuch-bdl'>&nbsp;</i>" 
								+"<span class='score'>"+item.markValueFormat+"<i class='iconmuch-arleft'>&nbsp;</i></span>"
								
							+"</div>"
							+"<div class='map-description'>"
								+"<a href='/#/dich-vu/"+ idLinhVuc + "/" + urlService+"' class='fix-text href-ser'>"+ten+"</a>"
								+"<p class='fix-text-address'>"+item.diaChi+"</p>"
								+"<div class='map-description-other'>" 
									+"<span><i class='iconmuch-comment'>&nbsp;</i> "+comment+"</span>"
									+"<span class='ml12'>"
										+"<i class='iconmuch-heart'>&nbsp;</i>"
										+"<span id='"+"quantam"+id+"'>" +interest+" </span></span>"
									//+"<p class='cut-text-map' style='text-align: justify;'>"+reMoTa+"</p>"
								+"</div>"
							+"</div>"
							+"<div class='linkmap-action'>"
								+"<input type='button' class='search-street iconInterest " + interested +"' value='Quan tâm' onclick='clickInterest("+id+")'/>"		
								+"<input type='button' value='Tìm đường' onclick='timDuongEvent("+viDo+","+ kinhDo+")' class='search-street' />"
								+"<input type='button' value='Xung quanh' onclick='btnXungQuanhClick("+viDo+","+ kinhDo+", false)' class='search-street'/>"
							+"</div>"
						+"</div>"
					+"</html>";
				if ($('#map').outerWidth() < 600) {
					$('.wd-left-map').toggle("slow");
					$('.wd-bt-tog-lct-mb').toggleClass("wd-bt-tog-lct-mb-tg");
					strHtml = strHtmlMobile;
				}
				addMarkerAddInfowindow(map, pos, strHtml, true, id ,iconMap);
			}
		}	
		
		function showServices(resSer ,flgLoadMap){
			if(flgLoadMap){
				loadMap();
			}
			var items = resSer;
			var itemsHref = resSer.urlstatic;
			
			_markerServices = [];
			$.each (items, function (index) {
				var kinhDo = items[index].kinhDo;
				var viDo = items[index].viDo;
				if (kinhDo != 0 && viDo != 0) {
					var ten = items[index].ten;
					var id = items[index].id;
					var idLinhVuc = items[index].idLinhVuc;
					var comment=items[index].comment;
					var interest=items[index].interest;
					var urlImage = items[index].anhDaiDien.urlImage;
					var urlHref = items[index].anhDaiDien.urlImage;
					var iconMap=items[index].imgLinhVuc;
					var urlService=items[index].url;
					var priceFormat = items[index].priceFormat;
					var markValueFormat = items[index].markValueFormat;
					var moTa = items[index].moTa;
					var diaChi = items[index].diaChi;
					var reMoTa = moTa.replace(/<\/?[^>]+(>|$)/g, "");
					var interested = '';
					if (getInterested(id) === 'daquantam') { 
						interested = 'active'; 
					}
					var pos = new vietbando.LatLng(viDo,kinhDo);
					var strHtml = "<html><div class='caption-map'> "
							+ "<div class='img-map-show'><img src='"+urlImage+"'/>"
							+"<span class='pricing'>Giá từ: <strong>" + priceFormat + "</strong> <i class='conmuch-bdl'>&nbsp;</i>" 
							+"<span class='score'>"+markValueFormat+"<i class='iconmuch-arleft'>&nbsp;</i></span></div>"
							+ "<div class='map-description'>"
							+	"<a target='_blank' href='/#/dich-vu/"+ idLinhVuc + "/" + urlService+"' class='fix-text href-ser'>"+ten+"</a>"
							+  	"<p class='fix-text-address'>"+diaChi+"</p>"
							+   "<div class=''><span class='map-bl-qt'><i class='iconmuch-comment'>&nbsp;</i> "+comment+" bình luận</span><span class='map-bl-qt ml12'><i class='iconmuch-heart'>&nbsp;</i> <span id='interest"+id+"'>" +interest+"</span> quan tâm </span>"
							+   "<p class='fix-text-map' style='text-align: justify;'>"+ reMoTa +"</p></div></div>"
							+ "<div class='linkmap-action'>"				
							+  "<input type='button' class='search-street iconInterest " + interested + "' value='Quan tâm' onclick='clickInterest("+id+")'/>"				
							+  "<input type='button' value='Tìm đường' onclick='timDuongEvent("+viDo+","+ kinhDo+")' class='search-street' />"
							+  "<input type='button' value='Xung quanh' onclick='btnXungQuanhClick("+viDo+","+ kinhDo+", false)' class='search-around'/></div>"
							+ "</div></html>";
					var strHtmlMobile = 
						"<html>"
							+"<div class='caption-map-mb'> "
								+"<div class='img-map-show'>"
									+"<img src='"+urlImage+"' />"
									+"<span class='pricing'>Giá từ: <strong>" + priceFormat + "</strong> <i class='conmuch-bdl'>&nbsp;</i>" 
									+"<span class='score'>"+markValueFormat+"<i class='iconmuch-arleft'>&nbsp;</i></span>"
									
								+"</div>"
								+"<div class='map-description'>"
									+"<a href='/#/dich-vu/"+ idLinhVuc + "/" + urlService+"' class='fix-text href-ser'>"+ten+"</a>"
									+"<p class='fix-text-address'>"+diaChi+"</p>"
									+"<div class='map-description-other'>" 
										+"<span><i class='iconmuch-comment'>&nbsp;</i> "+comment+"</span>"
										+"<span class='ml12'>"
											+"<i class='iconmuch-heart'>&nbsp;</i>"
											+"<span id='"+"quantam"+id+"'>" +interest+" </span></span>"
										//+"<p class='cut-text-map' style='text-align: justify;'>"+reMoTa+"</p>"
									+"</div>"
								+"</div>"
								+"<div class='linkmap-action'>"
									+"<input type='button' class='search-street iconInterest " + interested +"' value='Quan tâm' onclick='clickInterest("+id+")'/>"		
									+"<input type='button' value='Tìm đường' onclick='timDuongEvent("+viDo+","+ kinhDo+")' class='search-street' />"
									+"<input type='button' value='Xung quanh' onclick='btnXungQuanhClick("+viDo+","+ kinhDo+", false)' class='search-street'/>"
								+"</div>"
							+"</div>"
						+"</html>";
					//console.log($('#map').outerWidth());
					if ($('#map').outerWidth() < 600) {
						strHtml = strHtmlMobile;
					}
					addMarkerAddInfowindow(map, pos, strHtml, false ,id ,iconMap);
				}
			});
		}
		
		function showOneResult(items, number) {
			var longitude = items.Longitude;
			var latitude = items.Latitude;
			
			var strHtml = "<html><div class='caption-street'> "
				+ "<div class='map-description'>"
				+	"<p><label>"+items.Name+"</label></p>"
				+  	"<p>"+items.AddressString+"</p>"
				+ "</div></html>";
		
			var position = new vietbando.LatLng(latitude,longitude);
			if ($('#wd-header-map-mobile').prop('scrollHeight') > 0) {
				$('.wd-left-map').toggle("slow");
				$('.wd-bt-tog-lct-mb').css('display', 'none');
			}
			addMarker2(position, map , strHtml, 17, true, number);
		}
		
		function showPosition(items) {
			var longitude = items.Longitude;
			var latitude = items.Latitude;
			var pos = new vietbando.LatLng(latitude,longitude);
			if (self.searchPoint == 1) {
				tuDay(pos,map);
			} else {
				denDay(pos,map);
			}
		}
		
		function showResult(items) {
//			console.log(items);
			for (var i = 0; i < items.length; i++) {
				var longitude = items[i].Longitude;
				var latitude = items[i].Latitude;
				var name = items[i].Name;
				var address = items[i].AddressString;
				var strHtml = "<html><div class='caption-street'> "
					+ "<div class='map-description'>"
					+	"<p><label>"+name+"</label></p>"
					+  	"<p>"+address+"</p>"
					+ "</div></html>";
				var position = new vietbando.LatLng(latitude,longitude);
				//console.log("showResult: " + ((self.page-1)*10 + i + 1));
				addMarker2(position, map , strHtml, 17, false, (self.page-1)*10 + i + 1);	
			}
		}
		
		$(window).resize(function() {
			heightContent();
		});
		
		/* Viet Ban Do*/
		function initialize(){
			loadMap();
		}
		
		function loadMap(){
			$('#map').empty();
			Resize();
			var mapProp = {
				  center: new vietbando.LatLng(16.061074, 108.221854),
				  zoom: 13,
				  mapTypeId: vietbando.MapTypeId.ROADMAP
			};
			map = new vietbando.Map(document.getElementById("map"), mapProp);
			
			vietbando.event.addListener(map, 'rightclick', function(param) {
				if (document.getElementById('divMenu') != null) {
					$("#divMenu").remove(); 
				}
				var point = map.getMouseOnMap( param.Point.x, param.Point.y);
				var contextMenu = document.createElement('div');
				contextMenu.setAttribute("id", "divMenu");
				map.getContainer().appendChild(contextMenu);
				
				$("#divMenu").append("<ul>"
						+"<li id='btnTuDay'><a><i class='iconcontext-from'></i>Từ đây</a></li>"
						+"<li id='btnDenDay'><a><i class='iconcontext-here'></i>Đến đây</a></li>"
						+" <li id='btnXungQuanh'><a><i class='iconcontext-nearby'></i>Xung quanh</a></li>"
						+"</ul>");
				var x = point.x;
				var y = point.y;
				if(x > 1000) x = x - 150;
				if(y > 500) y = y - 100;
				var y = point.y;
				var left = document.getElementById('menu-left-map').style.display;
				if (left === 'none') {
					x = x + 338;
				} 
//				console.log("x: " + x);
//				console.log("y: " + y);
//				console.log("lat: " + param.LatLng.lat());
//				console.log("lng: " + param.LatLng.lng());
				contextMenu.style.position = 'absolute';
				contextMenu.style.left =  x + 'px';
				contextMenu.style.top = y + 'px';
				contextMenu.style.zIndex = 1000;
				
				$('#btnTuDay a').bind("click", function(){
					if (left === 'none') {
						document.getElementById('menu-left-map').style.display = 'block';
					}
					tuDay(param.LatLng, map);
				});
				
				$('#btnDenDay a').bind("click", function(){
					if (left === 'none') {
						document.getElementById('menu-left-map').style.display = 'block';
					}
					denDay(param.LatLng, map);
				});
				
				$('#btnXungQuanh a').bind("click", function(){
					$("#divMenu").remove();
					btnXungQuanhClick(param.LatLng.lat(), param.LatLng.lng(), true);					
					$('#location-tab').addClass('active');
					$('#search-tab').removeClass('active');
					$('#location-tab-mb').addClass('active');
					$('#search-tab-mb').removeClass('active');
					if ($('#wd-sortby-top-pc').outerHeight() > 0) {
						heightContent();
					}
				}); 
			});
			
			vietbando.event.addListener(map, 'click', function(param) {
				if (document.getElementById('divMenu') != null) {
					$("#divMenu").remove(); 
				}
			});
			// console.log("loadMap");
			if (markerA != null) {
				markerA = addMarker(markerA.Opt.position, map, 1);
				dragMarkerA_Event(markerA);
				map.setCenter(markerA.getPosition());
				checkShowAB();
				if(markerB != null ){ findShortestPath();}
			}
			if (markerB != null) {
				markerB = addMarker(markerB.Opt.position, map, 2);
				dragMarkerB_Event(markerB);
				map.setCenter(markerB.getPosition());
				checkShowAB();
				if(markerA != null ){ findShortestPath();}
			}
		}
		
		function addMarkerAddInfowindow(map, pos, content ,flagShowInfoWindown ,id , urlIcon ) {
			//console.log("addMarkerAddInfowindow");
			//map.setCenter(pos);
			if(pos.lat() != 0.0 && pos.lng() != 0.0) {
				var marker;
				if(flagShowInfoWindown) {
					if(_markerServices != null && _markerServices.length > 0) {
						for(var i = 0; i < _markerServices.length;i++ ) {
							if(_markerServices[i].id == id) {
								marker = _markerServices[i];
								closeInfowindow(infowindowService);
								infowindowService = new vietbando.InfoWindow({
										content : content,
										position : pos	
								});
								infowindowService.open(map, marker);
								return false;
							}
						} 
					}else {
						var marker1;
						alert(urlIcon);
						marker1 = new vietbando.Marker({ position : pos, icon : {url : urlIcon}});
						marker1.setMap(map);
						map.setCenter(pos);
						infowindowService = new vietbando.InfoWindow({content : content,autoPan : true});
						infowindowService.open(map, marker1);
						vietbando.event.addListener(marker1, 'click', function() {
							//console.log("market click");
							closeInfowindow(infowindowService);
							infowindowService = new vietbando.InfoWindow({content : content,autoPan : true});
							infowindowService.open(map, marker1);
						});
					}
				}else {
					marker = new vietbando.Marker({
						position : pos,
						icon : {url : urlIcon, size : {width:18, height:29}}
					});
					marker.id = id;

					_markerServices.push(marker);
					marker.setMap(map);
					vietbando.event.addListener(marker, 'click', function() {
						//console.log("market click2");
						closeInfowindow(infowindowService);
						infowindowService = new vietbando.InfoWindow({
							content : content,
							position : pos
						});
						infowindowService.open(map, marker);
						map.setCenter(pos);
					});
				}
			}
		}
		
		var infowindow;
		function addMarker2(position, map , content ,ZoomLevel, flag, number) {
			//console.log("addMarker2");
			var marker;
			if(position.lat() != 0.0 || position.lng() != 0.0) {
				var searchHtml = "<div class='customMarker'><a>" + number +"</a></div>";
				marker = new vietbando.CustomMarker(
					{
						position : position
					});
				
				if (flag) {
					marker.setMap(map);
					marker.setContent(searchHtml);
					if(number < 10) {
						$('.customMarker a').css('padding-left', '9px');
					} else if (number > 99) {
						$('.customMarker a').css('padding-left', '1px');
					}
					map.setCenter(position);
					closeInfowindow(infowindow);
					infowindow = new vietbando.InfoWindow({
						content : content,
						position : position
					});
					infowindow.open(map, marker);
					var wd = $("#map div div div div div").find("[type_object=0]");
					var top = wd.css('top').replace('px','');
					var left = wd.css('left').replace('px','');
					wd.css('top', (top - 30) + 'px');
					wd.css('left', parseFloat(left) + 4 + 'px');
				} else {
					marker.setMap(map);
					marker.setContent(searchHtml);
					if(number < 10) {
						$('.customMarker a').css('padding-left', '9px');
					} else if (number > 99) {
						$('.customMarker a').css('padding-left', '1px');
					}
					vietbando.event.addListener(marker, 'click', function() {
						closeInfowindow(infowindow);
						infowindow = new vietbando.InfoWindow({
							content : content,
							position : position
						});
						infowindow.open(map, marker);
						map.setCenter(position);
						var wd = $("#map div div div div div").find("[type_object=0]");
						var top = wd.css('top').replace('px','');
						var left = wd.css('left').replace('px','');
						wd.css('top', (top - 30) + 'px');
						wd.css('left', parseFloat(left) + 4 + 'px');
					});
				}
			}
			return marker;
		}
		
		function closeInfowindow(infowindow) {
			if(infowindow != null) {
				infowindow.close();
			}
		}
		
		function closeMarker(marker) {
			if(marker != null) {
				marker.setMap(null);
			}
		}
		
		function closeCircle(circle) {
			if(circle != null) {
				circle.setMap(null);
			}
		}
		
		function closePolyLine(polyLine) {
			if(polyLine != null) {
				polyLine.setMap(null);
			}
		}
		
		function addCircle(map, pos, radius) {
			var circle = new vietbando.Circle({
				center : pos,
				radius : radius
			});
			circle.setMap(map);
			map.setCenter(pos);
			vietbando.event.addListener(circle, 'rightclick', function(param) {
				var strHtml = "<input onclick='btnCloseCircleEvent();' type=\"button\" value=\"Đóng vòng tròn\" />";
				
				//anvt:13/1/2016: pos infWinCircle is center of circle - add closeclick evt
				if(!infWinCircle) {
					infWinCircle = new vietbando.InfoWindow({ position: pos, content: strHtml, autoPan : true });
					vietbando.event.addListener(infWinCircle, 'closeclick', function() {infWinCircle = null;});
					infWinCircle.open(map);
				}
				
			});
			//anvt:14/1/2016: thêm click lên circle
			vietbando.event.addListener(circle, "click", function() {
				if(!infoWinSearch || infoWinSearch == null) {
					appendSearchWindow(pos.lat(), pos.lng(), true, radius);
				}
			});
			return circle;
		}
		
		function tuDay(pos, map) {
			closeMarker(markerA);
			markerA = null;
			closePolyLine(polyLineShortPath);
			polyLineShortPath = null;
			markerA = addMarker(pos, map, 1);
			setValueToFindTextBox(pos, $('.fromA'));
			if(markerB != null) {
				findShortestPath();
			}
			checkShowAB();
			dragMarkerA_Event(markerA);
			$("#divMenu").remove();
			$('.wd-top-left-m .wd-tab-map li:last-child').addClass('active');
			$('.tab-content #location-tab').removeClass('active');
			$('.wd-top-left-m .wd-tab-map li:first-child').removeClass('active');
			$('.tab-content #search-tab').addClass('active');
		}

		function denDay(pos, map) {
			closeMarker(markerB);
			markerB = null;
			closePolyLine(polyLineShortPath);
			polyLineShortPath = null;
			markerB = addMarker(pos, map, 2);
			setValueToFindTextBox(pos, $('.toB'));
			if(markerA != null) {
				findShortestPath();
			}
			checkShowAB();
			if ($('.wd-left-map').css('display') == 'none') {
				$('.wd-left-map').toggle("slow");
				$('.wd-bt-tog-lct-mb').toggleClass("wd-bt-tog-lct-mb-tg");
			}
			dragMarkerB_Event(markerB);
			$("#divMenu").remove();
			selectTab('search');
		}
		
		function addMarker(position, map, point) {
			var content = '';
			var pointAhtml = "<div class='pointA'></div>";
			var pointBhtml = "<div class='pointB'></div>";
			var pointChtml = "<div class='pointC'></div>";
			switch (point) {
		    case 0:
		    	content = '';
		        break;
		    case 1:
		        content = pointAhtml;
		        break;
		    case 2:
		        content = pointBhtml;
		        break;
			case 3:
				content = pointChtml;
		        break;
		    } 
			var marker = null;
			if(position.lat() != 0.0 || position.lng() != 0.0) {
				marker = new vietbando.CustomMarker(
					{
						position : position
					});
				marker.setMap(map);
				marker.setContent(content);
				marker.setDraggable(true);
			}
			return marker;
		}
		
		function getListXungQuanhPage(page) {
			if (self.selectedXungQuanh.length == 0) {
				self.selectedXungQuanh = self.selectedXungQuanhFull;
			}
			var listXungQuanhAPI = 'map/services/catalog/'+self.selectedXungQuanh+'/'+page;
			var param = 'ban_kinh=' + self.xungQuanhBk + '&kinh_do=' + self.xungQuanhKd + '&vi_do=' + self.xungQuanhVd;
			//console.log("getListXungQuanhPage: " + listXungQuanhAPI);
			homeService
				.getDataByPostMethod(listXungQuanhAPI, param)
				.success(
					function(res, status, headers, config) {
						if (res.meta.code == 200 && res.data.items != null) {
							self.highlight = res.data.items.data;
							$('ul.wd-place-special li.wd-item-special').click(function(e){
								showOneService(res.data.items.data[$(this).index()]);
							});
							showServices(res.data.items.data, true);							
							setTimeout(function() {
			                	showServices(res.data.items.data, true);
								closeCircle(circle);
								circle = null;
								circle = addCircle(map, self.xungQuanhPos , self.xungQuanhBk * 1000);
			                }, 300);
							$('.wd-scroll-left-m').resize();
						}else {
							alert('Không tìm thấy dịch vụ nào xung quanh.');
						}
                    })
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
		
		function getListXungQuanh(linhVuc, bk, vd, kd, pos) {
			
			self.page = 1;
			var listXungQuanhAPI = 'map/services/catalog/'+linhVuc+'/'+self.page;
			var param = 'ban_kinh=' + bk + '&kinh_do=' + kd + '&vi_do=' + vd;
			self.xungQuanhBk = bk;
			self.xungQuanhKd = kd;
			self.xungQuanhVd = vd;
			self.xungQuanhPos = pos;
			//console.log("getListXungQuanh: " + listXungQuanhAPI);
			homeService
				.getDataByPostMethod(listXungQuanhAPI, param)
				.success(
					function(res, status, headers, config) {
						if (res.meta.code == 200 && res.data.items != null) {
							self.isListOrigin = false;
							self.highlight = res.data.items.data;
							self.searchCount = res.data.items.count;
							self.listCount = 0;
							self.searchCountView = true;
							self.isXungQuanh = true;
							$('ul.wd-place-special li.wd-item-special').click(function(e){
								showOneService(res.data.items.data[$(this).index()]);
							});
							self.listAttributeOfField = [];
							$('.filter-option').empty();
			                $('.filter-option').append('Chọn lĩnh vực');
							showServices(res.data.items.data, true);							
							setTimeout(function() {
			                	showServices(res.data.items.data, true);
								closeCircle(circle);
								circle = null;
								circle = addCircle(map, pos , bk * 1000);
			                }, 300);
							
							//-----
							if ($('.wd-left-map').css('display') == 'none') {
								$('.wd-left-map').toggle("slow");
								$('.wd-bt-tog-lct-mb').toggleClass("wd-bt-tog-lct-mb-tg");
							}
							$('.wd-scroll-left-m').resize();
						}else {
							alert('Không tìm thấy dịch vụ nào xung quanh.');
						}
                    })
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
		
		function getListService(numberPage) {
			//console.log("getListService");
            var listServiceApi = "services/" + CONSTANT.lang + "/" + self.currentServiceCode
            		+ "?keyword=" + removeSpecialLetter(self.keyword)
            		+ "&page="+ numberPage
            		+ "&checkThuocTinh="+ self.checkThuocTinh;
            self.isListOrigin = false;
            homeService.getData(listServiceApi).then(function(res) {
            	self.searchCount = res.data.items.count;
            	self.listCount = 0;
        		self.searchCountView = true;
            	self.highlight = res.data.items.data;    
            	$('.wd-scroll-left-m').resize();
                showServices(res.data.items.data, true);
            });
        }
		
		function dragMarkerA_Event(marker) {
			vietbando.event.addListener(marker, 'drag', function (){
				if(markerB != null) {
					findShortestPath();
				}
			});
			
			vietbando.event.addListener(marker, 'dragend', function (param){
				setValueToFindTextBox(param.LatLng, $('#fromA'));
			});
		}

		function dragMarkerB_Event(marker) {
			vietbando.event.addListener(marker, 'drag', function (){
				if(markerA != null) {
					findShortestPath();
				}
			});
			
			vietbando.event.addListener(marker, 'dragend', function (param){
				setValueToFindTextBox(param.LatLng, $('#toB'));
			});
		}
		
		function setValueToFindTextBox(pos , textBox) {
			var valueToFindTextBoxAPI = 'map/reverseGeoCoding';
			var param = 'lat=' + pos.lat() + '&lng=' + pos.lng();
			homeService
				.getDataByPostMethod(valueToFindTextBoxAPI, param)
				.success(
					function(res, status, headers, config) {
						if(res.meta.code == 200 && res.data.info != null) {
							var result = "";
							var Street = res.data.info.ReverseGeoCodingResult.Street;
							var Ward = res.data.info.ReverseGeoCodingResult.Ward;
							var District = res.data.info.ReverseGeoCodingResult.District;
							var Province = res.data.info.ReverseGeoCodingResult.Province;
							if(Street != ""){result = Street +', ';}
							if(Ward != ""){result = result + Ward + ', ';}
							if(District != ""){result = result + District + ', ';}
							if(Province != ""){result = result + Province;}
							
							textBox.prop('value',result);
						}else {
							alert('Không tìm thấy đia chỉ tương ứng với tọa độ trên');
							textBox.prop('value','Không xác định');
						}
                    })
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
		
		function checkShowAB(){
			 setTimeout(function() {
				if($('.fromA').val() == ""){
					$('.btnSearchA').show();
					$('.btnCloseA').hide();
				}else {
					$('.btnSearchA').hide();
					$('.btnCloseA').show();
				}
				if($('.toB').val() == ""){
					$('.btnSearchB').show();
					$('.btnCloseB').hide();
				}else {
					$('.btnSearchB').hide();
					$('.btnCloseB').show();
				}				 
             }, 300);
		}
				
		function searchStreetAB(textFind ,point) {
			// console.log("searchStreetAB");
			self.isSearchStreetAB = true;
			self.searchTextFind = textFind;
			self.searchPoint = point;
			var marker;
			$.ajax({
				url : CONSTANT.apiURL +'map/searchStreet?textFind='+textFind + "&page=" + self.page + "&id=",
				type : 'POST',
				data : {'isDichVu': false},
				async : false,
				success : function(res){
					if (res.data.items != null) {
						var items = res.data.items;
						// console.log(res.data.items);
						loadMap();
						$.each (items, function (index) {
							if(items[index].id !== undefined) {
								alert('Không tìm thấy dịch vụ, địa chỉ trên.');
								return false;
							} else {
								self.highlight = [];
								self.searchCountView = false;
								self.isOnCheckThuocTinh = false;
								selectTab("location");
								var lengthVBD = items[index].length;
							
								if (lengthVBD == undefined) {
									self.ketquaVBD = [];
									self.ketquaVBD.push(items[index]);
									$('.map-next-button').addClass('disabled');							
									if (self.page === 1) {
										$('.map-paging').css('display', 'none');
									} else {
										$('.map-paging').css('display', 'block');
										// console.log("block");
									}
									if (self.page === 1) {
										$('.map-back-button').addClass('disabled');
									} else {
										$('.map-back-button').removeClass('disabled');
									}
									
								} else {
									self.ketquaVBD = items[index];
									if (lengthVBD < 10) {
										$('.map-next-button').addClass('disabled');
									} else {
										$('.map-next-button').removeClass('disabled');
									}
									if (self.page === 1 && items[index].length < 10) {
										$('.map-paging').css('display', 'none');
									} else {
										$('.map-paging').css('display', 'block');
									}
									if (self.page === 1) {
										$('.map-back-button').addClass('disabled');
									} else {
										$('.map-back-button').removeClass('disabled');
									}
								}
								if (items[index].Name !== undefined) {
									showOneResult(items[index], (self.page-1)*10 + 1);
								} else {
									showResult(items[index]);
								}
							}
						});
						
						
						/*
						$.each (items, function (index) {
							if(items[index].id !== undefined) {
								alert('Không tìm thấy dịch vụ, địa chỉ trên.');
								return false;
							} else {
								var longitude = items[index].Longitude;
								var latitude = items[index].Latitude;
								var pos = new vietbando.LatLng(latitude,longitude);
								marker = addMarker(pos, map, point);
								return;
							}
						});
						*/
					}else {
						alert('Không tìm thấy dịch vụ, địa chỉ trên.');
						marker = null;
					}
				},
			});
			return marker;
		}
		
		function clearResultDiv() {
			$('#ulContDetailWay').empty();
			$('#ulContDetailWayMb').empty();
			$('.panelSearch').empty();
			// console.log("clearResultDiv");
		}
		
		function addPolyLine(listPath) {
			closePolyLine(polyLineShortPath);
			var alatlng = vietbando.Util.ConverALongLat2ALatLng(listPath);
			var mvcObj = new vietbando.MVCArray(alatlng);
			polyLineShortPath = new vietbando.Polyline({ path: mvcObj, map: map, strokeOpacity: 0.5, strokeWidth: 3, drawArrows: true, draggable: true });
			polyLineShortPath.setMap(map);
		}
		
		function showDetailPath(resultScript){
			var spss = resultScript.Section.ShortestPathSection.Segment.ShortestPathSectionSegment;
			
			var strClass1 = "iconmap-directions-01";
			var strClass2 = "iconmap-directions-02";
			var strClass3 = "iconmap-directions-03";//phai
			var strClass4 = "iconmap-directions-04";//trai
			var strClass5 = "iconmap-directions-05";
			$('#ulContDetailWay').empty();
			$('#ulContDetailWayMb').empty();
			$('.panelSearch').empty();
			$('.panelSearch').append("<p id='lblLong' class='tar pr20'>Chiều dài: "+ resultScript.fulllength +" " + resultScript.unit +" </p>");
			$.each (spss, function (idx) {
				var strClass = "iconmap-directions-01";
				var strDirect = "";
				if (spss[idx].direction == HEAD) {
					strClass = "iconmap-directions-01";
					strDirect = HEAD_VN;
				}else if (spss[idx].direction == TURN_RIGHT_ON) {
					strClass = "iconmap-directions-03";
					strDirect = TURN_RIGHT_ON_VN;
				}else if (spss[idx].direction == TURN_LEFT_ON) {
					strClass = "iconmap-directions-04";
					strDirect = TURN_LEFT_ON_VN;
				}else if (spss[idx].direction == CONTINUE_TO_FOLLOW) {
					strClass = "iconmap-directions-01";
					strDirect = CONTINUE_TO_FOLLOW_VN;
				}else if (spss[idx].direction == TURN_TO) {
					strClass = "iconmap-directions-01";
					strDirect = TURN_TO_VN;
				}else if (spss[idx].direction == AT) {
					strClass = "iconmap-directions-01";
					strDirect = AT_VN;
				}
				
				$('#ulContDetailWay').append(
					"<li>"
					+ "<span class='wd-item-w1'><span class='"+ strClass +"'></span></span>"
					+ "<span class='wd-item-w2'>"+idx +". "+ strDirect +" <strong>"+spss[idx].Name+"</strong></span>"
					+ "<span class='wd-item-w3'>"+spss[idx].Length.Value +spss[idx].Length.unit +"</span>"
					+ "</li>"
				);
				$('#ulContDetailWayMb').append(
						"<li>"
						+ "<span class='wd-item-w1'><span class='"+ strClass +"'></span></span>"
						+ "<span class='wd-item-w2'>"+idx +". "+ strDirect +" <strong>"+spss[idx].Name+": </strong></span>"
						+ "<span class='wd-item-w3'>"+spss[idx].Length.Value +spss[idx].Length.unit +"</span>"
						+ "</li>"
					);
			});
			
			$('.wd-content-find-way').mCustomScrollbar();
			$('.wd-content-find-way').resize();
			 
		}
		
		function findShortestPath() {
			var latBegin = markerA.getPosition().lat();
			var lngBegin = markerA.getPosition().lng();
			var latEnd = markerB.getPosition().lat();
			var lngEnd = markerB.getPosition().lng();
			// console.log($('#ulContDetailWay'));
			clearResultDiv();
			// console.log($('#ulContDetailWay'));
			var valueToFindTextBoxAPI = 'map/findShortestPath';
			var param = 'latBegin=' + latBegin + '&lngBegin=' + lngBegin + '&latEnd=' + latEnd + '&lngEnd=' + lngEnd + '&transportType=Motor';
			homeService
				.getDataByPostMethod(valueToFindTextBoxAPI, param)
				.success(
					function(res, status, headers, config) {
						if(res.meta.code == 200) {
							var item = res.data.info.FindShortestPathResult;
							var listPath = item.FullPath.ArrayOfArrayOfdouble.ArrayOfdouble.double;
							var resultScript = item.ResultScript;
							addPolyLine(listPath);
							showDetailPath(resultScript);
						}
                    })
                .error(
                    function(data, status) {
                    	alert('Đã xảy ra lỗi trong khi tìm kiếm.\n Vui lòng thử lại.');
                    })
                ;
		}
		
		/* functions for map popup button*/
		window.btnCloseCircleEvent = function btnCloseCircleEvent() {
			closeCircle(circle);
			circle = null;
			closeInfowindow(infWinCircle);
			infWinCircle = null;
		}
		
		window.timDuongEvent = function (vd, kd){
			//before set marker A
			var pos = new vietbando.LatLng(vd,kd);
			//set marker A : call tuDay()
			tuDay(pos, map);
		}
		
		//Click xungQuanh button in context menu right
		//anvt:13/1/2016: add infoWinSearch event closeclick
		window.btnXungQuanhClick = function(vd, kd ,flag) {
			self.selectedXungQuanh = [];
			closeInfowindow(infowindowService);
			infowindowService = null;
			closeInfowindow(infoWinSearch);			
			infoWinSearch = null;
			closeMarker(markerSearch);
			markerSearch = null;
			appendSearchWindow(vd, kd ,flag, 1000);
		};

		window.appendSearchWindow = function(vd, kd ,flag, banKinh) {
			var strHtmlCombobox;
			var pos = new vietbando.LatLng(vd, kd);
			map.setCenter(pos);
			strHtmlCombobox = "<select class='form-control wdt-service' id=\"cbbLinhVuc\" placeholder=\"Chọn lĩnh vực...\">";
			strHtmlCombobox = strHtmlCombobox + "<option value='"+0+"'/></option>";
			$.each (catalogs, function (idx) {
				if(catalogs[idx].id == catalogId){
					strHtmlCombobox = strHtmlCombobox + "<option selected='selected' value='"+catalogs[idx].id+"'/>"+catalogs[idx].ten+"</option>";
				}else {
					strHtmlCombobox = strHtmlCombobox + "<option value='"+catalogs[idx].id+"'/>"+catalogs[idx].ten+"</option>";
				}
			});
			strHtmlCombobox = strHtmlCombobox + "</select>";
			 
			var strHtmlCheckbox = "<div class='custom-checkbox'><input onclick='selectAllDichVu(event)' tabindex='5' type='checkbox' id='all' class='wd-input-sort'></input><label for='all'>Tất cả</label></div>";
			self.selectedXungQuanhFull = [];
			$.each (catalogs, function (idx) {
				self.selectedXungQuanhFull.push(catalogs[idx].id);
				strHtmlCheckbox = strHtmlCheckbox + 
										"<div class='custom-checkbox'>" +
                                        	"<input tabindex='5' value='" + catalogs[idx].id +"' name='radio-oparator'" + 
	                                        	 "onclick='selectDichVu(event, "+ catalogs[idx].id +")'" +
	                                        	 "type='checkbox' id='id" + catalogs[idx].id + "' class='wd-input-sort'>" +
	                                        "<label for='id"+ catalogs[idx].id + "'>" + catalogs[idx].ten + "</label>" +
                                        "</div>";
			});
			var strHtml =""
							+"<div class='pop-search-service'>"
								+"<label class='title-map-service'>Tìm dịch vụ ở gần vị trí này:</label> <br>"
								+"<div class='pop-search-checkbox'>"
									+"<div>"
										+strHtmlCheckbox
									+"</div>"
								+"</div>"
								+"<div class='pop-search-input'>"
									+"<label class='pop-search-bankinh'>Bán kính(m):</label> <br>"
									+"<input type=\"text\" id='txtMet' value='" + banKinh + "'' class='form-control wdt-service'/><br>"
									+"<a class='bt-search' onclick='timXungQuanhCheckbox(txtMet);'>"
										+"<i class='fa fa-search'></i>Tìm"
									+"</a>"
								+"</div>"
							+"</div>"
						+"";		
			if(flag) {
				markerSearch = new vietbando.Marker({
					position : pos
				});
				markerSearch.setMap(map);
				infoWinSearch = new vietbando.InfoWindow({ position: pos ,content: strHtml,autoPan: false });
				vietbando.event.addListener(infoWinSearch, 'closeclick', function() {
					closeMarker(markerSearch);
					markerSearch = null;
					infoWinSearch = null;
				});
				infoWinSearch.open(map, markerSearch);
			} else {
				infoWinSearch = new vietbando.InfoWindow({ position: pos ,content: strHtml,autoPan: false });	
				infoWinSearch.open(map);
			}
			serviceCheckAll(true);
		}
		
		
		function selectThuocTinh($event, id) {
			//console.log("newPage: " + $scope.currentPage);
			self.page = 1;
        	var checkbox = $event.target;
        	var action = (checkbox.checked ? 'add' : 'remove');
        	var label = $('label[for="' + id + '"]');
        	self.isOnCheckThuocTinh = true;
        	self.isXungQuanh = false;        	
        	if (action === 'add' && self.selected.indexOf(id) === -1) {
        		label.addClass('checked');
        	    self.selected.push(id);
        	}
        	if (action === 'remove' && self.selected.indexOf(id) !== -1) {
        		label.removeClass('checked');
        	    self.selected.splice(self.selected.indexOf(id), 1);
        	}
        	self.checkThuocTinh = self.selected;
        	if (self.checkThuocTinh.length > 0) {
            	getListService(self.page);
        	} else {
        		getServicesById(self.currentServiceId, self.currentServiceCode, self.currentServiceValue);
        	}
        }
		
		window.selectDichVu = function($event, id) {
        	var checkbox = $event.target;
        	var action = (checkbox.checked ? 'add' : 'remove');
        	var label = $('label[for="id' + id + '"]');
        	if (action === 'add' && self.selectedXungQuanh.indexOf(id) === -1) {
        		label.addClass('checked');
        	    self.selectedXungQuanh.push(id);
        	    if(self.selectedXungQuanh.length == self.selectedXungQuanhFull.length) {
        	    	checkAllButton(true);
        	    }
        	}

        	if (action === 'remove' && self.selectedXungQuanh.indexOf(id) !== -1) {
        		label.removeClass('checked');
        	    self.selectedXungQuanh.splice(self.selectedXungQuanh.indexOf(id), 1);
        	    checkAllButton(false);
        	}
        }

        window.checkAllButton = function(check) {
			var checkbox = $('#all');
			var label = $('label[for="all"]');
        	if (check) {
        		label.addClass('checked');
        		checkbox.prop('checked', true);
        	} else {
        		label.removeClass('checked');
        		checkbox.prop('checked', false);
        	}
        }

        //anvt:13/1/2016: service check all on Create window search. param(boolean)
        window.serviceCheckAll = function(checkAll) {
        	
        	if(self.selectedXungQuanh.length == 0 || self.selectedXungQuanh.length >= self.selectedXungQuanhFull.length) {
        		checkAllButton(checkAll);
        		selectAllAction(checkAll);
        	} else {
				selectCurrentActions();
        	}
        }

        //anvt:13/1/2016: adding select all service
        window.selectAllDichVu = function($event) {
        	var selected = [];
        	var checkbox = $event.target;
        	var action = (checkbox.checked ? 'add' : 'remove');
        	var label = $('label[for="all"]');

        	if (action === 'add') {
        		label.addClass('checked');
        	}

        	if (action === 'remove') {
        		label.removeClass('checked');
        	}
        	selectAllAction(checkbox.checked);
        }

        // anvt: chọn tất cả các checkbox dịch vụ và add vào self.selectedXungQuanh
        window.selectAllAction = function(isAdd) {
        	if(isAdd) {
        		self.selectedXungQuanh = [];
        	}
        	$('[name="radio-oparator"]').each(function() {  	
        		var val = parseInt($(this).attr('value'), 10);
			    var label2 = $('label[for="'+ $(this).attr('id') +'"]');
	        	if (isAdd) {
	        		label2.addClass('checked');
	        		$(this).prop('checked', true);
	        		self.selectedXungQuanh.push(val);
	        	} else {
	        		label2.removeClass('checked');
	        		$(this).prop('checked', false);
	        		self.selectedXungQuanh.splice(self.selectedXungQuanh.indexOf(val), 1);
	        	}
			});
        }
		
		window.selectCurrentActions = function() {
        	$('[name="radio-oparator"]').each(function() {  	
        		var val = parseInt($(this).attr('value'), 10);
			    var label2 = $('label[for="'+ $(this).attr('id') +'"]');
			    
	        	if (self.selectedXungQuanh.indexOf(val) > -1) {
	        		label2.addClass('checked');
	        		$(this).prop('checked', true);
	        	} else {
	        		label2.removeClass('checked');
	        		$(this).prop('checked', false);
	        	}
			});
        }

		window.timXungQuanh = function(txtMet, cbbLinhVuc) {
			if($.isNumeric(txtMet.value)) {
				if(cbbLinhVuc.value != 0) {
					var pos = infoWinSearch.getPosition();
					closeInfowindow(infoWinSearch);
					//anvt:14/1/2016
					infoWinSearch = null;
					//------
					var bkMet = txtMet.value;
					var bkKm = txtMet.value / 1000;
					getListXungQuanh(cbbLinhVuc.value, bkKm, pos.lat(), pos.lng(), pos);
				} else { alert('Vui lòng chọn lĩnh vực.');} 
			} else { alert('Vui lòng nhập vào kiểu số.');}
		}
		
		window.timXungQuanhCheckbox = function(txtMet) {
			if($.isNumeric(txtMet.value)) {
				if(self.selectedXungQuanh.size != 0) {
					var pos = infoWinSearch.getPosition();
					closeInfowindow(infoWinSearch);
					//anvt: 14/1/2016
					infoWinSearch = null;
					//-----
					var bkMet = txtMet.value;
					var bkKm = txtMet.value / 1000;
					//console.log("self.selectedXungQuanh.length: " + self.selectedXungQuanh.length);
					if (self.selectedXungQuanh.length == 0) {
						self.selectedXungQuanh = self.selectedXungQuanhFull;
					}
					getListXungQuanh(self.selectedXungQuanh, bkKm, pos.lat(), pos.lng(), pos);
					//anvt:14/1/2016 ẩn dòng nay
					//self.selectedXungQuanh = [];
				} else { alert('Vui lòng chọn lĩnh vực.');} 
			} else { alert('Vui lòng nhập vào kiểu số.');}
		}
		
		window.clickInterest = function(id) {
			if($scope.user !=null) {
				var userId = $scope.user.id;
				var addInterestApi = "comment/service/" + id + "/interest/add";
				var param;
				var check='';
				var cookie = ipCookie('interactions_service'+id+'.nguoidan_dukhach')
				if (cookie!=undefined && cookie==2){
					check='checked';
				}
				if(userId!='' || check==''){
					if(userId==''){
						if(cookie==1)
						{
							ipCookie('interactions_service'+id+'.nguoidan_dukhach', '2',{ expires: 1, path: '/' });
							userId='del';
							$('input.search-street.iconInterest').removeClass('active');
						}
						else{
							ipCookie('interactions_service'+id+'.nguoidan_dukhach', '1',{ expires: 1, path: '/' });
							userId='unlogin';
							$('input.search-street.iconInterest').addClass('active');
						}
					}
					param = 'username='+ userId;
					homeService.getDataByPostMethod(addInterestApi, param).then(function(res) {
						$('#quantam'+id).empty();
						$('#quantam'+id).text(res.data.data.info.count);		
						$('#interest'+id).empty();
						$('#interest'+id).text(res.data.data.info.count);
						$('#tempInterest'+id).empty();
						$('#tempInterest'+id).text(res.data.data.info.count);
						if (res.data.data.info.content == 'daquantam') {
							$('input.search-street.iconInterest').addClass('active');
						} else {
							$('input.search-street.iconInterest').removeClass('active');
						}
		            });
				} else {
					if(check !='') {
						alert("Bạn đã sử dụng chức năng này. Bạn phải đợi 24h nữa mới được thao tác tiếp");
					}
				}
			} else {
				alert('Vui lòng đăng nhập để sử dụng chức năng này.');
			}
		}
		
		/* functions common */
		function Resize() {
			var dom = document.getElementById('map');
			var containerMap = document.getElementById('wd-container-map');
			var headerMap = $('#wd-header-map').prop('scrollHeight'); 			
			var headerMapMobile = $('#wd-header-map-mobile').prop('scrollHeight');			
			var headerMapSearchMobile = $('#wd-header-map-search').prop('scrollHeight');
			var headcontainer = $('#wd-head-container').prop('scrollHeight'); 	
			var menudesktop = $('#wd-menu-desktop').prop('scrollHeight'); 	
			if (headerMap != 0) {
				headerMap +=1;
			}
			if (headerMapMobile != 0) {
				headerMapMobile +=1;
			}
			if (headerMapSearchMobile != 0) {
				headerMapSearchMobile +=1;
				//var hamburger = $('.wd-header-map-hamburger').width();
				//var textSearch = document.getElementById('textSearchMobile');
				//textSearch.style.width = $(window).width() - hamburger - 72 - 16 + 'px';;
			}
			//containerMap.style.marginTop = headerMap + headerMapMobile  + headerMapSearchMobile + 'px' ;
			//dom.style.height = $(window).height() - headerMap - headerMapMobile - headerMapSearchMobile + 'px' ;
			//console.log("headcontainer: " + headcontainer);
			//console.log("menudesktop: " + menudesktop);
			//console.log("marginTop: " + (headcontainer + menudesktop));
			containerMap.style.marginTop = headcontainer + menudesktop + 16 + 'px' ;
			dom.style.height = $(window).height() - headcontainer - menudesktop + 'px';
		}

		function heightContent() {
			// height catogory
			var h1 = 0;
			var this_LeftContenMap		=	$('.wd-left-map').outerHeight();
			var this_MainMap 			= $('.wd-main-container-map').outerHeight();
			var this_TopLeftContenMap	=	$('#wd-top-left-m-pc').outerHeight();
			var this_TopLeftContenMapMobile =  $('#wd-top-left-m-mb').outerHeight();
			var this_TopSortBy	=	$('#wd-sortby-top-pc').outerHeight();
			var this_TopSortByMobile	=	$('#wd-sortby-top-mb').outerHeight();
			var this_HeaderMapSearch = $('#wd-header-map-search').outerHeight();
			var this_MainLeftContenMap	=	$('.wd-scroll-left-m');			
			if (this_TopLeftContenMapMobile > 0) {this_TopLeftContenMapMobile = 39;}
			if (this_TopSortByMobile > 0) {
				if (self.searchCountView) {
					this_TopSortByMobile = 121;
				} else {
					this_TopSortByMobile = 105;
				}
			}
			var height2 = this_MainMap  - this_TopLeftContenMap - this_TopLeftContenMapMobile - this_TopSortBy - this_TopSortByMobile;
			if (this_TopSortBy === 0) {
				height2 = height2 - this_HeaderMapSearch;
			}
			this_MainLeftContenMap.height(height2);
			this_MainLeftContenMap.mCustomScrollbar();

			// height find way
			var this_TopFindWay		=	$('.wd-top-find-way').outerHeight();
			var this_ContentFindWay	=	$('.wd-content-find-way');
			var this_FindWayMobile = $('#top-find-way-mb').outerHeight();
			var this_FindWay = $('#top-find-way').outerHeight();
			var this_ContentFindWayHeight 	=	this_ContentFindWay.outerHeight();
			if (this_FindWayMobile > 0) {this_FindWayMobile = 131;}
			//var height_FindWay = this_LeftContenMap - this_TopLeftContenMap - this_TopFindWay - this_BottomFindWay;
			var height_FindWay = this_MainMap - this_TopLeftContenMap - this_TopLeftContenMapMobile - this_FindWay - this_FindWayMobile;
			
			if (h1 == 0 || h1 == this_TopLeftContenMap) {
				h1 = this_TopLeftContenMap + this_TopFindWay + this_ContentFindWayHeight;
			}
			// console.log("this_MainMap" + this_MainMap);
			// console.log("this_TopLeftContenMap: " + this_TopLeftContenMapMobile);
			// console.log("this_FindWay: " + this_FindWay);
			// console.log("this_FindWayMobile: " + this_FindWayMobile);
			// console.log("height_FindWay: " + height_FindWay);
			
			this_ContentFindWay.css('max-height', height_FindWay + 'px');
			if (this_FindWay > 0) {
				this_ContentFindWay.css('min-height', '0px');
			}
			this_ContentFindWay.mCustomScrollbar();
		}
	}
	
	function removeSpecialLetter(s) {
    	var str = s.trim().toLowerCase();
    	str = str.replace(/à/g, 'a');
    	str = str.replace(/á/g, 'a');
    	str = str.replace(/ạ/g, 'a');
    	str = str.replace(/ả/g, 'a');
    	str = str.replace(/ã/g, 'a');
    	str = str.replace(/â/g, 'a');
    	str = str.replace(/ầ/g, 'a');
    	str = str.replace(/ấ/g, 'a');
    	str = str.replace(/ậ/g, 'a');
    	str = str.replace(/ẩ/g, 'a');
    	str = str.replace(/ẫ/g, 'a');
    	str = str.replace(/ă/g, 'a');
    	str = str.replace(/ằ/g, 'a');
    	str = str.replace(/ắ/g, 'a');        	
    	str = str.replace(/ặ/g, 'a');
    	str = str.replace(/ẳ/g, 'a');
    	str = str.replace(/ẵ/g, 'a');        	
    	str = str.replace(/đ/g, 'd');        	
    	str = str.replace(/è/g, 'e');
    	str = str.replace(/é/g, 'e');
    	str = str.replace(/ẹ/g, 'e');
    	str = str.replace(/ẻ/g, 'e');
    	str = str.replace(/ẽ/g, 'e');
    	str = str.replace(/ê/g, 'e');        	
    	str = str.replace(/ề/g, 'e');
    	str = str.replace(/ế/g, 'e');
    	str = str.replace(/ệ/g, 'e');
    	str = str.replace(/ể/g, 'e');
    	str = str.replace(/ễ/g, 'e');
    	str = str.replace(/ì/g, 'i');
    	str = str.replace(/í/g, 'i');
    	str = str.replace(/ị/g, 'i');
    	str = str.replace(/ỉ/g, 'i');
    	str = str.replace(/ĩ/g, 'i');
    	str = str.replace(/ò/g, 'o');
    	str = str.replace(/ó/g, 'o');
    	str = str.replace(/ọ/g, 'o');
    	str = str.replace(/ỏ/g, 'o');
    	str = str.replace(/õ/g, 'o');
    	str = str.replace(/ô/g, 'o');        	
    	str = str.replace(/ồ/g, 'o');
    	str = str.replace(/ố/g, 'o');
    	str = str.replace(/ộ/g, 'o');
    	str = str.replace(/ổ/g, 'o');
    	str = str.replace(/ỗ/g, 'o');
    	str = str.replace(/ơ/g, 'o');        	
    	str = str.replace(/ờ/g, 'o');
    	str = str.replace(/ớ/g, 'o');
    	str = str.replace(/ợ/g, 'o');
    	str = str.replace(/ở/g, 'o');
    	str = str.replace(/ỡ/g, 'o');        	
    	str = str.replace(/ù/g, 'u');
    	str = str.replace(/ú/g, 'u');
    	str = str.replace(/ụ/g, 'u');
    	str = str.replace(/ủ/g, 'u');
    	str = str.replace(/ũ/g, 'u');
    	str = str.replace(/ư/g, 'u');        	
    	str = str.replace(/ừ/g, 'u');
    	str = str.replace(/ứ/g, 'u');
    	str = str.replace(/ự/g, 'u');
    	str = str.replace(/ử/g, 'u');
    	str = str.replace(/ữ/g, 'u');
    	str = str.replace(/ỳ/g, 'y');
    	str = str.replace(/ý/g, 'y');
    	str = str.replace(/ỵ/g, 'y');
    	str = str.replace(/ỷ/g, 'y');
    	str = str.replace(/ỹ/g, 'y');
    	return str;
    }
	
	function getNumber(num) {
        return new Array(num);
    }
	
})();



