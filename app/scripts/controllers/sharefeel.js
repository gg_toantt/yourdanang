﻿;
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name nddkApp.controller:ShareCtrl
   * @description # ShareCtrl Controller of the nddkApp
   */
  angular.module('nddkApp').controller('ShareFellCtrl', shareFellCtrl)
      .controller('AddShareFeelCtrl', addShareFeelCtrl)
      .config(['noCAPTCHAProvider', function (noCaptchaProvider) {
      noCaptchaProvider.setSiteKey('6LeQkwgTAAAAAIIk8kKnChhlGBwVvDtCsW_cDVQG');
      noCaptchaProvider.setTheme('dark');
  }]);

  shareFellCtrl.$inject = [ '$scope', 'ShareFellService', 'homeService',
      'CONSTANT', 'ngDialog', '$rootScope' ];

  function shareFellCtrl($scope, ShareFellService, homeService, CONSTANT,
      ngDialog, $rootScope) {

    CONSTANT.lang = homeService.getLanguage();

    var self = this;

    // public properties
    self.EMAIL_REGEXP = CONSTANT.emailregexp;

    // public method
    self.getResultsPage = getResultsPage;
    self.zoomInOutContent = zoomInOutContent;
    self.openDefault = openDefault;
    self.loading = false;

    // scope
    $scope.shareFeelList = [];
    $scope.topAttractions = [];
    $scope.everyPage = 12;
    $scope.totalShares = 0;

    // get list share feel when controller be created
    getResultsPage(1);

    function getResultsPage(pageNumber) {
      // get list of share feel
      self.loading = true;
      ShareFellService.getListShare('share', {
        'page' : pageNumber
      }).then(function(res) {
        $scope.shareFeelList = res.data.items.data;
        $scope.totalShares = res.data.items.count * $scope.everyPage;
         // check meta data
            homeService.checkMetaState();
            setTimeout(function() {
	 			$rootScope.htmlReady();
	 		}, 100);
        self.loading = false;
      });
    }

    // get 5 newsest news
    var topNewsApi = "services/" + CONSTANT.lang
        + "/12_attractive_destinations";
    homeService.getData(topNewsApi).then(function(res) {
      $scope.topAttractions = res.data.items;
    });

    // open dialog add share
    function openDefault() {
      if (homeService.isLoggedIn()) {

        ngDialog.open({
          template : 'addShareFeelDialog',
          className : 'ngdialog-theme-default custom-width-share-popup',
          controller : 'AddShareFeelCtrl'
        });
      } else {
        homeService.requriedLogin();
      }
    }
    ;

  }

  function zoomInOutContent(share) {
    share.limitContent = share.limitContent != 160 ? 160 : share.noiDung.length;
  }

  // Add share feel controller constructor
  addShareFeelCtrl.$inject = [ '$scope', 'homeService', 'CONSTANT', 'ngDialog',
      '$cookieStore', 'ipCookie' ];
  function addShareFeelCtrl($scope, homeService, CONSTANT, ngDialog,
      $cookieStore, ipCookie) {
    var self = this;
    self.captchaEmpty = true;
    self.clicked = false;

    // public proprties
    self.EMAIL_REGEXP = CONSTANT.emailregexp;
    self.input = {
      hoten : '',
      email : '',
      tieuDe : '',
      noiDung : ''
    };

	if (angular.isDefined(ipCookie('user')) && ipCookie('user') != null) {
		self.input.hoten = ipCookie('user').displayname;
		self.input.email = ipCookie('user').email;
	} else {
		self.input.hoten = '';
		self.input.email = '';
	}

    // public method
    self.closeDialog = closeDialog;
    self.createShareFeel = createShareFeel;

    //init method
    // margin center dialog after created
    homeService.locateDialog();

    // scope for add share feel
    $scope.shrCtrl = self;

    // close dialog add share
    function closeDialog() {
      ngDialog.close();
    };

    // add sharefeel
    function createShareFeel($event) {
      self.clicked = true;
      self.captchaEmpty = false;
      $event.preventDefault;
      if ($scope.addForm.$valid) {
        
        if (!angular.isUndefined($scope.gRecaptchaResponse)) {
          self.captchaEmpty = false;
          var url = "share/add";
          var param = "hoten=" + self.input.hoten + "&email=" + self.input.email
              + "&tieuDe=" + self.input.tieuDe + "&noiDung=" + self.input.noiDung
              + "&userID=" + homeService.getCurrentLoginUser().id;
          homeService.getDataByPostMethod(url, param).success(
              function(data, status, headers, config) {
                if (data.meta.code == 200) {
                  ngDialog.close();
                  homeService.showNotification(homeService
                      .updateLanguageByCode(CONSTANT.lang).guiChiaSeThanhCong,
                      2500);
                } else {
                  homeService.showNotification(homeService
                      .updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,
                      2500);
                }
                $scope.noCaptchaControl.reset();
                $scope.gRecaptchaResponse = undefined;
              }).error(
              function(data, status) {
                homeService.showNotification(homeService
                    .updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui,
                    2500);
              });
            } else {
              self.captchaEmpty = true;
              self.clicked = true;
            }
      }
    }
    ;
  }
})();
