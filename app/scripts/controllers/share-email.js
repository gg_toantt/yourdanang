;(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:ShareCtrl
	 * @description # ShareCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp')
	.controller('ShareEmailCtrl', shareEmailCtrl)
	.config(['noCAPTCHAProvider', function (noCaptchaProvider) {
	    noCaptchaProvider.setSiteKey('6LeQkwgTAAAAAIIk8kKnChhlGBwVvDtCsW_cDVQG');
	    noCaptchaProvider.setTheme('dark');
	}]);

	shareEmailCtrl.$inject = [ '$scope', 'homeService', 'CONSTANT', 'ngDialog' ];

	function shareEmailCtrl($scope, homeService, CONSTANT, ngDialog) {


	CONSTANT.lang = homeService.getLanguage();
	var self = this;

	// public properties
	self.EMAIL_REGEXP = CONSTANT.emailregexp;
	self.input = {
		hotenGui : '',
		emailGui : '',
		emailNhan : '',
		tieuDe : $scope.ngDialogData.tieuDe,
		loiNhan : '',
		link : $scope.ngDialogData.link
	};
	// public method
	self.shareArticleByEmail = shareArticleByEmail;

	// scope for add share feel
	$scope.shrCtrl = self;

	// margin center dialog after created
	homeService.locateDialog();
	  
	// close dialog add share
	function closeDialog() {
		ngDialog.close();
	};

	// send a article link to a email
	function shareArticleByEmail() {
        $scope.submitted = true;
        if ($scope.addForm.$valid) {
          if (!angular.isUndefined($scope.gRecaptchaResponse)) {
            $scope.captchaEmpty = false;
              var param = "emailRecipient=" + self.input.emailNhan
                  + "&fullNameSender=" + self.input.hotenGui
                  + "&emailSender=" + self.input.emailGui
                  + "&contentAttack=" + self.input.loiNhan
                  + "&linkArticle=" + self.input.link
                  + "&titleArticle=" + self.input.tieuDe;
              
              homeService.postData("sendMailArticle", param)
                .success(
                    function(data, status, headers, config) {
                        if (data.meta.code == 200) {
                          closeDialog();
                          homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiMailThanhCong, 2000);
                        } else {
                            homeService.showNotification("Khong thanh cong", 2000);
                        }
                })
                .error(function(data, status) {
                    //
                });
          }else{
            $scope.captchaEmpty = true;
          }
        }
	  }
	}
})();
