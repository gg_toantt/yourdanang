;
(function() {
  'use strict';

  /**
   * @ngdoc function
   * @name nddkApp.controller:EventBannerCtrl
   * @description # EventBannerCtrl Controller of the nddkApp
   */
  angular.module('nddkApp').controller('EventBannerCtrl', EventBannerCtrl);

  EventBannerCtrl.$inject = [ '$scope', 'homeService', 'Scopes' ];

  function EventBannerCtrl($scope, homeService, Scopes) {

    var self = this;
    Scopes.store('EventBannerCtrl', self);
    // public properties

    // public methods
    self.clickBanner = clickBanner;
    self.impressionBanner = impressionBanner;

    /* Scope */
    $scope.eventBnCtrl = self;

    function clickBanner(id) {
      var param = '';

      $(document).ready(
          function() {
            $.get('http://jsonip.com', function(res) {
              param = 'idBanner=' + id + '&ipAddressClient=' + res.ip;

              homeService.postData('advertising/click', param).success(
                  function(data, status, headers, config) {
                    if (data.meta.code == 200) {
                      // console.log('code == 200');
                      // console.log(data);
                    } else {
                      // console.log('code != 200');
                    }
                  }).error(function(data, status) {
                // console.log('Error');
              });
            });
          });
    }

    function impressionBanner(id) {
      var param = '';
      $(document).ready(
          function() {
            $.get('http://jsonip.com', function(res) {
              param = 'idBanner=' + id + '&ipAddressClient=' + res.ip;

              homeService.postData('advertising/impressions', param).success(
                  function(data, status, headers, config) {
                    if (data.meta.code == 200) {
                      // console.log('code == 200');
                      // console.log(data);
                    } else {
                      //							console.log('code != 200');
                    }
                  }).error(function(data, status) {
                //						console.log('Error');
              });
            });
          });
    }
  }
})();