;
(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:HomeCtrl
	 * @description # HomeCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp').controller('registrationCtrl', registrationCtrl)
	.config(['noCAPTCHAProvider', function (noCaptchaProvider) {
	    noCaptchaProvider.setSiteKey('6LeQkwgTAAAAAIIk8kKnChhlGBwVvDtCsW_cDVQG');
	    noCaptchaProvider.setTheme('dark');
	}]);

	registrationCtrl.$inject = [ '$scope', '$timeout', 'registrationService', 'CONSTANT', '$state', 'homeService', 'Scopes','$stateParams' ];

	function registrationCtrl($scope, $timeout, registrationService, CONSTANT, $state, homeService, Scopes, $stateParams) {

		CONSTANT.lang = homeService.getLanguage();

		var self = this;
		// public properties
		self.addBusiness = false;
		self.businessSuccess = false;

		if($state.current.name == 'register-enterprise') {
			self.addBusiness = true;
			self.isBusinessFace = true;
			self.businessSuccess = true;
		}

		self.agreeRole = false;
		self.captchaEmpty = true;

		// public methods
		self.addUser = addUser;
		self.isAddBusiness = isAddBusiness;
		self.toggleBusinessInfo = toggleBusinessInfo;
		self.changeAgreeRole = changeAgreeRole;
		self.isAgreeRole = isAgreeRole;
		
		// public methods
		self.addUser = addUser;
		self.isAddBusiness = isAddBusiness;
		self.toggleBusinessInfo = toggleBusinessInfo;
		self.changeAgreeRole = changeAgreeRole;
		self.isAgreeRole = isAgreeRole;
		self.openPopupLogin = openPopupLogin;

		/* Add new user */
		$scope.username = "";
		$scope.password = "";
		$scope.password_confirm = "";
		$scope.email = "";
		$scope.msgs = "";
		// $scope.firstname = "";
		// $scope.lastname = "";
		$scope.fullName = "";
		$scope.business_name = "";
		$scope.business_taxid = "";
		$scope.business_address = "";
		$scope.business_email = "";
		$scope.business_phone = "";
		$scope.submitted = false; // form validated when submit button clicked
		$scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;

		function openPopupLogin() {
			Scopes.get("LoginCtrl").openPopup();
		}

		// implements
		function addUser() {
			$scope.submitted = true;
		 	if (!angular.isUndefined($scope.gRecaptchaResponse)) {
		 		self.captchaEmpty = false;
				if ($scope.myForm.$valid) {
					var userRegistration = "username=" + $scope.email + "&password=" + $scope.password + "&password_confirm=" + $scope.password_confirm + "&email=" + $scope.email
					    + "&fullName=" + $scope.fullName;
					if (self.isAddBusiness()) {
						if ($scope.businessForm.$valid) {
							var businessInfor = "business_name=" + $scope.business_name + "&business_address=" + $scope.business_address
							    + "&business_phone=" + $scope.business_phone;

							userRegistration += "&" + businessInfor;
						}
					}
					userRegistration += "&isBusiness=" + self.isAddBusiness();

					if (!(self.isAddBusiness() ^ $scope.businessForm.$valid)) {
						registrationService.createUser('users', userRegistration).success(function(data, status, headers, config) {
	//						console.log(data);
	//						console.log(data.meta.message);
							if (data.meta.code == 200) {
								if (self.addBusiness) {
									self.toggleBusinessInfo();
								}
								// self.changeAgreeRole();
								$state.go('register-succ', {
									registedEmail : $scope.email,
									business : self.businessSuccess
								});
							} else {
								$scope.msgs = data.meta.message;
							}
						}).error(function(data, status) {
							$scope.msgs = data.meta.message;
						});
					}
				}
			} else {
				self.captchaEmpty = true;
			}
		}

		function toggleBusinessInfo() {
			self.addBusiness = !self.addBusiness;
		}

		function changeAgreeRole() {
			self.agreeRole = !self.agreeRole;
		}

		function isAddBusiness() {
			return angular.isUndefined(self.addBusiness) ? false : self.addBusiness;
		}

		function isAgreeRole() {
			return self.agreeRole;
		}

	};

	

})();
