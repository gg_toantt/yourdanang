;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('residenceService', residenceService);

    residenceService.$inject = [ '$scope', '$timeout', 'homeService',
            'CONSTANT', '$stateParams', '$rootScope', 'ngDialog', '$location' ];

    function residenceService($scope, $timeout, homeService, CONSTANT,
        $stateParams, $rootScope, ngDialog, $location) {
		CONSTANT.lang = homeService.getLanguage();
        var self = this;

        // public properties
        self.fieldId = $stateParams.fieldId;
        self.widthDevice = $('body').outerWidth();
        self.keyword = '';
        self.page = 1;
        self.checkThuocTinh = '';
        self.sortType = "rating_";
        self.sortOptional = 'desc';
        self.sort = self.sortType+self.sortOptional;
        self.selectedService = {};
        self.listField = [];
        self.gridLayout = true;
        self.selected = [];
        self.listAttributeOfField = [];
        self.showModalFillter = false;


        // public methods
        self.getListService = getListService;
        self.getSelectedService = getSelectedService;
        self.changeLayOut = changeLayOut;
        self.getNumber = getNumber;
        self.getListAttribute = getListAttribute;
        self.changeSort = changeSort;
        self.selectThuocTinh = selectThuocTinh;
        self.attributeClick = attributeClick;
        self.showPopup = showPopup;
        self.closePopup = closePopup;
        self.loading = false;

        /* Scope */
        $scope.serviceCtrl = self;
        $scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
        $scope.listService = [];
        $scope.everyPage = 12;
        $scope.total = 0;
        $scope.listId = '';

        /* init */
        getListService(self.page);
        getSelectedService();
        getListField();
        getListAttribute();
        equalElementHeight();

        function showPopup() {
            self.showModalFillter = true;
        }

        function closePopup() {
            self.showModalFillter = false;
        }


        $scope.clickPage = function() {
            if ($('body').outerWidth() > 736) {
            	setTimeout(function() {
                    if (self.gridLayout) {
                        if ($scope.listService.length > 0) {
                        var n = 0;
                        var m = 0;

                        for (var h = 0; h < parseInt($scope.listService.length / 3); h++) {
                            var arrIdDiv = [];
                            var maxHeight = 0;
                            for (var i = m; i < m + 3; i++) {
                                arrIdDiv.push($('#service' + $scope.listService[i].id).outerHeight());
                            }

                            for (var j = 0; j < arrIdDiv.length; j++) {
                                if (arrIdDiv[j] > maxHeight) {
                                    maxHeight = arrIdDiv[j];
                                }
                            }

                            for (var k = m; k < m + 3; k++) {
                                $('#service' + $scope.listService[k].id).outerHeight(maxHeight);
                            }

                            n++;
                            m = m + 3;
                        }
                        if ($scope.listService.length % 3 != 0) {
                            arrIdDiv = [];
                            maxHeight = 0;
                            for (var n = m; n < m + $scope.listService.length % 3; n++) {
                                arrIdDiv.push($('#service' + $scope.listService[n].id).outerHeight());
                            }
                            for (var j = 0; j < arrIdDiv.length; j++) {
                                if (arrIdDiv[j] > maxHeight) {
                                    maxHeight = arrIdDiv[j];
                                }
                            }

                            for (var k = m; k < m + $scope.listService.length % 3; k++) {
                                $('#service' + $scope.listService[k].id).outerHeight(maxHeight);
                            }
                        }
                    }
                    }
            		
                }, 200);
            }
        };
        
        /* implements method */
        function getListService(numberPage) {
            self.loading = true;
            var listServiceApi = "services/" + CONSTANT.lang + "/" + self.fieldId
            		+ "?keyword=" + self.keyword
            		+ "&page="+ numberPage
            		+ "&checkThuocTinh="+ self.checkThuocTinh
            		+ "&sort="+ self.sort;
            /*var list = "services/vn/vui-choi-giai-tri?page=1&sort=xep_hang_desc&checkThuocTinh=1"*/
            homeService.getData(listServiceApi).then(function(res) {

                $scope.listService = res.data.items.data;
                $scope.total = res.data.items.count * $scope.everyPage;

                if (res.data.items.data.length > 0) {
                	self.page = numberPage;
                	var listId = res.data.items.data[0].id;
                    for (var i = 1; i < res.data.items.data.length; i++) {
                    	listId = listId + ',' + res.data.items.data[i].id;
                    }
                    $scope.listId = listId;
                    if ($('body').outerWidth() > 736) {
                        setTimeout(function() {
                            if (self.gridLayout) {
                            	if ($scope.listService.length > 0) {
                        			var n = 0;
                        			var m = 0;

                        			for (var h = 0; h < parseInt($scope.listService.length / 3); h++) {
                        				var arrIdDiv = [];
                        				var maxHeight = 0;
                        				for (var i = m; i < m + 3; i++) {
                        					arrIdDiv.push($('#service' + $scope.listService[i].id).outerHeight());
                        				}

                        				for (var j = 0; j < arrIdDiv.length; j++) {
                        					if (arrIdDiv[j] > maxHeight) {
                        						maxHeight = arrIdDiv[j];
                        					}
                        				}

                        				for (var k = m; k < m + 3; k++) {
                        					$('#service' + $scope.listService[k].id).outerHeight(maxHeight);
                        				}

                        				n++;
                        				m = m + 3;
                        			}

                        			if ($scope.listService.length % 3 != 0) {
                        				arrIdDiv = [];
                        				maxHeight = 0;
                            			for (var n = m; n < m + $scope.listService.length % 3; n++) {
                            				arrIdDiv.push($('#service' + $scope.listService[n].id).outerHeight());
                            			}
                            			for (var j = 0; j < arrIdDiv.length; j++) {
                        					if (arrIdDiv[j] > maxHeight) {
                        						maxHeight = arrIdDiv[j];
                        					}
                        				}

                            			for (var k = m; k < m + $scope.listService.length % 3; k++) {
                        					$('#service' + $scope.listService[k].id).outerHeight(maxHeight);
                        				}
                        			}
                    			}
                            }
                        }, 10);
                    }
                }

             // create meta header
                var metaJson = {};
                if(!angular.isUndefined($scope.listService[0])){
                    metaJson = {"titlePage" : "Dịch vụ - " + $scope.listService[0].linhVuc.ten,
                        "metaKeywords" : "Dịch vụ - " + $scope.listService[0].linhVuc.ten,
                        "metaDescription" : $scope.listService[0].linhVuc.moTa,
                        "thumbNailUrl" : ""};

                }else{
                    metaJson = {"titlePage" : "Dịch vụ",
                            "metaKeywords" : "Dịch vụ",
                            "metaDescription" : "Dịch vụ",
                            "thumbNailUrl" : "",
                            "siteUrl" : $location.$$absUrl};
                }
                homeService.checkMetaState(metaJson);
                setTimeout(function() {
    	 			$rootScope.htmlReady();
    	 		}, 100);

                self.loading = false;
            });
        }

        /* Get selected field*/
        function getSelectedService() {
            var serviceDetailApi = "services/" + CONSTANT.lang + "/" + self.fieldId + "/detail";
            homeService.getData(serviceDetailApi).then(function(res) {
                self.selectedService = res.data.info;
            });
        }

        /* Get list all of field */
        function getListField() {
            homeService.getData('services/vn/field/home').then(function(res) {
                self.listField = res.data.items;
            });
        }

        /* Get list attribute of a field */
        function getListAttribute(){
            var attributeApi = "services/" + CONSTANT.lang + "/"
            + self.fieldId + "/attribute";
            homeService.getData(attributeApi).then(function (res){
                self.listAttributeOfField = res.data.items;
            });

        }

        /* when click sort button */
        function changeSort(sortType){
            if(self.sortType == sortType){
                if(self.sortOptional == "desc"){
                    self.sortOptional = "asc";
                }else{
                    self.sortOptional = "desc";
                }
            }
            self.sortType = sortType;
            self.sort = self.sortType + self.sortOptional;
            getListService(self.page);
        }

        function attributeClick(e) {
            var ele = e.target.parentElement.parentElement.parentElement.getElementsByClassName("wd-portlet-body");
            var eleQ = $(ele[0]);
            if (eleQ.is(':visible')) {
                eleQ.slideUp();
                e.target.parentElement.classList.add("active2");
            } else {
                eleQ.slideDown();
                e.target.parentElement.classList.remove("active2");
            }
        }


        /* change type of layout*/
        function changeLayOut(layout){
            if (layout == 'luoi') {
            	self.gridLayout = true;
                $scope.clickPage();
            } else if (layout == 'hang-ngang') {
            	self.gridLayout = false;
            }
        }

        function selectThuocTinh($event, id) {
        	var checkbox = $event.target;
        	var action = (checkbox.checked ? 'add' : 'remove');
        	var label = $('label[for="' + id + '"]');
        	if (action === 'add' && self.selected.indexOf(id) === -1) {
        		label.addClass('checked');
        	    self.selected.push(id);
        	}
        	if (action === 'remove' && self.selected.indexOf(id) !== -1) {
        		label.removeClass('checked');
        	    self.selected.splice(self.selected.indexOf(id), 1);
        	}
        	self.checkThuocTinh = self.selected;
        	getListService(self.page);
        }

    }

    /* convert a number become a array, using for ng-repeat with a number */
    function getNumber(num) {
        return new Array(num);
    }

    function equalElementHeight(){
    	var byRow = $('body');
    	// apply matchHeight to each item container's items
    	$('.wd-pofolio').each(function() {
    		$(this).children('.item').children('.wd-place').matchHeight({
                byRow: byRow
            });
    	});
    }

})();
