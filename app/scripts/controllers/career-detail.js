;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:careerDetailCtrl
     * @description # careerDetailCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('careerDetailCtrl', careerDetailCtrl);

    careerDetailCtrl.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT', '$stateParams', '$rootScope', '$location' ];

    function careerDetailCtrl($scope, $timeout, homeService, CONSTANT, $stateParams, $rootScope, $location) {

		CONSTANT.lang = homeService.getLanguage();
        var self = this;
        $scope.serviceDetailCtrl = self;

        // public properties
        self.tuyenDungId = $stateParams.fieldId;
        self.detail = {};
        self.relatedJobs = [];

        $scope.careerDetailCtrl = self;
        
        //public method
        self.getCareerDetail = getCareerDetail;
		    
        /* init */
        getCareerDetail();
        getRelatedJobs();

        function getCareerDetail() {
            var careerDetailApi = "tuyenDungs/" + self.tuyenDungId;
            
            homeService.getData(careerDetailApi).then(function(res) {
                if (res.meta.code == 200) {
                    self.detail = res.data.info;
                    homeService.checkMetaState(
                        {"titlePage" : self.detail.tieuDe, 
                         "metaKeywords" : self.detail.tieuDe, 
                         "metaDescription" : self.detail.mieuTaCongViec, 
                         "thumbNailUrl" : "",
                         "siteUrl" : $location.$$absUrl});
                }
           });
        }

        function getRelatedJobs() {
            /* http://api.yourdanang.vn/a/articles/vn/44/related_articles */
            homeService.getData(
                'tuyenDungs/' + self.tuyenDungId + '/related_jobs')
                .then(function(res) {
                    if (res.meta.code == 200 && res.data.items != null) {
                        self.relatedJobs = res.data.items;
                    }
               } 
            );
        }
	};
})();