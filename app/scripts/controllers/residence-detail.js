;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('residenceServiceDetail', residenceServiceDetail);

    residenceServiceDetail.$inject = [ '$scope', 'ngDialog', '$timeout', 'homeService', 
            'CONSTANT', '$stateParams', 'activeService', 'ipCookie', 'Scopes','$window', '$rootScope', '$location' ];

    function residenceServiceDetail($scope, ngDialog, $timeout, homeService, CONSTANT, $stateParams, activeService, ipCookie, Scopes,$window, $rootScope, $location) {

		CONSTANT.lang = homeService.getLanguage();
        var self = this;
        
        // public properties
        self.fieldId =  $stateParams.fieldId;
        self.serviceId = $stateParams.serviceUrl.substring($stateParams.serviceUrl.lastIndexOf('-') + 1, $stateParams.serviceUrl.length);
        self.detail = null;
        self.detailAttribute={};
        self.serviceField={};
        self.serviceDeals={};
        self.serviceAttrs={};
        self.listComment=[];
        self.serviceRelated={};
        self.interest;
        self.flag = false;
        
        
        //public method
        self.showHideCommentBox = showHideCommentBox;
        self.addReplyOfComment = addReplyOfComment;
        self.addComment = addComment;
        self.focusTextbox = focusTextbox;
        self.isBlank = isBlank;
        self.clickInterest = clickInterest;
        self.clickDatCho = clickDatCho;
        self.clickResize = clickResize;
        self.checkLoginRequired = checkLoginRequired;
        self.getListComment = getListComment;
        self.getBookingType = getBookingType;
        self.loading = false;
        
        /* Scope */
        
        //$scope = self;
        self.commentContent = '';
        self.replyCommentContent = '';
        $scope.everyPage = 10;
        $scope.totalComment = 0;
		
		
        $scope.getNumber = function(num) {
            return new Array(num);   
        };
        
        $scope.gotoComment = function() {
			$location.hash('scroll-location');
			$anchorScroll();
        };
		    
        /* init */
        getServiceDetail();
        getServiceField();
        getServiceDeals();
        getServiceAttrs();
        getListComment(1);
        getServiceRelated();
        createServiceRating();
        getScoreRating();
        getCountInterest();
        
        function clickResize(mode) {
        	if (mode == 'xemTatCa') {
        		self.flag = true;
        	} else if (mode == 'thuGon') {
        		self.flag = false;
        	}
        }
        
        function clickDatCho(obj) {
        	if (angular.isDefined(Scopes.get('ShoppingCtrl'))) {
        		Scopes.get('ShoppingCtrl').list = obj;	
        		
        		ngDialog.open({
				    template : 'templateOrderDichVu',
				    className : 'ngdialog-theme-default custom-width-order',
				    controller : [ '$scope', 'registrationService', 'Scopes', '$cookieStore', 'homeService',
				                   function($scope, registrationService, Scopes, $cookieStore) {

				    	// margin center dialog after created
		            	homeService.locateDialog();
				    	
				    	$scope.object = Scopes.get('ShoppingCtrl').list;
			    		$scope.tenLinhVuc = $scope.object.tenLinhVuc;
						$scope.ten = $scope.object.ten;
						$scope.xepHang = $scope.object.xepHang;
						
						$scope.getNumber = function(num) {
				            return new Array(num);
				        };
				    	
				    	$scope.addGioHang = function(list) {
				        	
				    		if (angular.isUndefined(list.inforamtionAddition)) {
				    			list.inforamtionAddition = '';
				    		}
				    		
				        	if (list.amount == null) {
				        		$scope.msgQuantity = 'Bạn vui lòng nhập số lượng !';
				        		$(".show-error.show-error-quanlity.mt10").show();
				        		$(".show-error.show-error-area").hide();
				        	} else if (list.amount < 1) {
				        		$scope.msgQuantity = 'Bạn vui lòng nhập số lượng > 0 !';
				        		$(".show-error.show-error-quanlity.mt10").show();
				        		$(".show-error.show-error-area").hide();
				        	} else if (list.inforamtionAddition == '' || list.amount == '') {
				        		$(".show-error.show-error-area").show();
				        		$(".show-error.show-error-quanlity.mt10").hide();
				        		$scope.msgInfo = 'Bạn vui lòng nhập thông tin cần đặt hàng. Xin cám ơn !';
				    		} else if (list.inforamtionAddition != '' && list.amount != '') {
				    			$scope.msgQuantity = '';
				    			$scope.msgInfo = '';
				        		$(".show-error").hide();
				        		
				        		list.nameService = $scope.ten;
				        		list.idService = $scope.object.id;
				        		$scope.listTmp = Scopes.get('ShoppingCtrl').listGioHang;
				        		
				        		var flag = true;
				        		if ($scope.listTmp.length > 0) {
					        		for (var i = 0; i < $scope.listTmp.length; i++) {
					        			if (list.idService == $scope.listTmp[i].idService) {
					        				Scopes.get('ShoppingCtrl').listGioHang[i].amount = Scopes.get('ShoppingCtrl').listGioHang[i].amount + list.amount;
					        				Scopes.get('ShoppingCtrl').listGioHang[i].inforamtionAddition = list.inforamtionAddition;
					        				flag = false;
					        				$cookieStore.put('sc', Scopes.get('ShoppingCtrl').listGioHang);
					        				break;
					        			}
					        		}
					        		if (flag) {
					        			Scopes.get('ShoppingCtrl').listGioHang.push(list);
					        			$cookieStore.put('sc', Scopes.get('ShoppingCtrl').listGioHang);
					        		}
				        		} else {
				        			Scopes.get('ShoppingCtrl').listGioHang.push(list);
				        			$cookieStore.put('sc', Scopes.get('ShoppingCtrl').listGioHang);
				        		}
	//			        		Scopes.get('MenuCtrl').quantityShoppingCart = $cookieStore.get('sc').length;
				        		
				        		$("#notifyQuantity").text($cookieStore.get('sc').length);
				        		ngDialog.close();
				        	}
				        	
				        };
				    	
				    }]
				});
        	}
        }
                
        /* functions */
        function getServiceDetail() {
        	self.loading = true;
            var serviceDetailApi = "services/" + CONSTANT.lang + "/field/" + self.serviceId + "/detail";
            homeService.getData(serviceDetailApi).then(function(res) {
                self.detail = res.data.info;
                // create meta header
                homeService.checkMetaState(
                        {"titlePage" : self.detail.ten, 
                         "metaKeywords" : self.detail.ten, 
                         "metaDescription" : self.detail.moTa, 
                         "thumbNailUrl" : self.detail.anhDaiDien.urlImage,
                         "siteUrl" : $location.$$absUrl});
                setTimeout(function() {
    	 			$rootScope.htmlReady();
    	 		}, 100);

    	 		self.loading = false;
           });
        }
        
        function getServiceField() {
        	 var serviceFieldApi = "services/" + CONSTANT.lang + "/" + self.fieldId + "/detail";
        	 homeService.getData(serviceFieldApi).then(function(res) {
             self.serviceField = res.data.info;
        	 });
        }
        
        function getServiceDeals() {
            var serviceDealsApi = "services/" + CONSTANT.lang + "/field/" + self.serviceId + "/article";
            homeService.getData(serviceDealsApi).then(function(res) {
                self.serviceDeals = res.data.items;
            });
        }
        
        function getServiceRelated() {
            var serviceDealsApi = "services/" + CONSTANT.lang + "/" + self.fieldId + "/field/" + self.serviceId + "/list";
            homeService.getData(serviceDealsApi).then(function(res) {
                self.serviceRelated = res.data.items;
            });
            setTimeout(function () {
            	$('.wd-place').css('height',maxH($('.wd-place')));
            }, 1000);
            
            
        }
        
        function getServiceAttrs() {
            var serviceAttrsApi = "services/" + CONSTANT.lang + "/field/" + self.serviceId + "/attribute";
            homeService.getData(serviceAttrsApi).then(function(res) {
                self.serviceAttrs = res.data.items;
            });
        }

		function getListComment(page) {
			var serviceCmtsApi = "comment/service/" + self.serviceId + "/list";
//			console.log("page: "+page);
			homeService.getDataByGetMethod(serviceCmtsApi, {'page':page}).then(function(res) {
				self.listComment = res.data.data.items[0];
				$scope.totalComment = res.data.data.items[2];
			});
		}
        
		function showHideCommentBox(comment_box_id){
			if ($('#'+comment_box_id).is(":visible")) {
				//visible do some than
				$('#'+comment_box_id).hide('slow');
			} else {
				//not visible do some else
				$('#'+comment_box_id).show('slow');
			}
		}
		
		function isBlank(str){
			return !str || !/[^\s]+/.test(str);
		}
		
		function focusTextbox() {
			$("#txtParentCommentArea").focus();
		}
		
		function addReplyOfComment(comment){
			if (homeService.isLoggedIn()) {
				var username = homeService.getCurrentLoginUser().id;
				if(self.isBlank(comment.reply)) {
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan, 2500);
				}else{
					var param = 'username='+username+'&content='+comment.reply;
					var path = 'comment/service/'+ self.serviceId + '/reply/' +comment.id+ '/add';
					activeService.addComment(path, param)
					.success(function(data, status, headers, config) {
						if(data.meta.code == 200){
							comment.reply = '';
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong, 2500);
						}else{
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
						}
					})
					.error(function(data, status){
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
					});
				}
			} else {
				homeService.requriedLogin();
			}
		}
		
		function addComment(){
			if (homeService.isLoggedIn()) {
				var username = homeService.getCurrentLoginUser().id;
				if(self.isBlank(self.commentContent)) {
					homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).yeuCauNhapNoiDungBinhLuan, 2500);
				} else{
					var param = 'username='+username+'&content='+self.commentContent;
					var path = 'comment/service/'+ self.serviceId + '/add';
					activeService.addComment(path, param)
					.success(function(data, status, headers, config) {
						if(data.meta.code == 200){
							self.commentContent = '';
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).guiBinhLuanThanhCong, 2500);
						}else{
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
						}
					})
					.error(function(data, status){
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
					});
				}
			} else {
				homeService.requriedLogin();
			}
		}
		
		function createServiceRating() {
			$('#wd-rating').raty({
				number:     10,
				hints: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10'],
				target     : '#hint',
				targetType : 'score',
				targetKeep : true,
				starOn:    'imagesnocode/star-on.png',
				starOff:   'imagesnocode/star-off.png',
				click: function(score, evt) {
					saveScoreRating(score);
				}
			});
		}
		
		function getScoreRating(){
			var userId = '';
			if(homeService.isLoggedIn()) {
				userId = homeService.getCurrentLoginUser().id;
				var scoreRatingApi = "rating/get_score_rating/"+ self.serviceId +"/"+ userId;
				homeService.getData(scoreRatingApi).then(function(res) {
					if (res.meta.code == 200) {
						var scoreRating = res.data.info.score;
						$('#wd-rating').raty('set', { score: scoreRating });
						setMark();
					}
	            });
			}
		}
		
		function saveScoreRating(scoreRating){
			if(homeService.isLoggedIn()) {
				var userId = homeService.getCurrentLoginUser().id;
				if(userId !='') {
					var saveRatingApi = "rating/save_score_rating";
					var param = 'idService='+ self.serviceId +'&idUser='+userId+'&score='+scoreRating;
					
		            homeService.getDataByPostMethod(saveRatingApi, param).then(function(res) {
		                if (res.data.meta.code == 200) {
							var averageRating = res.data.data.info.average;
							if(averageRating < 10 && averageRating.toString().indexOf('.') == -1){
								averageRating = averageRating + ".0";
							}
							$("#scoreRating").text(averageRating);
							setMark();
						}
		            });
				}
			} else {
				homeService.requriedLogin();
			}
		}

		function setMark() {
			var sortMark = $("#scoreRating").text();
			if (sortMark < 5) {
				$("#assessment").text("Trung bình");
			} else if (sortMark >= 5 && sortMark < 8) {
				$("#assessment").text("Tốt");
			} else if (sortMark >= 8 && sortMark <= 9) {
				$("#assessment").text("Rất Tốt");
			} else {
				$("#assessment").text("Tuyệt vời");
			}
		}
		
		function getCountInterest(){
			var userId = '';
			if(homeService.isLoggedIn()) {
				userId = homeService.getCurrentLoginUser().id;
			}
			var countInterestApi = "comment/service/" + self.serviceId + "/interest/count";
			var param = 'username='+ userId;
            homeService.getDataByPostMethod(countInterestApi, param).then(function(res) {
                if (res.data.meta.code == 200) {
                	self.interest = res.data.data.info;
				}
            });
		}
		
		function clickInterest() {
			if(homeService.isLoggedIn()) {
				var userId = homeService.getCurrentLoginUser().id;
				var addInterestApi = "comment/service/" + self.serviceId + "/interest/add";
				var param;
//				var check='';
				var cookie = ipCookie('interactions_service'+self.serviceId+'.nguoidan_dukhach');
				if (cookie!=undefined && cookie==2){
					check='checked';
				}
				/*if(userId!='' || check==''){
					if(userId==''){
						if(cookie==1)
						{
							ipCookie('interactions_service'+self.serviceId+'.nguoidan_dukhach', '2',{ expires: 1, path: '/' });
							userId='del';
						}
						else{
							ipCookie('interactions_service'+self.serviceId+'.nguoidan_dukhach', '1',{ expires: 1, path: '/' });
							userId='unlogin';
						}
					}*/
					
					param = 'username='+ userId;
					activeService.addInterest(addInterestApi, param).success(function(data, status, headers, config) {
						if(data.meta.code == 200){
							self.interest = data.data.info;
						}else{
							homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).xayRaLoiTrongKhiGui, 2500);
						}
					})
					.error(function(data, status){
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).loiKetNoi, 2500);
					});
				/*} else {
					if(check !='') {
						homeService.showNotification(homeService.updateLanguageByCode(CONSTANT.lang).gioHanQuanTam, 2500);
					}
				}*/
			} else {
				homeService.requriedLogin();
			}
		}
		
		// check if login is required
		function checkLoginRequired($event){
			if(!homeService.isLoggedIn()){
				$event.target.blur();
				homeService.requriedLogin();
			}
		}
		
		function maxH (obj){
			var heights = obj.map(function() {
				return $(this).outerHeight();
			}).get();

			var mh = $window.Math.max.apply(null, heights);
//			console.log('max height: '+mh);
			return mh;
		}

		function getBookingType(type) {
			var text = "";
			if('dat_phong' == type) {
				text = "Đặt phòng";
			} else if('dat_ban' == type) {
				text = "Đặt bàn";
			} else {
				text = "Đặt vé";
			}
			return text;
		}
    };
    
})();