/**
 * @ngdoc function
 * @name nddkApp.controller:HomeCtrl
 * @description
 * # HomeCtrl
 * Controller of the nddkApp
 */

;(function() {
	'use strict';
	
	angular
	.module('nddkApp')
	.controller('shoppingCart', shoppingCart);
	
	shoppingCart.$inject = [ '$scope', 'homeService', 'Scopes', '$cookieStore', 'CONSTANT' ];
	
	function shoppingCart($scope, homeService, Scopes, $cookieStore, CONSTANT){
		
		CONSTANT.lang = homeService.getLanguage();

		Scopes.store('shoppingCart', $scope);
		
		$scope.tmp = 0;
		
		homeService.getData('articles/' + CONSTANT.lang + '/promotion').then(function(res) {
			$scope.promotions = res.data.items;
		});
		

		$scope.clickDatHang = function() {
			$('.wd-block-information').slideDown(500);
		};
		
		$scope.clickClose = function() {
			$('.wd-block-information').slideUp(500);
		};
		
		
		
	}
})();



