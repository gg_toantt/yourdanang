;
(function() {
	'use strict';

	angular.module('nddkApp').controller('MenuMainCtrl', MenuMainCtrl);

	MenuMainCtrl.$inject = [ '$rootScope', 'homeService', 'CONSTANT' ];

	function MenuMainCtrl($rootScope, homeService, CONSTANT) {

		var self = this;

		$rootScope.menuList = [];
		$rootScope.menuMainTrangChu = '';
		$rootScope.menuMainVeDaNang = '';
		$rootScope.menuMainTinNong = '';
		$rootScope.menuMainSanPhamDichVu = '';
		$rootScope.menuMainMuaSam = '';
		$rootScope.menuMainChiaSeCamNhan = '';
		$rootScope.menuMainBanDo = '';

		homeService.getData('menu/' + CONSTANT.lang).then(function(res) {
			if ($rootScope.menuList.length <= 0) {
				if(res.meta.code == 200 && res.data.items != null) {
					$rootScope.menuList = res.data.items;

					angular.forEach($rootScope.menuList, function(value, index) {
						if (value.alias == 'trang-chu') {
							$rootScope.menuMainTrangChu = value.tenMenu;
						} else if (value.alias == 've-da-nang') {
							$rootScope.menuMainVeDaNang = value.tenMenu;
						} else if (value.alias == 'tin-nong') {
							$rootScope.menuMainTinNong = value.tenMenu;
						} else if (value.alias == 'san-pham-dich-vu') {
							$rootScope.menuMainSanPhamDichVu = value.tenMenu;
						} else if (value.alias == 'mua-sam') {
							$rootScope.menuMainMuaSam = value.tenMenu;
						} else if (value.alias == 'chia-se-cam-nhan') {
							$rootScope.menuMainChiaSeCamNhan = value.tenMenu;
						} else if (value.alias == 'ban-do') {
							$rootScope.menuMainBanDo = value.tenMenu;
						}
					})
					
				}
			}
		});
	}

})();