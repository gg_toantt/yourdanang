;
(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:HomeCtrl
	 * @description
	 * # HomeCtrl
	 * Controller of the nddkApp
	 */

	angular.module('nddkApp').controller('MenuCtrl', MenuCtrl);

	MenuCtrl.$inject = [ '$scope', 'homeService', 'Scopes', '$cookieStore', 'CONSTANT', '$timeout', 'ipCookie', "$rootScope" ];

	function MenuCtrl($scope, homeService, Scopes, $cookieStore, CONSTANT, $timeout, ipCookie, $rootScope) {
		CONSTANT.lang = homeService.getLanguage();

		var self = this;
		Scopes.store('MenuCtrl', $scope);
		// public properties

		// public methods
		self.getDichVuHot = getDichVuHot;
		self.changeLanguage = changeLanguage;
		self.clickMenuCurrent = clickMenuCurrent;
		self.closeSidebar = closeSidebar;

		/* Scope */
		if (!angular.isUndefined($cookieStore.get('sc'))) {
        	$scope.quantityShoppingCart = $cookieStore.get('sc').length;
		} else {
			$scope.quantityShoppingCart = 0;
		}
		$scope.menuCtrl = self;
		$scope.menuVeDaNang = [];
		$scope.menuSanPhamDichVu = [];
		$scope.slideImageSanPhamDichVu = [];
		$scope.tinMoiKhuyenMai = [];
		$scope.muaSamDatCho = [];
		$scope.dichVuHot = [];

		/* init */
		getMenuVeDaNang();
		getMenuSanPhamDichVu();
		//anvt: comment 28/1/16: getSlideImageSanPhamDichVu();
		// getDichVuHot(1);
		$rootScope.metaservice = homeService.getMetaService();
		/* implements method */

		setTimeout(function() {
			if ($('.container').outerWidth() >= 768 && $('.container').outerWidth() < 1024) {
				$(window).load(function () {

		    		var posOne = $('#posOne').outerHeight();
			    	var posTwo = $('#posTwo').outerHeight();

			    	if (posOne > posTwo) {
			    		$('#posTwo').outerHeight(posOne);
			    	} else {
			    		$('#posOne').outerHeight(posTwo);
			    	}

			    	setTimeout(function() {
			    		var posThree = $('#posThree').outerHeight();
				    	var posFour = $('#posFour').outerHeight();
				    	if (posThree > posFour) {
				    		$('#posFour').outerHeight(posThree);
				    	} else {
				    		$('#posThree').outerHeight(posFour);
				    	}
			    	}, 200);
				});
			}
    	}, 500);

		function clickMenuCurrent(mode) {

			if (mode == 'home') {
				$('#homeMenu').addClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').removeClass('current');

		    	setTimeout(function() {
			    	if ($('.container').outerWidth() >= 770) {
			    		var posOne = $('#posOne').outerHeight();
				    	var posTwo = $('#posTwo').outerHeight();

				    	if (posOne > posTwo) {
				    		$('#posTwo').outerHeight(posOne);
				    	} else {
				    		$('#posOne').outerHeight(posTwo);
				    	}

				    	setTimeout(function() {
					    	var posThree = $('#posThree').outerHeight();
					    	var posFour = $('#posFour').outerHeight();
					    	if (posThree > posFour) {
					    		$('#posFour').outerHeight(posThree);
					    	} else {
					    		$('#posThree').outerHeight(posFour);
					    	}
				    	}, 800);
		    		}
		    	}, 1000);

			} else if (mode == 'veDaNang') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').addClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').removeClass('current');
			} else if (mode == 'dichVu') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').addClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').removeClass('current');
			} else if (mode == 'tinTuc') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').addClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').removeClass('current');
			} else if (mode == 'muaSam') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').addClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').removeClass('current');
			} else if (mode == 'chiaSeCamNhan') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').addClass('current');
		    	$('#banDoMenu').removeClass('current');
			} else if (mode == 'banDo') {
				$('#homeMenu').removeClass('current');
				$('#veDaNangMenu').removeClass('current');
		    	$('#dichVuMenu').removeClass('current');
		    	$('#tinTucMenu').removeClass('current');
		    	$('#muaSamMenu').removeClass('current');
		    	$('#chiaSeCamNhanMenu').removeClass('current');
		    	$('#banDoMenu').addClass('current');

			}
		}

		function changeLanguage(mode) {
			if (mode == 'vn') {
				ipCookie('lg', 'vn');
			} else {
				ipCookie('lg', 'en');
			}
			location.reload();
		}

		function getMenuVeDaNang() {
			homeService.getData('articles/' + CONSTANT.lang + '/topic/introduction').then(function(res) {
				$scope.menuVeDaNang = res.data.items;
			});
		}

		function getMenuSanPhamDichVu() {
			homeService.getData('services/' + CONSTANT.lang + '/field/home').then(function(res) {
				$scope.menuSanPhamDichVu = res.data.items;
			});
		}

		function getSlideImageSanPhamDichVu() {
			homeService.getData('services/' + CONSTANT.lang + '/12_attractive_destinations').then(function(res) {
				$scope.slideImageSanPhamDichVu = res.data.items;
			});
		}

		function getDichVuHot(idService) {
			// var listDichVuHotApi = 'services/' + CONSTANT.lang + '/15_highlight_services_type/' + idService;
			// homeService.getData(listDichVuHotApi).then(function(res) {
			// 	$scope.dichVuHot = res.data.items;
			// 	angular.element('#contHotServices').empty();
			// 	angular.element('#contHotServices').append(get5DichVu(res.data.items));

			// 	angular.element('.wd-list-news-stmu').carouFredSel({
			// 	    responsive : true,
			// 	    auto : false,
			// 	    width : '100%',
			// 	    prev : '#wd-prev-5',
			// 	    next : '#wd-next-5',
			// 	    pagination : false,
			// 	    scroll : {
			// 	        items : 1,
			// 	        pauseOnHover : true
			// 	    },
			// 	    items : {
			// 	        width : 165,
			// 	        height : 'auto',
			// 	        visible : {
			// 	            min : 1,
			// 	            max : 3
			// 	        }
			// 	    }
			// 	});
			// });
		}

		function get5DichVu(items) {
			var dvLength = items.length;
			var result = '';
			for (var i = 0; i < dvLength; i++) {
				result = result + getDichVuDetail(items[i].idLinhVuc, items[i].url, items[i].anhDaiDien.urlImage, items[i].ten, items[i].diaChi);
			}
			return result;
		}

		function getDichVuDetail(idLinhVuc, url, anhDaiDien, ten, diaChi) {
			var detail = '<li class="col-xs-4 wd-item-mn"><div class="wd-thumbnail"><a href="#/dich-vu/' + idLinhVuc + '/' + url + '" class="process-img"><img src="' + anhDaiDien
			    + '" alt="" style="width:218px; height:142px;"/>' + '</a><div class="caption"><h3><a href="#/dich-vu/' + idLinhVuc + '/' + url + '">' + ten + '</a></h3><p>'
			    + diaChi + '</p></div></div></li>';
			return detail;
		}

		/* margin center dialog when resize screen */
		$(window).resize(
			function() {
				homeService.locateDialog();
			}
		);

		/* Menu mobile */
		var snapper = new Snap({
	        element: document.getElementById('page-layout'),
	        disable: 'right',
	        tapToClose: true
	    });


		if ($(window).width() <= 768) {
	        snapper.enable();
	    } else {
	        snapper.close();
	        snapper.disable();
	    }

	    function snapperToggler() {
	    	if ($(window).width() <= 768) {
		        snapper.enable();
		    } else {
		        snapper.close();
		        snapper.disable();
		    }
		}

		$(window).resize(function() {
		    snapperToggler();
		    if ($(window).width() <= 768) {
		        snapper.enable();
		    } else {
		        snapper.close();
		        snapper.disable();
		    }
		});

		snapper.disable();

		$("#btClickLeftNav").click(function() {
			if( snapper.state().state=="left" ){
	        	snapper.close();
		    } else {
		        snapper.open('left');
		    }
		});
		
		$("body").click(function() {
			if( snapper.state().state=="left" ) {
	        	snapper.close();
		    }
		});

		function closeSidebar() {
			snapper.close();
		}

	}



	function menuInit() {
		responsiveClasses();
		// pageTransition();
		dataResponsiveClasses();
		windowscroll();
	}

	function windowscroll(){
		var headerWrapOffset = 0;
		if( $header.length > 0 ) { headerWrapOffset = $headerWrap.offset().top; }
		$window.on( 'scroll', function(){
			$('body.open-header.close-header-on-scroll').removeClass("side-header-open");
			stickyMenu(headerWrapOffset);
		});
	}

	function stickyMenu(headerOffset){
		//console.log("$window.scrollTop(): " + $window.scrollTop());
		//console.log("headerOffset: " + headerOffset);

		if ($window.scrollTop() > 50) {
			//console.log("add");
			if( $body2.hasClass('device-lg') || $body2.hasClass('device-md') ) {
				//$pagemenu.hide(2000);
				$('body:not(.side-header) #wd-menu-desktop:not(.no-sticky)').addClass('sticky-header');
				$('#page-menu:not(.dots-menu,.no-sticky)').addClass('sticky-page-menu');
			}
		} else {
			//console.log("remove");
			removeStickyness();
		}
	}

	function removeStickyness(){
			//$pagemenu.show(2000);
			$('body:not(.side-header) #wd-menu-desktop:not(.no-sticky)').removeClass('sticky-header');
			$header.removeClass().addClass(oldHeaderClasses);
			$headerWrap.removeClass().addClass(oldHeaderWrapClasses);

	}

	function responsiveClasses(){
		var jRes = jRespond([
			{
				label: 'laptop',
				enter: 992,
				exit: 1199
			},{
				label: 'desktop',
				enter: 1200,
				exit: 10000
			}
		]);
		jRes.addFunc([
			{
				breakpoint: 'desktop',
				enter: function() { $body2.addClass('device-lg'); },
				exit: function() { $body2.removeClass('device-lg'); }
			},{
				breakpoint: 'laptop',
				enter: function() { $body2.addClass('device-md'); },
				exit: function() { $body2.removeClass('device-md'); }
			}
		]);
	}

	function pageTransition(){
		if( !$body2.hasClass('no-transition') ){
			var animationIn = $body2.attr('data-animation-in'),
				animationOut = $body2.attr('data-animation-out'),
				durationIn = $body2.attr('data-speed-in'),
				durationOut = $body2.attr('data-speed-out'),
				loaderTimeOut = $body2.attr('data-loader-timeout'),
				loaderStyleHtml = $body2.attr('data-loader-html');

			if( !animationIn ) { animationIn = 'fadeIn'; }
			if( !animationOut ) { animationOut = 'fadeOut'; }
			if( !durationIn ) { durationIn = 1500; }
			if( !durationOut ) { durationOut = 800; }
			if( !loaderStyleHtml ) { loaderStyleHtml = '<div class="css3-spinner-bounce1"></div><div class="css3-spinner-bounce2"></div><div class="css3-spinner-bounce3"></div>'; }

			if( !loaderTimeOut ) {
				loaderTimeOut = false;
			} else {
				loaderTimeOut = Number(loaderTimeOut);
			}

			$wrapper.animsition({
				inClass : animationIn,
				outClass : animationOut,
				inDuration : Number(durationIn),
				outDuration : Number(durationOut),
				linkElement : '#primary-menu ul li a:not([target="_blank"]):not([href*=#]):not([data-lightbox])',
				loading : true,
				loadingParentElement : 'body',
				loadingClass : 'css3-spinner',
				loadingHtml : loaderStyleHtml,
				unSupportCss : [
								 'animation-duration',
								 '-webkit-animation-duration',
								 '-o-animation-duration'
							   ],
				overlay : false,
				overlayClass : 'animsition-overlay-slide',
				overlayParentElement : 'body',
				timeOut: loaderTimeOut
			});
		}
	}

	function dataResponsiveClasses(){
		var $dataClassMd = $('[data-class-md]'),
			$dataClassLg = $('[data-class-lg]');

		if( $dataClassMd.length > 0 ) {
			$dataClassMd.each( function(){
				var element = $(this),
					elementClass = element.attr('data-class-md'),
					elementClassDelete = element.attr('data-class-xxs') + ' ' + element.attr('data-class-xs') + ' ' + element.attr('data-class-sm') + ' ' + element.attr('data-class-lg');

				if( $body2.hasClass('device-md') ) {
					element.removeClass( elementClassDelete );
					element.addClass( elementClass );
				}
			});
		}

		if( $dataClassLg.length > 0 ) {
			$dataClassLg.each( function(){
				var element = $(this),
					elementClass = element.attr('data-class-lg'),
					elementClassDelete = element.attr('data-class-xxs') + ' ' + element.attr('data-class-xs') + ' ' + element.attr('data-class-sm') + ' ' + element.attr('data-class-md');

				if( $body2.hasClass('device-lg') ) {
					element.removeClass( elementClassDelete );
					element.addClass( elementClass );
				}
			});
		}
	}

	function topScrollOffset() {
		var topOffsetScroll = 0;

		if( ( $body2.hasClass('device-lg') || $body2.hasClass('device-md') )) {
			if( $header.hasClass('sticky-header') ) {
				if( $pagemenu.hasClass('dots-menu') ) { topOffsetScroll = 100; } else { topOffsetScroll = 144; }
			} else {
				if( $pagemenu.hasClass('dots-menu') ) { topOffsetScroll = 140; } else { topOffsetScroll = 184; }
			}

			if( !$pagemenu.length ) {
				if( $header.hasClass('sticky-header') ) { topOffsetScroll = 100; } else { topOffsetScroll = 140; }
			}
		} else {
			topOffsetScroll = 40;
		}

		return topOffsetScroll;
	}

	var $window = $(window),
	$body2 = $('body'),
	$wrapper = $('#wd-wrapper'),
	$header = $('#wd-menu-desktop'),
	$headerWrap = $('#container-dropdown'),
	oldHeaderClasses = $header.attr('class'),
	oldHeaderWrapClasses = $headerWrap.attr('class'),
	$pagemenu = $('#wd-head-container');

	$(document).ready(menuInit());

})();
