;(function(){
	'use strict';
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:CheckLoginCtrl
	 * @description # CheckLoginCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp')
	.controller('CheckLoginCtrl', checkLoginCtrl);
	checkLoginCtrl.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT', '$state' ];
	
	function checkLoginCtrl($scope, $timeout, homeService, CONSTANT, $state) {
		if (!homeService.isLoggedIn()) {
			$state.go('home', {});
		}
	}
})();