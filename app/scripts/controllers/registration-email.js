;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('registrationEmailCtrl', registrationEmailCtrl);

    registrationEmailCtrl.$inject =[ '$scope', '$timeout', 'homeService', 'CONSTANT', '$stateParams', '$rootScope' ];
    function registrationEmailCtrl($scope, $timeout, homeService, CONSTANT,  $stateParams, $rootScope) {
        
        CONSTANT.lang = homeService.getLanguage();
        var self = this;

        // public properties
        self.showMessageSuccess = '';
        self.showMessageError = '';

        // public methods
        self.regEmail = regEmail;

        /* Scope */
        $scope.fullName = "";
        $scope.email = "";
        $scope.submitted = false; // form validated when submit button clicked
        $scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
        $scope.message = "";

        // implements
        function regEmail() {
            $scope.submitted = true;
            
            if ($scope.myForm.$valid) {
                var userRegistration = "fullName=" + $scope.fullName + "&email=" + $scope.email;
                homeService.registerEmail('registerEmail/addMail', userRegistration).success(function(res) {
                    if (res.meta.code == 200) {
                        self.showMessageSuccess = 'Bạn đã đăng ký email thành công hệ thống!';
                        self.showMessageError = '';
                        $timeout(function() {
                            self.showMessageSuccess = '';
                        }, 3000);
                    } else {
                        self.showMessageError = 'Email này đã đăng ký nhận thông tin khuyến mãi!';
                        self.showMessageSuccess = '';
                        $timeout(function() {
                            self.showMessageError = '';
                        }, 3000);
                    }
                });
            }
        }

    };
})();
