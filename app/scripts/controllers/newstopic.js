;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('NewsTopicCtrl', newsTopicCtrl);

    newsTopicCtrl.$inject = [ '$scope', '$stateParams', 'newsService', 'CONSTANT', 'homeService', '$rootScope', '$location'];

    function newsTopicCtrl($scope, $stateParams, newsService, CONSTANT, homeService, $rootScope, $location) {
        $('#datetimepicker1').datetimepicker({
            // format: 'DD-MM-YYYY',
            locale: 'en',
              // useCurrent: true,
              // useStrict: true,
              format: 'DD-MM-YYYY'
        });
        $('#datetimepicker2').datetimepicker({
            // format: 'DD-MM-YYYY',
            locale: 'en',
             // useCurrent: true,
             //  useStrict: true,
              format: 'DD-MM-YYYY'
        });
		CONSTANT.lang = homeService.getLanguage();

        var self = this;
        // public properties
        self.topic = $stateParams.catId;
        self.page = 1;
        self.nameTopic = '';
        self.fromDay = '';
        self.toDay = '';
        self.tenSuKien = '';
        // public methods
        self.getListNewsTopic = getListNewsTopic;

        /* Scope */
        $scope.newsTpCtrl = self;
        $scope.newsTopics = [];
        $scope.everyPage = 10;
        $scope.total = 0;
        $scope.sortTypes = [{id:'name_asc', name: 'Tên giảm dần'}, {id:'name_desc', name: 'Tên tăng dần'}, {id:'date_asc', name: 'Ngày giảm dần'}, {id:'date_desc', name: 'Ngày giảm dần'} ];
        $scope.selectedSortType = $scope.sortTypes[3];

        $scope.sortSelectChange = function(sort) {
            $scope.selectedSortType = sort;
            self.page = 1;
            getListNewsTopic(self.page);
        };

        /* init */
        getListNewsTopic(self.page);
        /* implements method */
        function getListNewsTopic(numberPage) {
        	// console.log('Filter');
        	switch (self.topic) {
  		    	case 'khuyen-mai':
  		    		self.nameTopic = 'Tin khuyến mãi';
  		    		$scope.linkTopic = 'allpromotion';
  		    		break;
  		    	case 'su-kien':
  		    		self.nameTopic = 'Sự kiện';
  		    		$scope.linkTopic = 'allevent';
  		    		break;
  		    	case 'tong-hop':
  		    		self.nameTopic = 'Tin tổng hợp';
  		    		$scope.linkTopic = 'allgeneral';
  		    		break;
  		    	default :
  		    		$scope.linkTopic = 'acticlesOther';
              if (self.topic == 'tin-thanh-pho') {
                self.nameTopic = 'Tin thành phố';
              } else if (self.topic == 'du-lich') {
                self.nameTopic = 'Du lịch';
              }
	    	  }

            var listNewsTopicApi = '';
            var from = '';
            var to = '';
            switch (self.topic) {
              case 'khuyen-mai':
                self.fromDay = $('#fromDate').val();
                self.toDay = $('#toDate').val();
                listNewsTopicApi = 'articles/' + CONSTANT.lang + '/' + $scope.linkTopic + "?page=" + numberPage
                    + "&ngayBatDauKM=" + self.fromDay + "&ngayKetThucKM=" + self.toDay;
                break;
              case 'su-kien':
                self.fromDay = $('#fromDate').val();
                self.toDay = $('#toDate').val();
                listNewsTopicApi = 'articles/' + CONSTANT.lang + '/' + $scope.linkTopic + "?page="+ numberPage
                    + '&sort='+$scope.selectedSortType.id + "&fromDate=" + self.fromDay + "&toDate=" + self.toDay+"&tenSuKien="+self.tenSuKien;
                break;
              case 'tong-hop':
                self.fromDay = $('#fromDate').val();
                self.toDay = $('#toDate').val();
                listNewsTopicApi = 'articles/' + CONSTANT.lang + '/' + $scope.linkTopic + "?page="+ numberPage
                    + '&sort='+$scope.selectedSortType.id + "&fromDate=" + self.fromDay + "&toDate=" + self.toDay;
                break;
              default :
              listNewsTopicApi = 'articles/' + CONSTANT.lang + '/' + $scope.linkTopic + "?path="+ self.topic + "&page="+ numberPage
                    + '&sort='+$scope.selectedSortType.id + "&fromDate=" + self.fromDay + "&toDate=" + self.toDay;
            }

            newsService.getData(listNewsTopicApi).then(function(res) {
                if (res.meta.code == 200) {
                	self.page = numberPage;
                	
                    $scope.newsTopics = res.data.items.data;
                  
        //                console.log($scope.newsTopics);
                    $scope.total = res.data.items.count;
                  
                    // create meta header
                    var metaJson = {};
                    if(!angular.isUndefined($scope.newsTopics[0])){
                        metaJson = {"titlePage" : $scope.newsTopics[0].chuDe.ten,
                            "metaKeywords" : $scope.newsTopics[0].chuDe.ten,
                            "metaDescription" : $scope.newsTopics[0].chuDe.moTa,
                            "thumbNailUrl" : "http://backend.yourdanang.vn/mis/dichvu/logo-congthongtin-danang_1434697214242.jpg",
                            "siteUrl" : $location.$$absUrl};

                    }else{
                        metaJson = {"titlePage" : self.nameTopic,
                                "metaKeywords" : "",
                                "metaDescription" : "",
                                "thumbNailUrl" : "http://backend.yourdanang.vn/mis/dichvu/logo-congthongtin-danang_1434697214242.jpg",
                                "siteUrl" : $location.$$absUrl};
                    }
                    homeService.checkMetaState(metaJson);
                }
                setTimeout(function() {
         			$rootScope.htmlReady();
        //    	 			console.log('new Topic');
         		}, 100);
            });

        }
    }
    ;
})();
