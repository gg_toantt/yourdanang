;(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:HomeCtrl
	 * @description
	 * # HomeCtrl
	 * Controller of the nddkApp
	 */
	 angular.module('nddkApp')
	 .controller('registrationSuccessCtrl', registrationSuccessCtrl);
	 
	 registrationSuccessCtrl.$inject = ['$scope','$stateParams', 'homeService'];

	 function registrationSuccessCtrl($scope, $stateParams, homeService) {

		// public properties
		$scope.registedEmail = $stateParams.registedEmail;
		$scope.isBusinessParam = $stateParams.business;
		
		$scope.code = $stateParams.code;
		
		$scope.msg = '';
		
		homeService.getDataByPostMethod('registrations/validate_email', 'code=' + $scope.code)
			.success(function(data, status, headers, config) {
				if (data.meta.code == 200){
					$scope.msg = "Chúc mừng bạn đã trở thành thành viên";
				} else{
					$scope.msg = data.meta.message;
				}
			})
			.error(function(data, status){
//				console.log('Xảy ra lỗi trong khi gửi. Vui lòng thử lại');
			});
		
	 };

 })();