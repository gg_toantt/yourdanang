;(function(){
	'use strict';
	
	/**
	 * @ngdoc function
	 * @name nddkApp.controller:LanguageCtrl
	 * @description # LanguageCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp')
	.controller('LanguageCtrl', languageController);
	languageController.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT' ];
	
	function languageController($scope, $timeout, homeService, CONSTANT) {
		CONSTANT.lang = homeService.getLanguage();
		$scope.language = homeService.updateLanguageByCode(CONSTANT.lang);
	}
})();