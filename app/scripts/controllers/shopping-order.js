;
(function() {
	'use strict';

	/**
	 * @ngdoc function
	 * @name nddkApp.controller:HomeCtrl
	 * @description # HomeCtrl Controller of the nddkApp
	 */
	angular.module('nddkApp').controller('shoppingOrder', shoppingOrder);

	shoppingOrder.$inject = [ '$scope', '$timeout', 'homeService', 'CONSTANT',
			'$stateParams', 'ngDialog', 'Scopes', '$cookieStore',
			'registrationService', '$state', '$rootScope', 'ipCookie' ];

	function shoppingOrder($scope, $timeout, homeService, CONSTANT,
			$stateParams, ngDialog, Scopes, $cookieStore, registrationService,
			$state, $rootScope, ipCookie) {
		CONSTANT.lang = homeService.getLanguage();
		var self = this;

		Scopes.store('ShoppingCtrl', $scope);

		// public properties
		self.widthDevice = $('body').outerWidth();
		self.fieldId = '0';
		self.keyword = '';
		self.page = 1;
		self.thuoctinh = '1';
		self.sortType = "rating_";
		self.sortOptional = 'desc';
		self.sort = self.sortType + self.sortOptional;
		self.selectedService = {};
		self.listField = [];
		self.gridLayout = true;
		self.nameSelected = 'Tất cả';
		self.show;
		self.showView = true;

		// public methods
		self.getListService = getListService;
		self.getSelectedService = getSelectedService;
		self.changeLayOut = changeLayOut;
		self.getNumber = getNumber;
		self.changeSort = changeSort;
		self.selectLinhVuc = selectLinhVuc;
		self.clickDatCho = clickDatCho;
		self.search = search;
		self.chinhSua = chinhSua;
		self.xoa = xoa;
		self.save = save;
		self.getBookingType = getBookingType;
		self.hoanThanhOrder = hoanThanhOrder;
		$scope.EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i;
		$scope.PHONE_REGEXP = /^(\()?\d{3}(\))?(-|\s)?\d{3}(-|\s)\d{4}$/;
		self.loading = false;

		/* Scope */
		$scope.serviceCtrl = self;
		$scope.listService = [];
		$scope.everyPage = 12;
		$scope.total = 0;
		$scope.listGioHang = $cookieStore.get('sc');

		$scope.hoTen = '';
		$scope.emailOrder = '';
		$scope.phone = '';
		$scope.address = '';
		$scope.gio = 'gio';
		$scope.phut = 'phut';
		$scope.listId = '';

		if (angular.isDefined($stateParams.ma)) {
			$scope.maDatHang = $stateParams.ma;
		}

		if (!angular.isUndefined($cookieStore.get('sc'))) {
			$scope.quantityShoppingCart = $cookieStore.get('sc').length;
		} else {
			$scope.quantityShoppingCart = 0;
		}

		if (angular.isDefined(ipCookie('user')) && ipCookie('user') != null) {
			$scope.hoTen = ipCookie('user').displayname;
			$scope.emailOrder = ipCookie('user').email;

		} else {
			$scope.hoTen = '';
			$scope.emailOrder = '';

		}

		/* init */
		getListService(self.page);
		getSelectedService();
		getListField();

		if (angular.isUndefined($scope.listGioHang)) {
			$scope.listGioHang = [];
		}

		function hoanThanhOrder() {

			$scope.submittedOrder = true;

			var pattern = new RegExp(
					/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);

			/*
			 * if (pattern.test($scope.emailOrder) == false) {
			 * $scope.msgValidateEmail = 'Email không hợp lệ'; } else
			 */

			var infor = "infor : " + "&fullName=" + $scope.hoTen + "&email="
					+ $scope.emailOrder + "&address=" + $scope.address
					+ "&phone=" + $scope.phone;


			if ($scope.phone == '') {
				$scope.msgValidatePhone = 'Yêu cầu nhập số điện thoại';
			} else if ($scope.hoTen != '' && $scope.emailOrder != ''
					&& $scope.phone != '' && $scope.address != ''
					&& !angular.isUndefined($scope.hoTen)
					&& !angular.isUndefined($scope.emailOrder)
					&& !angular.isUndefined($scope.phone)
					&& !angular.isUndefined($scope.address)) {


				$scope.msgValidateEmail = '';
				if ($scope.gio == 'gio') {
					$scope.gio = '0';
				}

				if ($scope.phut == 'phut') {
					$scope.phut = '00';
				}

				if (!angular.isUndefined(Scopes.get('LoginCtrl').user)
						&& Scopes.get('LoginCtrl').user != null) {
					$scope.idUser = Scopes.get('LoginCtrl').user.id;
				} else {
					$scope.idUser = '';
				}

				// Cập nhật không yêu cầu đăng nhập khi mua sắm đặt hàng
				// if ($scope.idUser != 0) {
				if ($("#notifyQuantity").text() > 0) {

					var infoPostData = "strJson="
							+ angular.toJson($cookieStore.get('sc'))
							+ "&idUser=" + $scope.idUser + "&fullName="
							+ $scope.hoTen + "&email=" + $scope.emailOrder
							+ "&address=" + $scope.address + "&phone="
							+ $scope.phone + "&appropriateTime=" + $scope.gio
							+ ' giờ ' + $scope.phut + ' phút';

					registrationService
							.loginUser('order', infoPostData)
							.success(
									function(data, status, headers, config) {
										if (data.meta.code == 200) {
											console
													.log('go gio hang -> login -> success');
											Scopes.get('ShoppingCtrl').listGioHang
													.splice(0, $cookieStore
															.get('sc').length);
											$cookieStore
													.put(
															'sc',
															Scopes
																	.get('ShoppingCtrl').listGioHang);
											$("#notifyQuantity")
													.text(
															$cookieStore
																	.get('sc').length);
											$state.go('orderSuccess', {
												ma : data.data.info
											});
										} else {
										}
									}).error(function(data, status) {
							});

				} else {
					alert('Vui lòng chọn mặt hàng !');
				}
				// Cập nhật không yêu cầu đăng nhập khi mua sắm đặt hàng
				// } else {
				// homeService.requriedLogin();
				// }

			}
		}

		function chinhSua(index, obj) {
			self.show = index;
			self.showView = false;
			obj.quantityOrder = obj.amount;
			obj.infoOrder = obj.inforamtionAddition;
		}

		function save(index, obj) {
			if (obj.quantityOrder.length == 0) {
				$scope.msgQuantityOrder = 'Bạn vui lòng nhập số lượng !';
				$scope.msgInfoOrder = '';
			} else if (obj.quantityOrder < 1) {
				$scope.msgQuantityOrder = 'Bạn vui lòng nhập số lượng > 0 !';
				$scope.msgInfoOrder = '';
			} else if (obj.infoOrder.length == 0) {
				$scope.msgInfoOrder = 'Bạn vui lòng nhập thông tin cần đặt hàng. Xin cám ơn !';
				$scope.msgQuantityOrder = '';
			} else {
				$scope.msgQuantityOrder = '';
				$scope.msgInfoOrder = '';
				Scopes.get('ShoppingCtrl').listGioHang[index].amount = obj.quantityOrder;
				Scopes.get('ShoppingCtrl').listGioHang[index].inforamtionAddition = obj.infoOrder;
				$cookieStore.put('sc', Scopes.get('ShoppingCtrl').listGioHang);

				self.showView = true;
			}
		}

		function xoa(index) {
			Scopes.get('ShoppingCtrl').listGioHang.splice(index, 1);
			$cookieStore.put('sc', Scopes.get('ShoppingCtrl').listGioHang);
			$("#notifyQuantity").text($cookieStore.get('sc').length);
			if ($("#notifyQuantity").text() == 0) {
				$state.go('home', {});
			}
		}

		function clickDatCho(list) {
			$scope.list = list;
			ngDialog
			.open({
				template : 'templateOrder',
				className : 'ngdialog-theme-default custom-width-order',
				controller : [
				              '$scope',
				              'registrationService',
				              'Scopes',
				              '$cookieStore',
				              function($scope, registrationService, Scopes,
				            		  $cookieStore) {

				            	  $scope.object = Scopes.get('ShoppingCtrl').list;
				            	  $scope.tenLinhVuc = $scope.object.tenLinhVuc;
				            	  $scope.ten = $scope.object.ten;

				            	  // margin center dialog after created
				            	  homeService.locateDialog();

				            	  $scope.addGioHang = function(list) {

				            		  if (angular
				            				  .isUndefined(list.inforamtionAddition)) {
				            			  list.inforamtionAddition = '';
				            		  }

				            		  if (list.amount == null) {
				            			  $scope.msgQuantity = 'Bạn vui lòng nhập số lượng !';
				            			  $(
				            			  ".show-error.show-error-quanlity.mt10")
				            			  .show();
				            			  $(".show-error.show-error-area")
				            			  .hide();
				            		  } else if (list.amount < 1) {
				            			  $scope.msgQuantity = 'Bạn vui lòng nhập số lượng > 0 !';
				            			  $(
				            			  ".show-error.show-error-quanlity.mt10")
				            			  .show();
				            			  $(".show-error.show-error-area")
				            			  .hide();
				            		  } else if (list.inforamtionAddition == ''
				            			  || list.amount == '') {
				            			  $(".show-error.show-error-area")
				            			  .show();
				            			  $(
				            			  ".show-error.show-error-quanlity.mt10")
				            			  .hide();
				            			  $scope.msgInfo = 'Bạn vui lòng nhập thông tin cần đặt hàng. Xin cám ơn !';
				            		  } else if (list.amount != ''
				            			  && list.inforamtionAddition != '') {
				            			  $scope.msgQuantity = '';
				            			  $scope.msgInfo = '';
				            			  $(".show-error").hide();

				            			  list.nameService = $scope.ten;
				            			  list.idService = $scope.object.id;
				            			  $scope.listTmp = Scopes
				            			  .get('ShoppingCtrl').listGioHang;

				            			  var flag = true;
				            			  if ($scope.listTmp.length > 0) {
				            				  for (var i = 0; i < $scope.listTmp.length; i++) {
				            					  if (list.idService == $scope.listTmp[i].idService) {
				            						  Scopes
				            						  .get('ShoppingCtrl').listGioHang[i].amount = Scopes
				            						  .get('ShoppingCtrl').listGioHang[i].amount
				            						  + list.amount;
				            						  Scopes
				            						  .get('ShoppingCtrl').listGioHang[i].inforamtionAddition = list.inforamtionAddition;
				            						  flag = false;
				            						  $cookieStore
				            						  .put(
				            								  'sc',
				            								  Scopes
				            								  .get('ShoppingCtrl').listGioHang);
				            						  break;
				            					  }
				            				  }
				            				  if (flag) {
				            					  Scopes.get('ShoppingCtrl').listGioHang
				            					  .push(list);
				            					  $cookieStore
				            					  .put(
				            							  'sc',
				            							  Scopes
				            							  .get('ShoppingCtrl').listGioHang);
				            				  }
				            			  } else {
				            				  Scopes.get('ShoppingCtrl').listGioHang
				            				  .push(list);
				            				  $cookieStore
				            				  .put(
				            						  'sc',
				            						  Scopes
				            						  .get('ShoppingCtrl').listGioHang);
				            			  }
				            			  // Scopes.get('MenuCtrl').quantityShoppingCart
				            			  // = $cookieStore.get('sc').length;

				            			  $("#notifyQuantity")
				            			  .text(
				            					  $cookieStore
				            					  .get('sc').length);
				            			  ngDialog.close();
				            		  }
				            	  };
				              } ]
			});
		}
		
		function removeSpecialLetter(s) {
        	var str = s.trim().toLowerCase();
        	str = str.replace(/à/g, 'a');
        	str = str.replace(/á/g, 'a');
        	str = str.replace(/ạ/g, 'a');
        	str = str.replace(/ả/g, 'a');
        	str = str.replace(/ã/g, 'a');
        	str = str.replace(/â/g, 'a');
        	str = str.replace(/ầ/g, 'a');
        	str = str.replace(/ấ/g, 'a');
        	str = str.replace(/ậ/g, 'a');
        	str = str.replace(/ẩ/g, 'a');
        	str = str.replace(/ẫ/g, 'a');
        	str = str.replace(/ă/g, 'a');
        	str = str.replace(/ằ/g, 'a');
        	str = str.replace(/ắ/g, 'a');        	
        	str = str.replace(/ặ/g, 'a');
        	str = str.replace(/ẳ/g, 'a');
        	str = str.replace(/ẵ/g, 'a');        	
        	str = str.replace(/đ/g, 'd');        	
        	str = str.replace(/è/g, 'e');
        	str = str.replace(/é/g, 'e');
        	str = str.replace(/ẹ/g, 'e');
        	str = str.replace(/ẻ/g, 'e');
        	str = str.replace(/ẽ/g, 'e');
        	str = str.replace(/ê/g, 'e');        	
        	str = str.replace(/ề/g, 'e');
        	str = str.replace(/ế/g, 'e');
        	str = str.replace(/ệ/g, 'e');
        	str = str.replace(/ể/g, 'e');
        	str = str.replace(/ễ/g, 'e');
        	str = str.replace(/ì/g, 'i');
        	str = str.replace(/í/g, 'i');
        	str = str.replace(/ị/g, 'i');
        	str = str.replace(/ỉ/g, 'i');
        	str = str.replace(/ĩ/g, 'i');
        	str = str.replace(/ò/g, 'o');
        	str = str.replace(/ó/g, 'o');
        	str = str.replace(/ọ/g, 'o');
        	str = str.replace(/ỏ/g, 'o');
        	str = str.replace(/õ/g, 'o');
        	str = str.replace(/ô/g, 'o');        	
        	str = str.replace(/ồ/g, 'o');
        	str = str.replace(/ố/g, 'o');
        	str = str.replace(/ộ/g, 'o');
        	str = str.replace(/ổ/g, 'o');
        	str = str.replace(/ỗ/g, 'o');
        	str = str.replace(/ơ/g, 'o');        	
        	str = str.replace(/ờ/g, 'o');
        	str = str.replace(/ớ/g, 'o');
        	str = str.replace(/ợ/g, 'o');
        	str = str.replace(/ở/g, 'o');
        	str = str.replace(/ỡ/g, 'o');        	
        	str = str.replace(/ù/g, 'u');
        	str = str.replace(/ú/g, 'u');
        	str = str.replace(/ụ/g, 'u');
        	str = str.replace(/ủ/g, 'u');
        	str = str.replace(/ũ/g, 'u');
        	str = str.replace(/ư/g, 'u');        	
        	str = str.replace(/ừ/g, 'u');
        	str = str.replace(/ứ/g, 'u');
        	str = str.replace(/ự/g, 'u');
        	str = str.replace(/ử/g, 'u');
        	str = str.replace(/ữ/g, 'u');
        	str = str.replace(/ỳ/g, 'y');
        	str = str.replace(/ý/g, 'y');
        	str = str.replace(/ỵ/g, 'y');
        	str = str.replace(/ỷ/g, 'y');
        	str = str.replace(/ỹ/g, 'y');
        	
        	return str;
        }
		
		function search() {
			getListService(1);
		}

		$scope.clickPage = function() {
			if ($('body').outerWidth() > 736) {
				setTimeout(
						function() {
							if (self.gridLayout) {
								if ($scope.listService.length > 0) {
									var n = 0;
									var m = 0;

									for (var h = 0; h < parseInt($scope.listService.length / 3); h++) {
										var arrIdDiv = [];
										var maxHeight = 0;
										for (var i = m; i < m + 3; i++) {
											arrIdDiv.push($(
													'#shopping'
															+ $scope.listService[i].id)
													.outerHeight());
										}

										for (var j = 0; j < arrIdDiv.length; j++) {
											if (arrIdDiv[j] > maxHeight) {
												maxHeight = arrIdDiv[j];
											}
										}

										for (var k = m; k < m + 3; k++) {
											$('#shopping' + $scope.listService[k].id)
													.outerHeight(maxHeight);
										}

										n++;
										m = m + 3;
									}

									if ($scope.listService.length % 3 != 0) {
										arrIdDiv = [];
										maxHeight = 0;
										for (var n = m; n < m
												+ $scope.listService.length % 3; n++) {
											arrIdDiv.push($(
													'#shopping'
															+ $scope.listService[n].id)
													.outerHeight());
										}

										for (var j = 0; j < arrIdDiv.length; j++) {
											if (arrIdDiv[j] > maxHeight) {
												maxHeight = arrIdDiv[j];
											}
										}

										for (var k = m; k < m
												+ $scope.listService.length % 3; k++) {
											$('#shopping' + $scope.listService[k].id)
													.outerHeight(maxHeight);
										}
									}
								}
							}
						}, 100);
			}
		};

		/* implements method */
		function getListService(numberPage) {
			
			self.loading = true;
			var listServiceApi = "services/" + CONSTANT.lang + "/booking/"
					+ self.fieldId + "/searchs?keyword=" + self.keyword
					+ "&page=" + numberPage + "&sort=" + self.sort;
			
			// /services/{code}/booking/{id}/searchs?keyword={keyword}&page={page}&sort={sort}
			homeService
					.getData(listServiceApi)
					.then(
							function(res) {
								
								if(res.data.items != null) {
									$scope.listService = res.data.items.data;
									$scope.total = res.data.items.count
										* $scope.everyPage;								
									if (res.data.items.data.length > 0) {
										var listId = res.data.items.data[0].id;
										for (var i = 1; i < res.data.items.data.length; i++) {
											listId = listId + ','
													+ res.data.items.data[i].id;
										}
										$scope.listId = listId;

										if ($('body').outerWidth() > 736) {
											setTimeout(
												function() {
													if (self.gridLayout) {
														if ($scope.listService.length > 0) {
														var n = 0;
														var m = 0;

														for (var h = 0; h < parseInt($scope.listService.length / 3); h++) {
															var arrIdDiv = [];
															var maxHeight = 0;
															for (var i = m; i < m + 3; i++) {
																arrIdDiv
																		.push($(
																				'#shopping'
																						+ $scope.listService[i].id)
																				.outerHeight());
															}

															for (var j = 0; j < arrIdDiv.length; j++) {
																if (arrIdDiv[j] > maxHeight) {
																	maxHeight = arrIdDiv[j];
																}
															}

															for (var k = m; k < m + 3; k++) {
																$(
																		'#shopping'
																				+ $scope.listService[k].id)
																		.outerHeight(
																				maxHeight);
															}

															n++;
															m = m + 3;
														}

														if ($scope.listService.length % 3 != 0) {
															arrIdDiv = [];
															maxHeight = 0;
															for (var n = m; n < m
																	+ $scope.listService.length
																	% 3; n++) {
																arrIdDiv
																		.push($(
																				'#shopping'
																						+ $scope.listService[n].id)
																				.outerHeight());
															}

															for (var j = 0; j < arrIdDiv.length; j++) {
																if (arrIdDiv[j] > maxHeight) {
																	maxHeight = arrIdDiv[j];
																}
															}

															for (var k = m; k < m
																	+ $scope.listService.length
																	% 3; k++) {
																$(
																		'#shopping'
																				+ $scope.listService[k].id)
																		.outerHeight(
																				maxHeight);
															}
														}
													}
													}
													
												}, 1000);
										}
										
									}
								} else {
									$scope.listService = [];
									$scope.total = 0;
								}
								// check meta data
								if ($state.current.url == '/mua-sam') {
									homeService.checkMetaState();
								}
								setTimeout(function() {
									$rootScope.htmlReady();
								}, 100);
								self.loading = false;
							});

		}

		function selectLinhVuc(code, name) {
			self.fieldId = code;
			self.nameSelected = name;
			getListService(self.page);
		}

		/* Get selected field */
		function getSelectedService() {
			// var serviceDetailApi =
			// "services/vn/booking/luu-tru/searchs?keyword=da+nang&page=1&sort=xep_hang_asc";
			// //
			// /services/{code}/booking/{id}/searchs?keyword={keyword}&page={page}&sort={sort}
			// console.log("respone: " + self.fieldId);
			// homeService.getData(serviceDetailApi).then(function(res) {
			// console.log("abc@");
			// console.log(res.data.items.data);
			// self.selectedService = res.data.info;
			// });
		}

		/* Get list all of field */
		function getListField() {
			homeService.getData('services/vn/field/home').then(function(res) {
				self.listField = res.data.items;
			});
		}

		/* when click sort button */
		function changeSort(sortType) {
			if (self.sortType == sortType) {
				if (self.sortOptional == "desc") {
					self.sortOptional = "asc";
				} else {
					self.sortOptional = "desc";
				}
			}
			self.sortType = sortType;
			self.sort = self.sortType + self.sortOptional;
			getListService(self.page);
		}

		/* change type of layout */
		function changeLayOut(layout) {
			if (layout == 'luoi') {
				self.gridLayout = true;
				$scope.clickPage();
			} else if (layout == 'hang-ngang') {
				self.gridLayout = false;
			}
		}

		homeService.getData('articles/' + CONSTANT.lang + '/promotion').then(
				function(res) {
					$scope.promotions = res.data.items;
				});

	}

	/* convert a number become a array, using for ng-repeat with a number */
	function getNumber(num) {
		return new Array(num);
	}

	function getBookingType(type) {
		var text = "";
		if('dat_phong' == type) {
			text = "Đặt phòng";
		} else if('dat_ban' == type) {
			text = "Đặt bàn";
		} else {
			text = "Đặt vé";
		}
		return text;
	}

})();
