;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('SearchCtrl', SearchCtrl);

    SearchCtrl.$inject = [ '$scope', '$stateParams', 'homeService', 'CONSTANT', '$window'];

    function SearchCtrl($scope, $stateParams, homeService, CONSTANT, $window) {

		CONSTANT.lang = homeService.getLanguage();
        var self = this;

        // public properties
        self.keywords = $stateParams.keywords;
        self.isExactly = $stateParams.isExactly;
        if (angular.isUndefined(self.keywords)) {
            self.keywords = '';
        }
        if (angular.isUndefined(self.isExactly)) {
        	self.isExactly = true;
        }
        self.page1 = 1;
        self.page2 = 1;
        self.page3 = 1;
        self.fieldId = '';

        // public methods
        self.getListSearchTinTuc = getListSearchTinTuc;
        self.getListSearchDichVu = getListSearchDichVu;
        self.getListSearchViecLam = getListSearchViecLam;
        self.highlightText = highlightText;

        self.selectTab = selectTab;
        self.getEnterKey = getEnterKey;

        /* Scope */
        $scope.searchCtrl = self;
        $scope.resultTinTuc = [];
        $scope.resultDichVu = [];
        $scope.resultViecLam = [];
        $scope.everyPage = 12;
        $scope.total1 = 0;
        $scope.total2 = 0;
        $scope.total3 = 0;

        /* init */
        getListSearchTinTuc(self.page1);
        getListSearchDichVu(self.page2);
        getListSearchViecLam(self.page3);

        /* implements method */

        function getListSearchTinTuc(numberPage) {
        	var listSearchTinTucApi = 'services/'+ CONSTANT.lang +'/news/searchs?keyword=' 
        		+ self.keywords + "&page="+ numberPage + "&isExactly=" + self.isExactly;
        	homeService.getData(listSearchTinTucApi).then(function(res) {
         		if (res.meta.code == 200) {
                    $scope.resultTinTuc = res.data.items.data;
             		$scope.total1 = res.data.items.count;

                    if ($scope.resultTinTuc.length > 0) {
                        angular.forEach($scope.resultTinTuc, function(value, index) {
                            $scope.resultTinTuc[index].tomTat = highlightText(subStringBySize($scope.resultTinTuc[index].tomTat, 190));
                            $scope.resultTinTuc[index].tieuDe = highlightText($scope.resultTinTuc[index].tieuDe);
                        });
                    }

                }
            });
        }

        function getListSearchDichVu(numberPage) {
        	var listSearchDichVuApi = 'services/'+ CONSTANT.lang +'/services/searchs?keyword=' 
        		+ self.keywords + "&page="+ numberPage + "&ids=" + "&isExactly="+self.isExactly;
        	homeService.getData(listSearchDichVuApi).then(function(res) {
                if (res.meta.code == 200) {
             		$scope.resultDichVu = res.data.items.data;
             		$scope.total2 = res.data.items.count;

                    if ($scope.resultDichVu.length > 0) {
                        angular.forEach($scope.resultDichVu, function(value, index) {
                            $scope.resultDichVu[index].ten = highlightText($scope.resultDichVu[index].ten);
                            $scope.resultDichVu[index].diaChi = highlightText($scope.resultDichVu[index].diaChi);
                        });
                    }
                }
            });
        }

        function getListSearchViecLam(numberPage) {
            self.fieldId = 'viec-lam';
            var listCarrerApi = 'tuyenDungs/' +'jobs/searchs?keyword=' +  homeService.removeSpecialLetter(self.keywords) + "&page="+ numberPage + "&isExactly=" + self.isExactly;

            homeService.getData(listCarrerApi).then(function(res) {
                if (res.meta.code == 200) {
                    $scope.resultViecLam = res.data.items.data;
                    $scope.total3= res.data.items.count;

                    if ($scope.resultViecLam.length > 0) {
                        angular.forEach($scope.resultViecLam, function(value, index) {
                            $scope.resultViecLam[index].tieuDe = highlightText($scope.resultViecLam[index].tieuDe);
                        });
                    }
                }
            });
        }

        function selectTab(news, serv, care) {
        	if (news > (serv + care)) {
        		$('#tabNews').addClass('resp-tab-active');
        		$('#tabServices').removeClass('resp-tab-active');
                $('#tabCareers').removeClass('resp-tab-active');
        		document.getElementById('sectionNews').style.display = 'block';
        		document.getElementById('sectionServices').style.display = 'none';
                document.getElementById('sectionCareers').style.display = 'none';
        	} else if (serv > (news + care)) {
                $('#tabServices').addClass('resp-tab-active');
                $('#tabNews').removeClass('resp-tab-active');
                $('#tabCareers').removeClass('resp-tab-active');
                document.getElementById('sectionNews').style.display = 'none';
                document.getElementById('sectionServices').style.display = 'block';
                document.getElementById('sectionCareers').style.display = 'none';
            } else {
        		$('#tabCareers').addClass('resp-tab-active');
                $('#tabServices').removeClass('resp-tab-active');
                $('#tabNews').removeClass('resp-tab-active');
                document.getElementById('sectionNews').style.display = 'none';
                document.getElementById('sectionServices').style.display = 'none';
                document.getElementById('sectionCareers').style.display = 'block';
        	}
        }
        
        

        function getEnterKey() {
        	getListSearchTinTuc(self.page1);
            getListSearchDichVu(self.page2);
            getListSearchViecLam(self.page3);
        	$window.location.href = '/tim-kiem/'+self.isExactly+'/'+self.keywords;
        }

        function subStringBySize(text, size) {
            if(text != null && text != '') {
                var l = text.length;
                var index = size > l ? l : size;
                while (index < l && ' ' != text.charAt(index)) {
                    index++;
                }
                var tail = index < l ? " ..." : "";
                return text.substring(0, index) + tail;
            }
            return "";
        }

        function highlightText(text) {
            var text1 = text;
            var beginInsertTag = '<b class="search-keyword">';
            var endInsertTag = '</b>';
            if (!angular.isUndefined(self.keywords) && self.keywords != null && self.keywords != '') {
                var key = homeService.removeSpecialLetterNoTrim(self.keywords).split(' ');

                for (var k = 0; k < key.length; k++) {
                    if (key[k] != '') {
                        var keyWord = key[k];
                        var kl = keyWord.length;

                        var tmp = homeService.removeSpecialLetterNoTrim(text1).split(' ');
                        for (var m = 0; m < tmp.length; m++) {
                            if (tmp[m] == homeService.removeSpecialLetterNoTrim(keyWord)) {
                                var i = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord));
                                var j = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord + ':'));
                                var n = homeService.removeSpecialLetterNoTrim(text1).indexOf(homeService.removeSpecialLetterNoTrim(keyWord + '.'));

                                if (i > -1) {
                                    text1 = insertString(text1, beginInsertTag, i);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+i+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+i+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+i+kl+endInsertTag.length)));
                                }

                                if (j > -1) {
                                    text1 = insertString(text1, beginInsertTag, j);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+j+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+j+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+j+kl+endInsertTag.length)));
                                }

                                if (n > -1) {
                                    text1 = insertString(text1, beginInsertTag, n);
                                    text1 = insertString(text1, endInsertTag, beginInsertTag.length+n+kl);
                                    text1 = text1.substring(0, beginInsertTag.length+n+kl+endInsertTag.length).concat(highlightText(text1.substring(beginInsertTag.length+n+kl+endInsertTag.length)));
                                }
                            }
                        }
                        
                    }
                }
            }
            
            return text1;
        }
        
        function insertString(text, insertString, index) {
            var s1 = text.substring(0,index);
            var s2 = text.substring(index);
            return s1.concat(insertString).concat(s2);
        }

    }
    ;
})();
