;
(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name nddkApp.controller:HomeCtrl
     * @description # HomeCtrl Controller of the nddkApp
     */
    angular.module('nddkApp').controller('careerCtrl', careerCtrl);

    careerCtrl.$inject =[ '$scope', 'homeService', 'CONSTANT', '$stateParams', '$rootScope' ];
    function careerCtrl($scope, homeService, CONSTANT,  $stateParams, $rootScope) {
    $('#datetimepicker1').datetimepicker({
         // format: 'DD-MM-YYYY',
         locale: 'en',
         // useCurrent: true,
         // useStrict: true,
         format: 'DD-MM-YYYY'
    });
    CONSTANT.lang = homeService.getLanguage();
    var self = this;

    // public properties
    self.keyword = '';
    self.fieldId = '';
    self.ngayHetHan = '';

    // public methods
    self.getJobs = getJobs;
    self.page = 1;
    self.getNumber = getNumber;
    self.gridLayout = false;
    self.linhVucTuyenDung = {};

    /* Scope */
    $scope.listCarrer = [];
    $scope.everyPage = 12;
    $scope.listLinhVucTuyenDung = {};
    $scope.total = 0;
    $scope.carrerCtrl = self;
    $scope.widthDevice = $('body').outerWidth();
    
    /* init */
    getJobs(self.page);
    getLinhVucTuyenDung();


    /* implements method */
    /*function getJobs(numberPage) {
        console.log(numberPage);
        console.log('getJobs');
        console.log('keyword : ' +self.keyword);
        self.fieldId = 'viec-lam';
        //self.keyword = '';
        var listCarrerApi = "tuyenDungs/searchTuyenDungs/" + CONSTANT.lang + "/" + self.fieldId
                + "?keyword=" + homeService.removeSpecialLetter(self.keyword)
                + "&page="+ numberPage;

        console.log('listCarrerApi : ' + listCarrerApi);

        homeService.getData(listCarrerApi).then(function(res) {
            console.log('get data');

            $scope.listCarrer = res.data.items.data;
            $scope.total = res.data.items.count * $scope.everyPage;


            console.log(res.data);

        });

        
    }*/


    function getJobs(numberPage) {
            self.fieldId = 'viec-lam';
            if (angular.isUndefined(self.keyword) || self.keyword == null) {
                self.keyword = '';
            }
            self.ngayHetHan = $('#ngayHetHan').val();
            var idLinhVuc;
            if (self.linhVucTuyenDung === null || Object.getOwnPropertyNames(self.linhVucTuyenDung).length === 0) {
            	idLinhVuc = 0;
            } else {
            	idLinhVuc = self.linhVucTuyenDung.id;
            }
            var listCarrerApi = "tuyenDungs/searchTuyenDungs?keyword=" + homeService.removeSpecialLetter(self.keyword)
                + "&page="+ numberPage + "&idLinhVuc=" + idLinhVuc + "&ngayHetHan=" + self.ngayHetHan;
            homeService.getData(listCarrerApi).then(function(res) {
                if (res.meta.code == 200) {

                    $scope.listCarrer = res.data.items.data;
                    $scope.total = res.data.items.count;
                    self.page = numberPage;
                }
            });
            homeService.checkMetaState();
    }
    
    function getLinhVucTuyenDung() {
    	var listLinhVucTuyenDung = "tuyenDungs/listLinhVucTuyenDung";
    	homeService.getData(listLinhVucTuyenDung).then(function(res){
    		if (res.meta.code == 200) {
    			$scope.listLinhVucTuyenDung = res.data.items.data;
    		}
    	});
    }

    /* convert a number become a array, using for ng-repeat with a number */
    function getNumber(num) {
        return new Array(num);
    }

    };
})();
