﻿'use strict';

/**
 * @ngdoc filter
 * @name nddkApp.filter:commonFilter
 * @function
 * @description
 * # commonFilter
 * Filter in the nddkApp.
 */
angular.module('nddkApp').filter('to_trusted', ['$sce', function($sce) {
	return function(text) {
		return $sce.trustAsHtml(text);
	};

}]).filter('cut', function() {
	return function(valueParam, wordwise, maxParam, tail) {
		var value = valueParam;
		var max = maxParam;

		if (!value)
			return '';

		max = parseInt(max, 10);
		if (!max)
			return value;
		if (value.length <= max)
			return value;

		value = value.substr(0, max);
		if (wordwise) {
			var lastspace = value.lastIndexOf(' ');
			if (lastspace != -1) {
				value = value.substr(0, lastspace);
			}
		}

		return value + (tail || ' …');
	};
}).filter('truncateText', function() {
	return function(valueParam, wordwise, maxParam, tail) {
		var value = valueParam;
		var max = maxParam;

		if (!value) {
			return '';
		}

		max = parseInt(max, 10);
		if (!max) {
			return value;
		}
		if (value.length <= max) {
			return value;
		}

		value = value.substr(0, max);
		if (wordwise) {
			var lastspace = value.lastIndexOf(' ');
			if (lastspace !== -1) {
				value = value.substr(0, lastspace);
			}
		}

		return value + (tail || ' …');
	};
});
