﻿;
(function() {
	'use strict';

	/**
	 * @ngdoc service
	 * @name nddkApp.homeService
	 * @description # homeService Service in the nddkApp.
	 */
	// declaration
	angular.module('nddkApp').service('registrationService', registrationService);

	// implements
	registrationService.$inject = [ '$http', 'CONSTANT' ];
	function registrationService($http, CONSTANT) {
		var self = this;
		self.data = [];
		self.registrationService = {};

		self.getUser = function(path) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
			data = $http.get(CONSTANT.apiURL + path).then(function(response) {
				// The return value gets picked up by the then in the
				// controller.
//				console.log(response.data);
				return response.data;
			});
			// Return the promise to the controller
			return data;
		};

		self.createUser = function(path, user) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
//			var msgs = [];
			var req = {
			    method : 'POST',
			    url : CONSTANT.apiURL + path,
			    headers : {
				    'Content-type' : 'application/x-www-form-urlencoded'
			    },
			    data : user
			};
			return $http(req);
		};

		self.loginUser = function(path, user) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
//			var msgs = [];
			var req = {
			    method : 'POST',
			    url : CONSTANT.apiURL + path,
			    headers : {
				    "Content-type" : "application/x-www-form-urlencoded"
			    },
			    data : user
			};
			return $http(req);
		};
		
		self.loginUserFacebook = function(path, user) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
//			var msgs = [];
			var req = {
			    method : 'POST',
			    url : CONSTANT.apiURL + path,
			    headers : {
				    "Content-type" : "application/json"
			    },
			    data : user
			};
			return $http(req);
		};

		self.logoutUser = function(path, user) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
//			var msgs = [];
			var req = {
			    method : 'POST',
			    url : CONSTANT.apiURL + path,
			    headers : {
				    "Content-type" : "application/x-www-form-urlencoded"
			    },
			    data : user
			};
			return $http(req);
		};
	}
	;

})();