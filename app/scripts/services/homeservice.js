﻿'use strict';

/**
 * @ngdoc service
 * @name nddkApp.homeService
 * @description # homeService Service in the nddkApp.
 */
angular.module('nddkApp').factory('homeService', function($http, CONSTANT, ipCookie, ngDialog, Scopes, MetaService, $state, $rootScope) {
	var data = [];
	var homeService = {};
	homeService.getData = function(path) {
		// $http returns a promise, which has a then function, which also
		// returns a promise
		data = $http.get(CONSTANT.apiURL + path).then(function(response) {
			// The return value gets picked up by the then in the controller.
			return response.data;
		});
		// Return the promise to the controller
		return data;
	};

	homeService.postData = function(path, param) {
		// $http returns a promise, which has a then function, which also
		// returns a promise
		var req = {
		    method : 'POST',
		    url : CONSTANT.apiURL + path,
		    headers : {
			    "Content-type" : "application/x-www-form-urlencoded"
		    },
		    data : param
		};
		return $http(req);
	};

	homeService.getDataByPostMethod = function(path, paramString) {
		// $http returns a promise, which has a then function, which also
		// returns a promise
		var req = {
		    method : 'POST',
		    url : CONSTANT.apiURL + path,
		    headers : {
			    "Content-type" : "application/x-www-form-urlencoded"
		    },
		    data : paramString
		};
		return $http(req);
	};

	homeService.getDataByGetMethod = function(path, paramjson) {
		// $http returns a promise, which has a then function, which also
		// returns a promise
//		var msgs = [];
		var req = {
		    method : 'GET',
		    url : CONSTANT.apiURL + path,
		    headers : {
			    "Content-type" : "application/x-www-form-urlencoded"
		    },
		    params : paramjson
		};
		return $http(req);
	};

	homeService.registerEmail = function(path, param) {
		// $http returns a promise, which has a then function, which also
		// returns a promise
		var req = {
		    method : 'POST',
		    url : CONSTANT.apiURL + path,
		    headers : {
			    "Content-type" : "application/x-www-form-urlencoded"
		    },
		    data : param
		};
		return $http(req);
	};

	homeService.getLanguage = function() {
		if (!angular.isUndefined(ipCookie('lg'))) {
			if (ipCookie('lg') == 'vn') {
				return 'vn';
			}
			return 'en';
		}
		return 'vn';
	};

	/* Show notification */
	homeService.showNotification = function(message, time) {
		var dialog = ngDialog.open({
		    template : 'noticeDialog',
		    className : 'ngdialog-theme-default',
		    data : {
			    'message' : message
		    }
		});
		setTimeout(function() {
			dialog.close();
		}, time);
	};

	/* Check if logged in yet? */
	homeService.isLoggedIn = function() {
		var isLoggedIn = false;
		if (!angular.isUndefined(ipCookie("user"))) {
			isLoggedIn = true;
		}
		return isLoggedIn;
	};

	/* Get user is logging */
	homeService.getCurrentLoginUser = function() {
		var currentLoginUser = null;
		if (homeService.isLoggedIn()) {
			currentLoginUser = ipCookie("user");
		}
		return currentLoginUser;
	};

	/* show message requirement and pop up login */
	homeService.requriedLogin = function() {
		Scopes.get("LoginCtrl").openPopup();
	};

	homeService.checkLoginPublic = function () {
		var result = false;
		if (homeService.isLoggedIn()) {
			var currentUser = ipCookie('user');
			if (currentUser.sys) {
				result = true;
			}
		}
		return result;
	};

	/* locate the dialog on center of screen */
	homeService.locateDialog = function() {
		setTimeout(function() {
			var scrH = $("body").height();
			var dlgH = $(".ngdialog-content").outerHeight();
			$(".ngdialog-theme-default").css('padding-top', Math.round((scrH - dlgH) / 2) + 'px');
			$(".ngdialog-theme-default").css('padding-bottom', Math.round((scrH - dlgH) / 2) + 'px');
		}, 10);
	};

	/* Responsive equal height rows */
	homeService.equalheight = function(container) {

		var currentTallest = 0, currentRowStart = 0, rowDivs = new Array(), $el, topPostion = 0, currentDiv = 0;

		$(container).each(function() {

			$el = $(this);
			$($el).height('auto');
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	};

	homeService.removeSpecialLetter = function(s) {
    	var str = s.trim().toLowerCase();
    	str = str.replace(/à/g, 'a');
    	str = str.replace(/á/g, 'a');
    	str = str.replace(/ạ/g, 'a');
    	str = str.replace(/ả/g, 'a');
    	str = str.replace(/ã/g, 'a');
    	str = str.replace(/â/g, 'a');
    	str = str.replace(/ầ/g, 'a');
    	str = str.replace(/ấ/g, 'a');
    	str = str.replace(/ậ/g, 'a');
    	str = str.replace(/ẩ/g, 'a');
    	str = str.replace(/ẫ/g, 'a');
    	str = str.replace(/ă/g, 'a');
    	str = str.replace(/ằ/g, 'a');
    	str = str.replace(/ắ/g, 'a');        	
    	str = str.replace(/ặ/g, 'a');
    	str = str.replace(/ẳ/g, 'a');
    	str = str.replace(/ẵ/g, 'a');        	
    	str = str.replace(/đ/g, 'd');        	
    	str = str.replace(/è/g, 'e');
    	str = str.replace(/é/g, 'e');
    	str = str.replace(/ẹ/g, 'e');
    	str = str.replace(/ẻ/g, 'e');
    	str = str.replace(/ẽ/g, 'e');
    	str = str.replace(/ê/g, 'e');        	
    	str = str.replace(/ề/g, 'e');
    	str = str.replace(/ế/g, 'e');
    	str = str.replace(/ệ/g, 'e');
    	str = str.replace(/ể/g, 'e');
    	str = str.replace(/ễ/g, 'e');
    	str = str.replace(/ì/g, 'i');
    	str = str.replace(/í/g, 'i');
    	str = str.replace(/ị/g, 'i');
    	str = str.replace(/ỉ/g, 'i');
    	str = str.replace(/ĩ/g, 'i');
    	str = str.replace(/ò/g, 'o');
    	str = str.replace(/ó/g, 'o');
    	str = str.replace(/ọ/g, 'o');
    	str = str.replace(/ỏ/g, 'o');
    	str = str.replace(/õ/g, 'o');
    	str = str.replace(/ô/g, 'o');        	
    	str = str.replace(/ồ/g, 'o');
    	str = str.replace(/ố/g, 'o');
    	str = str.replace(/ộ/g, 'o');
    	str = str.replace(/ổ/g, 'o');
    	str = str.replace(/ỗ/g, 'o');
    	str = str.replace(/ơ/g, 'o');        	
    	str = str.replace(/ờ/g, 'o');
    	str = str.replace(/ớ/g, 'o');
    	str = str.replace(/ợ/g, 'o');
    	str = str.replace(/ở/g, 'o');
    	str = str.replace(/ỡ/g, 'o');        	
    	str = str.replace(/ù/g, 'u');
    	str = str.replace(/ú/g, 'u');
    	str = str.replace(/ụ/g, 'u');
    	str = str.replace(/ủ/g, 'u');
    	str = str.replace(/ũ/g, 'u');
    	str = str.replace(/ư/g, 'u');        	
    	str = str.replace(/ừ/g, 'u');
    	str = str.replace(/ứ/g, 'u');
    	str = str.replace(/ự/g, 'u');
    	str = str.replace(/ử/g, 'u');
    	str = str.replace(/ữ/g, 'u');
    	str = str.replace(/ỳ/g, 'y');
    	str = str.replace(/ý/g, 'y');
    	str = str.replace(/ỵ/g, 'y');
    	str = str.replace(/ỷ/g, 'y');
    	str = str.replace(/ỹ/g, 'y');
    	return str;
    }

    homeService.removeSpecialLetterNoTrim = function(s) {
    	var str = s.toLowerCase();
    	str = str.replace(/à/g, 'a');
    	str = str.replace(/á/g, 'a');
    	str = str.replace(/ạ/g, 'a');
    	str = str.replace(/ả/g, 'a');
    	str = str.replace(/ã/g, 'a');
    	str = str.replace(/â/g, 'a');
    	str = str.replace(/ầ/g, 'a');
    	str = str.replace(/ấ/g, 'a');
    	str = str.replace(/ậ/g, 'a');
    	str = str.replace(/ẩ/g, 'a');
    	str = str.replace(/ẫ/g, 'a');
    	str = str.replace(/ă/g, 'a');
    	str = str.replace(/ằ/g, 'a');
    	str = str.replace(/ắ/g, 'a');        	
    	str = str.replace(/ặ/g, 'a');
    	str = str.replace(/ẳ/g, 'a');
    	str = str.replace(/ẵ/g, 'a');        	
    	str = str.replace(/đ/g, 'd');        	
    	str = str.replace(/è/g, 'e');
    	str = str.replace(/é/g, 'e');
    	str = str.replace(/ẹ/g, 'e');
    	str = str.replace(/ẻ/g, 'e');
    	str = str.replace(/ẽ/g, 'e');
    	str = str.replace(/ê/g, 'e');        	
    	str = str.replace(/ề/g, 'e');
    	str = str.replace(/ế/g, 'e');
    	str = str.replace(/ệ/g, 'e');
    	str = str.replace(/ể/g, 'e');
    	str = str.replace(/ễ/g, 'e');
    	str = str.replace(/ì/g, 'i');
    	str = str.replace(/í/g, 'i');
    	str = str.replace(/ị/g, 'i');
    	str = str.replace(/ỉ/g, 'i');
    	str = str.replace(/ĩ/g, 'i');
    	str = str.replace(/ò/g, 'o');
    	str = str.replace(/ó/g, 'o');
    	str = str.replace(/ọ/g, 'o');
    	str = str.replace(/ỏ/g, 'o');
    	str = str.replace(/õ/g, 'o');
    	str = str.replace(/ô/g, 'o');        	
    	str = str.replace(/ồ/g, 'o');
    	str = str.replace(/ố/g, 'o');
    	str = str.replace(/ộ/g, 'o');
    	str = str.replace(/ổ/g, 'o');
    	str = str.replace(/ỗ/g, 'o');
    	str = str.replace(/ơ/g, 'o');        	
    	str = str.replace(/ờ/g, 'o');
    	str = str.replace(/ớ/g, 'o');
    	str = str.replace(/ợ/g, 'o');
    	str = str.replace(/ở/g, 'o');
    	str = str.replace(/ỡ/g, 'o');        	
    	str = str.replace(/ù/g, 'u');
    	str = str.replace(/ú/g, 'u');
    	str = str.replace(/ụ/g, 'u');
    	str = str.replace(/ủ/g, 'u');
    	str = str.replace(/ũ/g, 'u');
    	str = str.replace(/ư/g, 'u');        	
    	str = str.replace(/ừ/g, 'u');
    	str = str.replace(/ứ/g, 'u');
    	str = str.replace(/ự/g, 'u');
    	str = str.replace(/ử/g, 'u');
    	str = str.replace(/ữ/g, 'u');
    	str = str.replace(/ỳ/g, 'y');
    	str = str.replace(/ý/g, 'y');
    	str = str.replace(/ỵ/g, 'y');
    	str = str.replace(/ỷ/g, 'y');
    	str = str.replace(/ỹ/g, 'y');
    	return str;
    }
	
	var languageVn = {
		hoVaTen : 'Họ và tên',
	    ho : 'Họ',
	    ten : 'Tên',
	    nhapLaiMatKhau : 'Nhập lại mật khẩu',
	    tenDoanhNghiep : 'Tên doanh nghiệp',
	    maSoThue : 'Mã số thuế',
	    diaChi : 'Địa chỉ',
	    soDienThoai : 'Số điện thoại',
	    thongTinDoanhNghiep : 'THÔNG TIN DOANH NGHIỆP',
	    huyDangKyDoanhNghiep : 'Hủy đăng ký doanh nghiệp',
	    dangKyThanhVienmoi : 'Đăng ký thành viên mới',
	    tim : 'Tìm',
	    timKiem : 'Tìm kiếm',
	    datHang : 'Đặt hàng',
	    datCho : 'Đặt chỗ',
	    binhLuan : 'bình luận',
	    traLoi : 'Trả lời',
	    gui : 'Gửi',
	    tinTucKhac : 'Tin tức khác',
	    tinTucTongHop : 'Tin tức tổng hợp',
	    congThongTinHoTro : 'Cổng thông tin hỗ trợ người dân - du khách',
	    trangChu : 'Trang chủ',
	    veDaNang : 'Về Đà Nẵng',
	    tinTuc : 'Tin tức',
	    tinKhuyenMai : 'Khuyến mãi',
	    tinMoiNhat : 'Tin mới nhất',
	    suKienLeHoi : 'Sự kiện - Lễ hội',
	    thongTinChoDuKhach : 'Thông tin cho người dân và du khách',
	    suKienSapDienRa : 'Sự kiện sắp diễn ra',
	    suKienNoiBat : 'Sự kiện nổi bật',
	    suKien : 'Sự kiện',
	    duLich : 'Du lịch',
	    tinThanhPho : 'Tin thành phố',
	    daNangTrenDaPhatTrien : 'Đà Nẵng trên đà phát triển',
	    noiBat : 'Nổi bật',
	    luuTru : 'Lưu trú',
	    amThuc : 'Ẩm thực',
	    vuiChoiGiaiTri : 'Vui chơi - Giải trí',
	    muaSam : 'Mua sắm',
	    thongTinCanBiet : 'Thông tin cần biết',
	    topDiemDen : 'Top điểm đến',
	    diemThamQuan : 'Điểm tham quan',
	    chiaSe : 'Chia sẻ',
	    chiaSeCamNhan : 'Chia sẻ - Cảm nhận',
	    banDo : 'Bản đồ',
	    dangNhap : 'Đăng nhập',
	    dangNhapHeThong : 'Đăng nhập hệ thống',
	    quenMatKhau : 'Quên mật khẩu',
	    nhapMatKhau : 'Nhập mật khẩu',
	    giuTrangThaiDangNhap : 'Giữ trạng thái đăng nhập',
	    dangNhapBangTaiKhoan : 'Đăng nhập bằng tài khoản',
	    dangNhapVoiFacebook : 'Đăng nhập với Facebook',
	    dangNhapVoiGoogle : 'Đăng nhập với Google',
	    chungToiSeKhong : 'Chúng tôi sẽ không bao giờ gởi bài viết hoặc chia sẽ thông tin mà chưa được sự đồng ý của bạn',
	    vuiLongNhapDiaChiEmail : 'Vui lòng nhập địa chỉ email',
	    vuiLongNhapMatKhau : 'Vui lòng nhập mật khẩu',
	    banChuaCoTaiKhoan : 'Bạn chưa có tài khoản ?',
	    dangKyTaiKhoan : 'Đăng ký tài khoản',
	    dangKyTaiKhoanDN : 'Đăng ký tài khoản doanh nghiệp',
	    dangKy : 'Đăng ký',
	    viTri : 'Vị trí',
	    timDuong : 'Tìm đường',
	    chonLinhVuc : 'Chọn lĩnh vực',
	    quanTam : 'Quan tâm',
	    chuongTrinhKhuyenMai : 'Chương trình khuyến mãi',
	    khuyenMaiGiamGia : 'Khuyến mãi - Giảm giá',
	    camNhan : 'Cảm nhận',
	    xemThemCamNhan : 'Xem thêm cảm nhận',
	    mangXaHoi : 'Mạng xã hội',
	    banQuyenThuocVeTPDaNang : 'Bản quyền thuộc về Thành Phố Đà Nẵng',
	    dichVu : 'Dịch vụ',
		sanPhamDichVu : 'Sản phẩm - Dịch vụ',
	    gioHang : 'Giỏ hàng',
	    thongTinTaiKhoan : 'Thông tin tài khoản',
	    toiDaDangKyTaiKhoan : 'Tôi đã đăng ký tài khoản',
	    matKhau : 'Mật khẩu',
	    dangXuat : 'Đăng xuất',
	    phienBanDiDong : 'Phiên bản di động',
	    phatTrienBoi : 'Phát triển bởi',
	    toanCauXanh : 'Toàn Cầu Xanh',
	    quanLyDoanhNghiep : 'Quản lý doanh nghiệp',
	    xemTatCa : 'Xem tất cả',
	    thuGon : 'Thu gọn',
	    yeuCauDangNhap : 'Vui lòng đăng nhập !',
	    yeuCauNhapNoiDungBinhLuan : 'Bạn chưa nhập nội dung bình luận',
	    guiBinhLuanThanhCong : 'Gửi bình luận thành công',
	    xayRaLoiTrongKhiGui : 'Xảy ra lỗi trong khi gửi. Vui lòng thử lại!',
	    loiKetNoi : 'Lỗi kết nối. Vui lòng thử lại!',
	    guiChiaSeThanhCong : 'Gửi chia sẽ thành công',
	    gioHanQuanTam : 'Bạn đã sử dụng chức năng này. Bạn phải đợi 24h nữa mới được thao tác tiếp',
	    diemDuLich : "Những điểm du lịch không thể bỏ qua",
	    guiMailThanhCong : "Gửi email thành công !",
	    noResult : "Không có kết quả nào được tìm thấy ",
		viecLam : "Tuyển dụng - Việc làm",
		viecLamLienQuan : "Việc làm khác",
		trangKhac : "Trang khác",
		thongTinLienHe : "Thông tin liên hệ"
	};

	var languageEn = {
		hoVaTen : 'Fullname',
	    ho : 'Last name',
	    ten : 'First name',
	    nhapLaiMatKhau : 'Password-confirm',
	    tenDoanhNghiep : 'Business name',
	    maSoThue : 'Tax code',
	    diaChi : 'Address',
	    soDienThoai : 'Phone number',
	    thongTinDoanhNghiep : 'BUSINESS INFORMATION',
	    huyDangKyDoanhNghiep : 'Cancel',
	    dangKyThanhVienmoi : 'New member registration',
	    tim : 'Search',
	    timKiem : 'Search',
	    datHang : 'Order',
	    datCho : 'Order',
	    binhLuan : 'comment',
	    traLoi : 'Reply',
	    gui : 'Send',
	    tinTucKhac : 'Other news',
	    tinTucTongHop : 'General news',
	    congThongTinHoTro : 'Residents - visitors support portal',
	    trangChu : 'Home',
	    veDaNang : 'About Da Nang',
	    tinTuc : 'News',
	    tinKhuyenMai : 'Promotion news',
	    tinMoiNhat : 'Lastest news',
	    suKienLeHoi : 'Events - Festival',
	    thongTinChoDuKhach : 'Information for you',
	    suKienSapDienRa : 'Upcoming events',
	    suKienNoiBat : 'Upcoming events',
	    suKien : 'Events',
	    duLich : 'Tourism',
	    tinThanhPho : 'City news',
	    daNangTrenDaPhatTrien : 'Da Nang on the rise',
	    noiBat : 'Outstanding',
	    luuTru : 'Stay',
	    amThuc : 'Cuisine',
	    vuiChoiGiaiTri : 'Entertaiment - Relax',
	    muaSam : 'Shopping',
	    thongTinCanBiet : 'Need to know',
	    topDiemDen : 'Top destination',
	    diemThamQuan : 'Sights',
	    chiaSe : 'Share',
	    chiaSeCamNhan : 'Share - Feel',
	    banDo : 'Maps',
	    dangNhap : 'Sign in',
	    dangNhapHeThong : 'Logged on the system',
	    quenMatKhau : 'Forgot password',
	    nhapMatKhau : 'Enter password',
	    giuTrangThaiDangNhap : 'Keep signing in',
	    dangNhapBangTaiKhoan : 'Sign in by your account',
	    dangNhapVoiFacebook : 'Sign in by Facebook account',
	    dangNhapVoiGoogle : 'Sign in by Google account',
	    chungToiSeKhong : 'We will never send your post or share your information without your permission',
	    vuiLongNhapDiaChiEmail : 'Please enter your email address',
	    vuiLongNhapMatKhau : 'Please enter your password',
	    banChuaCoTaiKhoan : 'Do you have account ?',
	    dangKyTaiKhoan : 'Sign up',
	    dangKyTaiKhoanDN : 'Sign up for business',
	    dangKy : 'Register',
	    viTri : 'Location',
	    timDuong : 'Find road',
	    chonLinhVuc : 'Choose field',
	    quanTam : 'Interest',
	    chuongTrinhKhuyenMai : 'Promotion Events',
	    khuyenMaiGiamGia : 'Promotion - Sale',
	    camNhan : 'Testimonials',
	    xemThemCamNhan : 'See more',
	    mangXaHoi : 'Social networks',
	    banQuyenThuocVeTPDaNang : 'Copyright by DaNang City',
	    dichVu : 'Services',
	    sanPhamDichVu : 'Products - Services',
	    gioHang : 'Shopping cart',
	    thongTinTaiKhoan : 'Account information',
	    toiDaDangKyTaiKhoan : 'I had registered',
	    matKhau : 'Password',
	    dangXuat : 'Logout',
	    phienBanDiDong : 'Mobile version',
	    phatTrienBoi : 'Developed by',
	    toanCauXanh : 'Green Global',
	    quanLyDoanhNghiep : 'Business Management',
	    xemTatCa : 'view all',
	    thuGon : 'Collapse',
	    yeuCauDangNhap : 'Login is required !',
	    yeuCauNhapNoiDungBinhLuan : 'The comment can be not blank!',
	    guiBinhLuanThanhCong : 'Send comment successful',
	    xayRaLoiTrongKhiGui : 'Occurs errors during sending. Try again please!',
	    loiKetNoi : 'Connection fail. Try again please!',
	    guiChiaSeThanhCong : 'Share feel is sent successful',
	    gioHanQuanTam : 'You have used this function. Please try it again 24 hours later',
	    diemDuLich : "Attractions should be not missed",
	    guiMailThanhCong : "Email sent successfully !",
	    noResult : "No results found",
		viecLam : "Careers",
		viecLamLienQuan : "Jobs raleated",
		trangKhac : "Other page",
		thongTinLienHe : "Contact us"
	};

	homeService.updateLanguageByCode = function(code) {
		if (code == 'vn') {
			return languageVn;
		}
		return languageEn;
	};

	homeService.convertTime = function(time) {
		if (time.indexOf('/') != -1) {
			return time;
		} else {
			var splTime = String(time).split(':');
			var rs = '';
			if(splTime != null){
				if (splTime[0] <= 0) {
					rs = splTime[1] == 0?"vừa xong":splTime[1]+" phút trước";
				} else {
					rs = splTime[0]+" giờ "+splTime[1]+" phút trước";
				}
			}
		}
		return rs;
	};

	function getFormattedDate(input) {
	    var spl_DateTime = String(input).split(" ");
	    var spl_Date = String(spl_DateTime[1]).split('/');
	    var date = new Date(spl_Date[2]+'/'+spl_Date[1]+'/'+spl_Date[0]+' '+spl_DateTime[0]);
	    return date;
	};

	homeService.openShareEmailPopup = function(tieuDe, link){
		ngDialog.open({
		    template : 'popupShareEmail',
		    className : 'ngdialog-theme-default custom-width-share-popup',
		    data : {'tieuDe':tieuDe, 'link':link},
		    controller : 'ShareEmailCtrl'
    	});
	};

	homeService.getMetaService = function(){
	    if(angular.isUndefined($rootScope.metaService)){
            $rootScope.metaService = MetaService;
        }
	    return $rootScope.metaService;
	};

	// check and set meta
	homeService.checkMetaState = function(metaParam){
	  if(!angular.isUndefined(metaParam) && metaParam != null){
	      homeService.getMetaService().set(
	              metaParam.titlePage,
	              metaParam.metaKeywords,
	              metaParam.metaDescription,
	              metaParam.thumbNailUrl);
	  } else {
	      if(!angular.isUndefined($state.current.meta)){
	        homeService.getMetaService().set(
	              $state.current.meta.titlePage,
	              $state.current.meta.metaKeywords,
	              $state.current.meta.metaDescription,
	              $state.current.meta.thumbNailUrl);
    	  }else{
    	      homeService.getMetaService().set("Cổng thông tin dịch vụ Đà Nẵng","","","");
    	  }
	  }
	};

	homeService.setMataService = function(titlePage_, metaKeyword_, metaDescription_, thumbNailUrl_){
	    $rootScope.metaservice.set(titlePage_, metaKeyword_, metaDescription_, thumbNailUrl_);
	};

	return homeService;
});
