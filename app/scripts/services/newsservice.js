﻿'use strict';

/**
 * @ngdoc service
 * @name nddkApp.newsService
 * @description
 * # newsService
 * Service in the nddkApp.
 */
 angular.module('nddkApp')
 .factory('newsService', function ($http, CONSTANT) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var data = [];
    var newsService = {};
    newsService.getData = function(path) {
      	// $http returns a promise, which has a then function, which also returns a promise
//    	console.log("newsService path: " + path);
      	data = $http.get(CONSTANT.apiURL + path).then(function (response) {
	        // The return value gets picked up by the then in the controller.
	        return response.data;
	    });
      	// Return the promise to the controller
      	return data;
      };
      return newsService;
  });
