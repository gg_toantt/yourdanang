﻿'use strict';

/**
 * @ngdoc service
 * @name nddkApp.homeService
 * @description
 * # homeService
 * Service in the nddkApp.
 */
 angular.module('nddkApp')
 .factory('homeService', function ($http, CONSTANT) {
 	var data = [];
 	var homeService = {};
 	homeService.getData = function(path) {
      	// $http returns a promise, which has a then function, which also returns a promise
      	data = $http.get(CONSTANT.apiURL + path).then(function (response) {
	        // The return value gets picked up by the then in the controller.
	        return response.data;
	    });
      	// Return the promise to the controller
      	return data;
    };
    return homeService;
});
