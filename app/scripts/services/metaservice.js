;
(function() {
    'use strict';
/**
 * @ngdoc service
 * @name nddkApp.homeService
 * @description # homeService Service in the nddkApp.
 */
 angular.module('nddkApp').service('MetaService', metaService);
//implements
 metaService.$inject = [ '$http', 'CONSTANT', "$location" ];
 function metaService($http, CONSTANT, $location) {
       var titlePage = 'Cổng thông tin dịch vụ Đà Nẵng';
       var metaDescription = '';
       var metaKeywords = '';
       var thumbNailUrl = '';
       var siteUrl = "";
       return {
          set: function(titlePage_, metaKeyword_, metaDescription_, thumbNailUrl_, siteUrl_) {
              titlePage = titlePage_ + " - YourDaNang";
              metaKeywords = metaKeyword_;
              metaDescription = metaDescription_;
              thumbNailUrl = thumbNailUrl_;
              siteUrl = siteUrl_;
          },
          titlePage: function(){ return titlePage; }, 
          metaDescription: function() { return metaDescription; },
          metaKeywords: function() { return metaKeywords; },
          thumbNailUrl: function() { return thumbNailUrl; },
          siteUrl: function() { return siteUrl; }
       };
     }	
  })();