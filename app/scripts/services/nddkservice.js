﻿'use strict';

/**
 * @ngdoc service
 * @name nddkApp.nddkservice
 * @description
 * # nddkservice
 * Service in the nddkApp.
 */
angular.module('nddkApp')
.factory('nddkservice', function ($http, CONSTANT) {
    // AngularJS will instantiate a singleton by calling "new" on this function    
    var data = [];
    var nddkservice = {};
    nddkservice.getData = function(path) {
		// $http returns a promise, which has a then function, which also returns a promise
		data = $http.get(CONSTANT.apiURL + path).then(function (response) {
			// The return value gets picked up by the then in the controller.
			return response.data;
		});
		// Return the promise to the controller
		return data;
	};

	return nddkservice;
});
