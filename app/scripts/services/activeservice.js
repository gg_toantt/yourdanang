﻿;(function() {
	'use strict';

	/**
	 * @ngdoc service
	 * @name nddkApp.homeService
	 * @description
	 * # homeService
	 * Service in the nddkApp.
	 */
	 // declaration
	 angular.module('nddkApp')
	 .service('activeService', activeService);
	 
	 // implements
	 activeService.$inject = ['$http', 'CONSTANT'];
	 function activeService($http, CONSTANT) {
		var self = this;
		self.data = [];
		self.activeService = {};
		
		self.getData = function(path) {
			// $http returns a promise, which has a then function, which also returns a promise
			data = $http.get(CONSTANT.apiURL + path).then(function (response) {
				// The return value gets picked up by the then in the controller.
//				console.log(response.data);
				return response.data;
			});
			// Return the promise to the controller
			return data;
		};
		
		self.addComment = function(path, param) {
			// $http returns a promise, which has a then function, which also returns a promise
//			var msgs = [];
			var req = {
				method : 'POST',
				url : CONSTANT.apiURL + path,
				headers : {
					"Content-type" : "application/x-www-form-urlencoded"
				},
				data : param
			};
			return $http(req);
		};
		
		self.addInterest = function(path, param) {
			// $http returns a promise, which has a then function, which also returns a promise
//			var msgs = [];
			var req = {
				method : 'POST',
				url : CONSTANT.apiURL + path,
				headers : {
					"Content-type" : "application/x-www-form-urlencoded"
				},
				data : param
			};
			return $http(req);
		};
	};
	
})();