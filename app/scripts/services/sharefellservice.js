﻿;
(function() {
	'use strict';

	/**
	 * @ngdoc service
	 * @name nddkApp.homeService
	 * @description # homeService Service in the nddkApp.
	 */
	// declaration
	angular.module('nddkApp').service('ShareFellService', shareFellService);

	// implements
	shareFellService.$inject = [ '$http', 'CONSTANT' ];
	function shareFellService($http, CONSTANT) {
		var self = this;
		self.data = [];
		self.registrationService = {};

		self.getListShare = function(path, page) {
			// $http returns a promise, which has a then function, which also
			// returns a promise
			var req = {
			    method : 'GET',
			    url : CONSTANT.apiURL + path,
			    headers : {
				    "Content-type" : "application/x-www-form-urlencoded"
			    },
			    params : page
			};
			return $http(req).then(function(response) {
				// The return value gets picked up by the then in the
				// controller.
//				console.log(response.data);
				return response.data;
			});
			return data;
		};
	}
})();