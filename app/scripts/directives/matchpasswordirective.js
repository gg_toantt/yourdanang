﻿'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:lastestNewsDirective
 * @description
 * # lastestNewsDirective
 */
 angular.module('nddkApp')
 .
 directive('match-pas', function() {
 
    return {
		
        require: 'registrationCtrl',
        link: function (scope, elem, attrs, model) {
            if (!attrs.match-pas) {
                console.error('match-pas expects a model as an argument!');
                return;
            }
            scope.$watch(attrs.match-pas, function (value) {
                // Only compare values if the second ctrl has a value.
                if (model.$viewValue !== undefined && model.$viewValue !== '') {
                    model.$setValidity('match-pas', value === model.$viewValue);
                }
            });
            model.$parsers.push(function (value) {
                // Mute the match-pas error if the second ctrl is empty.
                if (value === undefined || value === '') {
                    model.$setValidity('match-pas', true);
                    return value;
                }
                var isValid = value === scope.$eval(attrs.match-pas);
                model.$setValidity('match-pas', isValid);
                return isValid ? value : undefined;
            });
        }
    };
});