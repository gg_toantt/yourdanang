'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:advertisingRight
 * @description
 * # advertisingRight
 */
 angular.module('nddkApp')
 .directive('videoPlayer', function ($timeout, CONSTANT) {
 	return {
 		templateUrl: 'views/partials/videoPlayer.html',
 		restrict: 'ACE',
 		scope: {
 			video: '@urlvideo',
 			image: '@urlpreview',
 			title: '@title'
 		},
 		link: function (scope) {
 			jwplayer('video').setup({ 
          	    file: scope.video, 
          	    image: scope.image,
          	    width: '100%',
          	    height: '100%'
          	});
 		}
 	};
 });