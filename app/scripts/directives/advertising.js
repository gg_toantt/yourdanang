﻿'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:advertisingRight
 * @description # advertisingRight
 */
angular.module('nddkApp').directive('advertisingRight', function($timeout, CONSTANT, Scopes) {
	return {
	    templateUrl : 'views/partials/adsBanner.html',
	    restrict : 'ACE',
	    scope : {
		    linkBanner : '@linkBanner'
	    },
	    controller : function($scope, $attrs, $http) {
		    $http.get(CONSTANT.apiURL + 'advertising/' + $attrs.linkbanner).then(function(response) {
			    // The return value gets picked up by the then in the
				// controller.
				if (angular.isDefined(response.data.data.info) && response.data.data.info != null) {
					$scope.adsRight = response.data.data.info[0];
			    	Scopes.get("EventBannerCtrl").impressionBanner(response.data.data.info[0].id);	
				}
			    
		    });
	    },
	    link : function() {
	    	//
	    }
	};
});