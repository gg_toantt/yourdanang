﻿'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:hotDestinationDirective
 * @description
 * # hotDestinationDirective
 */
 angular.module('nddkApp')
 .directive('hotDestination', function ($timeout, CONSTANT) {
 	return {
 		templateUrl: 'views/partials/hotDestinations.html',
 		restrict: 'AE',
 		scope: {type:'@type', title:'@title'},
 		controller: function ($scope, $http){
 			/*http://api.yourdanang.vn/a/services/vn/12_attractive_destinations*/
 			$http.get(CONSTANT.apiURL + 'services/' + CONSTANT.lang + '/12_attractive_destinations').then(function (response) {
	        // The return value gets picked up by the then in the controller.
	        $scope.hotDestinations = response.data.data.items;
	    });
 		},
 		link: function () {
 			$timeout( function(){
 				angular.element('#wd-slide-visit').carouFredSel({
 					responsive: true,
 					auto: false,
 					width: '100%',
 					prev: '#wd-prev-3',
 					next: '#wd-next-3',
 					pagination: false,
 					scroll : {
 						items        : 1,
 						pauseOnHover : true
 					},
 					items: {
 						width: 188,
 						height:'auto',
 						visible: {
 							min: 1,
 							max: 1
 						}
 					}
 				});
 			},1500);
 		}
 	};
 });