'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:hotDestinationDirective
 * @description
 * # hotDestinationDirective
 */
 angular.module('nddkApp')
 .directive('contact', function () {
 	return {
 		templateUrl: 'views/partial/contact.html',
 		restrict: 'AE'
 	};
 });