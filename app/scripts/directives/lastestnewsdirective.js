﻿'use strict';

/**
 * @ngdoc directive
 * @name nddkApp.directive:lastestNewsDirective
 * @description
 * # lastestNewsDirective
 */
 angular.module('nddkApp')
 .directive('lastestNews', function () {
 	return {
 		templateUrl: 'views/partials/lastestNews.html',
 		restrict: 'AE',
 		scope: {},
 		controller: function ($scope, $http, CONSTANT){
 			$http.get(CONSTANT.apiURL + 'articles/' + CONSTANT.lang + '/5_newest_articles').then(function (response) {
				// The return value gets picked up by the then in the controller.
				$scope.topNews = response.data.data.items;
			});
 		},
 		link: function () {}
 	};
 });