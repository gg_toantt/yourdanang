/*!
 * jQuery Cookie Plugin v1.4.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

    var pluses = /\+/g;

    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }

    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }

    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }

    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
        }

        try {
            // Replace server-side written pluses with spaces.
            // If we can't decode the cookie, ignore it, it's unusable.
            // If we can't parse the cookie, ignore it, it's unusable.
            s = decodeURIComponent(s.replace(pluses, ' '));
            return config.json ? JSON.parse(s) : s;
        } catch(e) {}
    }

    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }

    var config = $.cookie = function (key, value, options) {

        // Write

        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);

            if (typeof options.expires === 'number') {
                var days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e+5);
            }

            return (document.cookie = [
                encode(key), '=', stringifyCookieValue(value),
                options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                options.path    ? '; path=' + options.path : '',
                options.domain  ? '; domain=' + options.domain : '',
                options.secure  ? '; secure' : ''
            ].join(''));
        }

        // Read

        var result = key ? undefined : {};

        // To prevent the for loop in the first place assign an empty array
        // in case there are no cookies at all. Also prevents odd result when
        // calling $.cookie().
        var cookies = document.cookie ? document.cookie.split('; ') : [];

        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split('=');
            var name = decode(parts.shift());
            var cookie = parts.join('=');

            if (key && key === name) {
                // If second argument (value) is a function it's a converter...
                result = read(cookie, value);
                break;
            }

            // Prevent storing a cookie that we couldn't decode.
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }

        return result;
    };

    config.defaults = {};

    $.removeCookie = function (key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }

        // Must not alter options, thus extending a fresh object...
        $.cookie(key, '', $.extend({}, options, { expires: -1 }));
        return !$.cookie(key);
    };

}));

$(document).ready(function(){
    var common = {
        scrollToTop: function() {
            console.log('scroll to top');
            window.scrollTo(0,
            0);

            return false;
        },

        toTop: function(){
            console.log('toTop');
            $("body,html").animate({
                scrollTop: 0
            }, "normal");
            $("#page").animate({
                scrollTop: 0
            }, "normal");
        }
    };

    //common.toTop();
    //common.scrollToTop();

    $(window).scroll(function() {
        if ($(window).scrollTop() >= 200) 
            $("#back-top").fadeIn();
        else $("#back-top").fadeOut()
    });

});

$(document).ready(function() {
    $(function() {
        $("#back-top").click(function() {

            $("body,html").animate({
                scrollTop: 0
            }, "normal");
            $("#page").animate({
                scrollTop: 0
            }, "normal");

            return !1;
        })
    })
});   

$(document).ready(function(dataAndEvents) {
    //console.log("hotline");
    //$("#wd_contact").fadeIn();

     $(document).ready(function(e) {
         var cookieCloseBalloon = jQuery.cookie('closeBalloon');
         //console.log("cookieCloseBalloon " +cookieCloseBalloon);
         jQuery.cookie('closeBalloon', 1);
         $("#close-balloon").click(function() {
             
             cookieCloseBalloon = -1;
             $("#wd_contact").slideUp(1000);
         });
         setTimeout(showFooterContact, 1000);

         
         function showFooterContact() {
            
            //console.log("cookieCloseBalloon " +cookieCloseBalloon);
             if (cookieCloseBalloon == 1) {
                 //console.log("showFooterContact");
                 if ($(window).scrollTop() + $(window).height() < $(document).height() - 100) {
                     //console.log("open 2");
                     if ($(window).width() > 480) {
                        //console.log("480");
                         $("#wd_contact").slideDown(1000);
                     } else {
                         $("#wd_contact").slideUp(1000);
                     }
                 } else {
                     $("#wd_contact").slideUp(1000);
                 }
             }
         }

         $(window).resize(function() {
            //console.log("resize");
             showFooterContact();
         });

         $(window).scroll(function() {
            //console.log("scroll");
            showFooterContact();
         });
     });
});  